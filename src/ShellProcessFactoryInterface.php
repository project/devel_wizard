<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard;

use Symfony\Component\Process\Process;

interface ShellProcessFactoryInterface {

  /**
   * @see \Symfony\Component\Process\Process::__construct
   */
  public function createInstance(
    array $command,
    ?string $cwd = NULL,
    ?array $env = NULL,
    $input = NULL,
    ?float $timeout = 60
  ): Process;

  /**
   * @see \Symfony\Component\Process\Process::fromShellCommandline
   */
  public function createInstanceFromString(
    string $command,
    string $cwd = NULL,
    array $env = NULL,
    $input = NULL,
    ?float $timeout = 60
  ): Process;

}
