<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;

/**
 * @property \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
 */
trait ConfigEntitySpellTrait {

  /**
   * Machine-name of the module which provides the entity type.
   *
   * @var string
   */
  protected string $provider = '';

  protected string $configEntityTypeId = '';

  protected ?ConfigEntityTypeInterface $configEntityType = NULL;

  protected function getConfigEntityType(): ?ConfigEntityTypeInterface {
    if ($this->configEntityType === NULL && $this->entityTypeManager->hasDefinition($this->configEntityTypeId)) {
      $this->configEntityType = $this->entityTypeManager->getDefinition($this->configEntityTypeId);
    }

    return $this->configEntityType;
  }

  protected ?ConfigEntityStorageInterface $configStorage = NULL;

  protected function getConfigStorage(): ?ConfigEntityStorageInterface {
    if ($this->configStorage === NULL && $this->entityTypeManager->hasDefinition($this->configEntityTypeId)) {
      $this->configStorage = $this->entityTypeManager->getStorage($this->configEntityTypeId);
    }

    return $this->configStorage;
  }

  protected string $contentEntityTypeId = '';

  protected ?ContentEntityTypeInterface $contentEntityType = NULL;

  protected function getContentEntityType(): ?ContentEntityTypeInterface {
    if ($this->contentEntityType === NULL && $this->entityTypeManager->hasDefinition($this->contentEntityTypeId)) {
      $this->contentEntityType = $this->entityTypeManager->getDefinition($this->contentEntityTypeId);
    }

    return $this->contentEntityType;
  }

  protected ?ContentEntityStorageInterface $contentStorage = NULL;

  protected function getContentStorage(): ?ContentEntityStorageInterface {
    if ($this->contentStorage === NULL && $this->entityTypeManager->hasDefinition($this->contentEntityTypeId)) {
      $this->contentStorage = $this->entityTypeManager->getStorage($this->contentEntityTypeId);
    }

    return $this->contentStorage;
  }

}
