<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Constraints;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NodeBundleValidator extends ConstraintValidator implements ContainerInjectionInterface {

  protected EntityTypeBundleInfoInterface $bundleInfo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.bundle.info'),
    );
  }

  public function __construct(EntityTypeBundleInfoInterface $bundleInfo) {
    $this->bundleInfo = $bundleInfo;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $nodeBundles = $this->bundleInfo->getBundleInfo('node');

    if (!in_array($value, array_keys($nodeBundles))) {
      $this->context->addViolation('Node bundle %bundle does not exist', [
        '%bundle' => $value,
      ]);
    }
  }

}
