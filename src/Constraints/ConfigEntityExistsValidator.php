<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Constraints;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ConfigEntityExistsValidator extends ConstraintValidator implements ContainerInjectionInterface {

  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    /** @var \Drupal\devel_wizard\Plugin\Validation\Constraint\ConfigEntityExistsConstraint $constraint */
    if ($value === NULL || $value === '') {
      return;
    }

    $args = [
      '%entity_type.id' => $constraint->entityTypeId,
      '@entity_type.id' => $constraint->entityTypeId,
      '%entity_type.label' => $constraint->entityTypeId,
      '@entity_type.label' => $constraint->entityTypeId,
    ];

    if (!$this->entityTypeManager->hasDefinition($constraint->entityTypeId)) {
      $this->context->addViolation($constraint->messageEntityTypeIsMissing, $args);

      return;
    }

    $entityType = $this->entityTypeManager->getDefinition($constraint->entityTypeId);
    $args['%entity_type.label'] = $entityType->getLabel();
    $args['@entity_type.label'] = $entityType->getLabel();

    $storage = $this->entityTypeManager->getStorage($constraint->entityTypeId);
    $entities = $storage->loadMultiple();
    $exists = array_key_exists($value, $entities);
    $args['%bundle.id'] = $value;
    $args['@bundle_list'] = implode(', ', array_keys($entities));

    if ($exists && !$constraint->hasToBeExists) {
      $this->context->addViolation($constraint->messageExpectedNotToBeExists, $args);
    }
    elseif (!$exists && $constraint->hasToBeExists) {
      $this->context->addViolation($constraint->messageExpectedToBeExists, $args);
    }
  }

}
