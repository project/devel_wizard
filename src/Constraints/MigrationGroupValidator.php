<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Constraints;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MigrationGroupValidator extends ConstraintValidator implements ContainerInjectionInterface {

  protected EntityStorageInterface $migrationGroupStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('migration_group'),
    );
  }

  public function __construct(EntityStorageInterface $migrationGroupStorage) {
    $this->migrationGroupStorage = $migrationGroupStorage;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $migrationGroups = $this->migrationGroupStorage->loadMultiple();

    foreach ($migrationGroups as $migrationGroup) {
      if ($migrationGroup->id() === $value) {
        return;
      }
    }

    $this->context->addViolation("migration group %migration_group does not exist", [
      '%migration_group' => $value,
    ]);
  }

}
