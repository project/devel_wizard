<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Constraints;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ModuleExistsValidator extends ConstraintValidator implements ContainerInjectionInterface {

  protected ModuleExtensionList $moduleList;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('extension.list.module'),
    );
  }

  public function __construct(ModuleExtensionList $moduleList) {
    $this->moduleList = $moduleList;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $installedModules = $this->moduleList->getAllInstalledInfo();
    if (isset($installedModules[$value])) {
      return;
    }

    $availableModules = $this->moduleList->getAllAvailableInfo();
    if (!isset($availableModules[$value])) {
      $this->context->addViolation("module %module is not available", [
        '%module' => $value,
      ]);
    }
  }

}
