<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\SpellForm;

use Symfony\Component\DependencyInjection\ContainerInterface;

class BlockContentTypeRoleSpellForm extends SpellFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('devel_wizard.spell.block_content_type_role')
    );
  }

}
