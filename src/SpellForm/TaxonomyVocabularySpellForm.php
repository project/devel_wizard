<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\SpellForm;

use Symfony\Component\DependencyInjection\ContainerInterface;

class TaxonomyVocabularySpellForm extends SpellFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('devel_wizard.spell.taxonomy_vocabulary'),
    );
  }

  protected function buildSpellSettingsFromFormValues(array $values): array {
    $machineName = $values['vid'];
    $values['create']['settings']['values']['vid'] = $machineName;
    $values['behat']['settings']['machine_name'] = $machineName;
    $values['migration']['settings']['machine_name'] = $machineName;
    $values['role']['settings']['machine_name'] = $machineName;

    return parent::buildSpellSettingsFromFormValues($values);
  }

}
