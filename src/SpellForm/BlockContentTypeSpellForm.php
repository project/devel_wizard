<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\SpellForm;

use Symfony\Component\DependencyInjection\ContainerInterface;

class BlockContentTypeSpellForm extends SpellFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('devel_wizard.spell.block_content_type'),
    );
  }

  protected function buildSpellSettingsFromFormValues(array $values): array {
    $machineName = $values['machine_name'];
    $values['create']['settings']['values']['id'] = $machineName;
    $values['admin_view']['settings']['machine_name'] = $machineName;
    $values['behat']['settings']['machine_name'] = $machineName;
    $values['migration']['settings']['machine_name'] = $machineName;
    $values['role']['settings']['machine_name'] = $machineName;

    return parent::buildSpellSettingsFromFormValues($values);
  }

}
