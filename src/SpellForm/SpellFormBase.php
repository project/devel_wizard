<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\SpellForm;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\devel_wizard\SpellInterface;

abstract class SpellFormBase extends FormBase {

  protected SpellInterface $spell;

  public function __construct(SpellInterface $spell) {
    $this->spell = $spell;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->spell->id() . '_spell_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['settings'] = $this->spell->settingsFormBuild(['settings'], $form_state);
    if (!count($form['settings'])) {
      // @todo Maybe the submit button is still needed.
      // Spell has no settings form.
      return $form;
    }

    $form['settings']['#tree'] = TRUE;

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 999,
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Abracadabra'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $parents = ['settings'];
    $configData = $this->buildSpellSettingsFromFormValues($form_state->getValue($parents));
    $validationErrors = $this->spell->validate($configData);

    /** @var \Symfony\Component\Validator\ConstraintViolationInterface $validationError */
    foreach ($validationErrors as $validationError) {
      $nameParts = array_merge(
        $parents,
        explode('.', $validationError->getPropertyPath()),
      );

      $form_state->setErrorByName(
        implode('][', $nameParts),
        $validationError->getMessage(),
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $parents = ['settings'];
    $settings = $this->buildSpellSettingsFromFormValues($form_state->getValue($parents));

    $messenger = $this->messenger();
    $args = [
      '%spell.id' => $this->spell->id(),
    ];
    try {
      $this->spell->abracadabra($settings);
    }
    catch (\Exception $e) {
      $messenger->addError($this->t(
        'Casting spell %spell.id was unsuccessful.',
        $args,
      ));
      $messenger->addError($e->getMessage());

      return;
    }

    $messenger->addStatus($this->t(
      'Spell %spell.id was successful!',
      $args,
    ));
  }

  protected function buildSpellSettingsFromFormValues(array $values): array {
    return $this->spell->applyDefaultValues($values);
  }

}
