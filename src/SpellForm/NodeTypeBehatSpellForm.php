<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\SpellForm;

use Symfony\Component\DependencyInjection\ContainerInterface;

class NodeTypeBehatSpellForm extends SpellFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('devel_wizard.spell.node_type_behat'),
    );
  }

}
