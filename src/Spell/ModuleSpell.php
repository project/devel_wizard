<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Component\Serialization\YamlSymfony;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\devel_wizard\ShellProcessFactoryInterface;
use Symfony\Component\Filesystem\Filesystem;
use Webmozart\PathUtil\Path;

class ModuleSpell extends SpellBase {

  protected string $id = 'devel_wizard_module';

  protected ShellProcessFactoryInterface $processFactory;

  public function __construct(
    MessengerInterface $messenger,
    LoggerChannelInterface $logger,
    TwigEnvironment $twig,
    ModuleExtensionList $moduleList,
    ModuleHandlerInterface $moduleHandler,
    ModuleInstallerInterface $moduleInstaller,
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    EntityTypeManagerInterface $entityTypeManager,
    ShellProcessFactoryInterface $processFactory,
    ?Filesystem $fs = NULL
  ) {
    $this->processFactory = $processFactory;

    parent::__construct(
      $messenger,
      $logger,
      $twig,
      $moduleList,
      $moduleHandler,
      $moduleInstaller,
      $configFactory,
      $typedConfigManager,
      $entityTypeManager,
      $fs,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('New module');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Generates a code base for a new module');
  }

  public function applyDefaultValues(array $settings): array {
    $settings = parent::applyDefaultValues($settings);
    assert(!empty($settings['machine_name']), 'machine_name is required');

    $machineName = $settings['machine_name'] ?? '';
    $package = $settings['machine_name'] ?? '';
    $parentDir = $settings['parent_dir'] ?? Path::join('modules', 'custom');
    $nameParts = explode('_', $machineName, 2);
    if (count($nameParts) === 2) {
      $name = ucwords(implode(' - ', $nameParts));
      $package = ucwords($nameParts[0]);
    }

    $default = [
      'is_standalone' => FALSE,
      'dst_dir' => Path::join(
        $parentDir,
        $machineName,
      ),
      'parent_dir' => $parentDir,
      'project_type' => 'module',
      'package' => $package,
      'name' => $name ?? $machineName,
      'description' => 'Useless description',
      'core_compatibility' => [
        '8' => FALSE,
        '9' => TRUE,
      ],
      'initial_git_branch' => '1.x',
      'info.yml' => [],
    ];

    $settings = array_replace_recursive($default, $settings);

    if ($settings['is_standalone'] && $settings['initial_git_branch']) {
      $settings['dst_dir'] .= '-' . $settings['initial_git_branch'];
    }

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormBuild(array $parents, FormStateInterface $formState): array {
    return [
      // @todo Machine-name widget.
      'machine_name' => [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => $this->t('Machine-name'),
        '#description' => $this->t('Machine readable name of the new module'),
        '#default_value' => '',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function abracadabra(array $settings) {
    $this->context = $settings;
    $this->generateInfoYml();
    if (!empty($this->context['is_standalone'])) {
      $this->generateComposerJson();
      $this->generateReadmeMd();
      $this->gitInit();
    }

    if (empty($this->context['is_standalone'])) {
      drupal_flush_all_caches();
    }

    return $this;
  }

  /**
   * @return $this
   */
  protected function generateInfoYml() {
    $fileName = Path::join(
      $this->context['dst_dir'],
      "{$this->context['machine_name']}.info.yml",
    );

    $this->dumpFile(
      $fileName,
      YamlSymfony::encode($this->getInfoYmlData()),
    );

    $this->messageFileCreate($fileName);

    return $this;
  }

  /**
   * @return $this
   */
  protected function generateComposerJson() {
    $fileName = Path::join(
      $this->context['dst_dir'],
      'composer.json',
    );

    $this->dumpFile(
      $fileName,
      json_encode($this->getComposerJsonData(), $this->jsonEncodeFlags) . "\n",
    );

    $this->messageFileCreate($fileName);

    return $this;
  }

  /**
   * @return $this
   */
  protected function generateReadmeMd() {
    $fileName = Path::join(
      $this->context['dst_dir'],
      'README.md',
    );
    $this->fs->dumpFile($fileName, $this->getReadmeMdContent());
    $this->messageFileCreate($fileName);

    return $this;
  }

  /**
   * @return $this
   */
  protected function gitInit() {
    $process = $this->processFactory->createInstance(
      ['git', 'init', "--initial-branch={$this->context['initial_git_branch']}"],
      $this->context['dst_dir'],
    );
    $process->run();

    $exitCode = $process->getExitCode();

    $msgArgs = [
      '@spell' => $this->id(),
      '%dir' => $this->context['dst_dir'],
    ];
    $msgType = $exitCode ? MessengerInterface::TYPE_WARNING : MessengerInterface::TYPE_STATUS;
    $msgText = $exitCode ?
      $this->t('@spell Git repository could not be initialized in: %dir', $msgArgs)
      : $this->t('@spell Git repository has been initialized in: %dir', $msgArgs);

    $this->messenger()->addMessage($msgText, $msgType);

    return $this;
  }

  protected function getReadmeMdContent(): string {
    return $this->twig->render(
      '@devel_wizard/devel_wizard.extension.README.md.twig',
      $this->context
    );
  }

  protected function getInfoYmlData(): array {
    $data = [
      'type' => 'module',
      'package' => $this->context['package'],
      'name' => $this->context['name'],
      'description' => $this->context['description'],
    ];

    if ($this->context['core_compatibility']['8']) {
      $data['core'] = '8.x';
    }

    $data['core_version_requirement'] = '^' . implode(' || ^', array_keys($this->context['core_compatibility'], TRUE, TRUE));

    $data += $this->context['info.yml'];

    return $data;
  }

  protected function getComposerJsonData(): array {
    return [
      'name' => "drupal/{$this->context['machine_name']}",
      'type' => 'drupal-module',
      'description' => "{$this->context['description']}",
      'keywords' => [],
      'license' => 'GPL-2.0-or-later',
      'homepage' => "https://drupal.org/project/{$this->context['machine_name']}",
      'authors' => [],
      'support' => [
        'issues' => "https://drupal.org/project/issues/{$this->context['machine_name']}",
        'source' => "https://git.drupalcode.org/project/{$this->context['machine_name']}",
      ],
      'config' => [],
      'require' => [
        'php' => '>=7.4',
      ],
      'require-dev' => [],
    ];
  }

}
