<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\devel_wizard\SpellInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @todo Validate that a views.view with the same name does not exists.
 */
class NodeTypeAdminViewSpell extends ConfigEntityAdminViewSpellBase {

  protected string $id = 'devel_wizard_node_type_admin_view';

  /**
   * {@inheritdoc}
   */
  protected string $provider = 'node';

  protected string $configEntityTypeId = 'node_type';

  protected string $contentEntityTypeId = 'node';

  protected string $primaryTabPath = 'admin/content/node';

  protected string $bundleIdInConfigTemplate = 'article';

  protected ModuleExtensionList $moduleList;

  /**
   * @var \Drupal\devel_wizard\SpellInterface
   */
  protected SpellInterface $moduleSpell;

  protected ConfigEntityStorageInterface $nodeTypeStorage;

  public function __construct(
    MessengerInterface $messenger,
    LoggerChannelInterface $logger,
    TwigEnvironment $twig,
    ModuleExtensionList $moduleList,
    ModuleHandlerInterface $moduleHandler,
    ModuleInstallerInterface $moduleInstaller,
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    EntityTypeManagerInterface $entityTypeManager,
    SpellInterface $moduleSpell,
    UuidInterface $uuid,
    ?Filesystem $fs = NULL
  ) {
    $this->moduleSpell = $moduleSpell;
    $this->uuid = $uuid;

    parent::__construct(
      $messenger,
      $logger,
      $twig,
      $moduleList,
      $moduleHandler,
      $moduleInstaller,
      $configFactory,
      $typedConfigManager,
      $entityTypeManager,
      $fs,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Content type - admin view');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Creates a view for administrators for an existing Content type');
  }

  public function applyDefaultValues(array $settings): array {
    $settings = parent::applyDefaultValues($settings);
    assert(!empty($settings['machine_name']), 'node_type machine_name is required');
    $nodeTypeMachineName = $settings['machine_name'];

    return array_replace_recursive(
      [
        'active' => TRUE,
        'machine_name' => $nodeTypeMachineName,
        'module' => [
          'machine_name' => 'app_core',
        ],
      ],
      $settings,
    );
  }

  /**
   * {@inheritdoc}
   *
   * @todo Maybe this method can be moved into the parent class.
   */
  public function abracadabra(array $settings) {
    $this->context = $settings;

    $this->createDerivatives($this->configEntityTypeId);

    $this->installComposerPackages('prod', $this->getRequiredPackages());
    $this->installComposerPackages('dev', $this->getRequiredDevPackages());

    $this->installModules($this->getRequiredModules());

    $this->viewStorage = $this->entityTypeManager->getStorage('view');

    // @todo Config views.view.content" is required.
    // @todo Check that if the links.task entry for the main route is exists or not.
    $viewRaw = $this->getSecondaryBundleTabView();

    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $view */
    $view = $this->viewStorage->load($viewRaw['id']);
    if ($view) {
      $this->messageConfigEntityExists($view);
    }
    else {
      /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $view */
      $view = $this->viewStorage->create($viewRaw);
      $view->save();
      $this->messageConfigEntityCreate($view);
    }

    // Flushing caches so the action items show up on the created view pages.
    drupal_flush_all_caches();
  }

  protected function getSecondaryBundleTabView(): array {
    $machineName = $this->context['machine_name'];

    $data = parent::getSecondaryBundleTabView();
    $data['display']['default']['display_options']['access']['options']['perm'] = "delete any $machineName content";
    $data['display']['overview']['display_options']['path'] = "admin/content/node/list-$machineName";

    return $data;
  }

}
