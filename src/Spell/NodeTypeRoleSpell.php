<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Core\StringTranslation\TranslatableMarkup;

class NodeTypeRoleSpell extends ConfigEntityRoleSpellBase {

  /**
   * {@inheritdoc}
   */
  protected string $id = 'devel_wizard_node_type_role';

  protected string $provider = 'node';

  protected string $configEntityTypeId = 'node_type';

  protected string $contentEntityTypeId = 'node';

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Content type - role');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Creates a new user role which grants full control over a specific Content type');
  }

  /**
   * @param array $settings
   *
   * @return bool[]
   */
  protected function getDefaultPermissions(array $settings): array {
    $machineName = $settings['machine_name'];

    return [
      'access administration pages' => TRUE,
      'view the administration theme' => TRUE,
      "create $machineName content" => TRUE,
      "edit any $machineName content" => TRUE,
      "delete any $machineName content" => TRUE,
      "view $machineName revisions" => TRUE,
      "revert $machineName revisions" => TRUE,
      "delete $machineName revisions" => TRUE,
    ];
  }

}
