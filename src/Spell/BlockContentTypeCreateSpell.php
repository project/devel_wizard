<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\block_content\Entity\BlockContentType;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

class BlockContentTypeCreateSpell extends ConfigEntitySpellBase {

  /**
   * {@inheritdoc}
   */
  protected string $id = 'devel_wizard_block_content_type_create';

  /**
   * {@inheritdoc}
   */
  protected string $provider = 'block_content';

  protected string $configEntityTypeId = 'block_content_type';

  protected string $contentEntityTypeId = 'block_content';

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Block Content type - create');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Creates a new Block Content type without any extra');
  }

  public function applyDefaultValues(array $settings): array {
    $settings = parent::applyDefaultValues($settings);
    assert(!empty($settings['values']['id']), 'id is required');
    $machineName = $settings['values']['id'];

    return array_replace_recursive(
      [
        'values' => [
          'id' => $machineName,
          'label' => $machineName,
          'description' => 'Useless description',
          'revision' => FALSE,
        ],
      ],
      $settings,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormBuild(array $parents, FormStateInterface $formState): array {
    return [
      'values' => [
        '#tree' => TRUE,
        'id' => [
          '#type' => 'machine_name',
          '#required' => TRUE,
          '#default_value' => '',
          '#maxlength' => 64,
          '#description' => $this->t('A unique name for this item. It must only contain lowercase letters, numbers, and underscores.'),
          '#machine_name' => [
            'exists' => [BlockContentType::class, 'load'],
            'standalone' => TRUE,
          ],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function abracadabra(array $settings) {
    $this->context = $settings;
    $this->createConfigInstance($this->context['values']);

    return $this;
  }

}
