<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\RoleInterface;

class BlockContentTypeRoleSpell extends ConfigEntityRoleSpellBase {

  /**
   * {@inheritdoc}
   */
  protected string $id = 'devel_wizard_block_content_type_role';

  /**
   * {@inheritdoc}
   */
  protected string $provider = 'block_content';

  protected string $configEntityTypeId = 'block_content_type';

  protected string $contentEntityTypeId = 'block_content';

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Block content type - role');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Creates a new user role which grants full control over a specific Block content type');
  }

  /**
   * @param array $settings
   *
   * @return bool[]
   */
  protected function getDefaultPermissions(array $settings): array {
    $machineName = $settings['machine_name'];

    return [
      'devel_wizard.access_custom_blocks' => TRUE,
      'access administration pages' => TRUE,
      'view the administration theme' => TRUE,
      "create $machineName block content" => TRUE,
      "delete any $machineName block content" => TRUE,
      "update any $machineName block content" => TRUE,
    ];
  }

  /**
   * Helper method to grant block content permissions for a given role.
   *
   * @param \Drupal\user\RoleInterface $role
   *   The role object we want to update the permissions for.
   *
   * @return $this
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function updatePermissions(RoleInterface $role) {
    $this->installComposerPackages('prod', ['drupal/block_content_permissions' => '^1.10']);
    if (!$this->ensureModuleInstalled('block_content_permissions')) {
      return $this;
    }
    else {
      $permissions = $this->context['user_role']['values']['permissions'];
      foreach ($permissions as $permission) {
        $role->grantPermission($permission);
      }
      $role->save();
      $this->messageConfigEntityUpdated($role);
    }

    return $this;
  }

}
