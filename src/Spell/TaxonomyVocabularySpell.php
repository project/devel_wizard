<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class TaxonomyVocabularySpell extends ConfigEntitySpellBase {

  /**
   * {@inheritdoc}
   */
  protected string $id = 'devel_wizard_taxonomy_vocabulary';

  /**
   * {@inheritdoc}
   */
  protected string $provider = 'taxonomy';

  protected string $configEntityTypeId = 'taxonomy_vocabulary';

  protected string $contentEntityTypeId = 'taxonomy_term';

  protected TaxonomyVocabularyCreateSpell $taxonomyVocabularyCreateSpell;

  protected TaxonomyVocabularyRoleSpell $taxonomyVocabularyRoleSpell;

  protected TaxonomyVocabularyMigrationSpell $taxonomyVocabularyMigrationSpell;

  protected TaxonomyVocabularyBehatSpell $taxonomyVocabularyBehatSpell;

  public function __construct(
    MessengerInterface $messenger,
    LoggerChannelInterface $logger,
    TwigEnvironment $twig,
    ModuleExtensionList $moduleList,
    ModuleHandlerInterface $moduleHandler,
    ModuleInstallerInterface $moduleInstaller,
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    EntityTypeManagerInterface $entityTypeManager,
    TaxonomyVocabularyCreateSpell $taxonomyVocabularyCreateSpell,
    TaxonomyVocabularyRoleSpell $taxonomyVocabularyRoleSpell,
    TaxonomyVocabularyMigrationSpell $taxonomyVocabularyMigrationSpell,
    TaxonomyVocabularyBehatSpell $taxonomyVocabularyBehatSpell,
    ?Filesystem $fs = NULL
  ) {
    $this->taxonomyVocabularyCreateSpell = $taxonomyVocabularyCreateSpell;
    $this->taxonomyVocabularyRoleSpell = $taxonomyVocabularyRoleSpell;
    $this->taxonomyVocabularyMigrationSpell = $taxonomyVocabularyMigrationSpell;
    $this->taxonomyVocabularyBehatSpell = $taxonomyVocabularyBehatSpell;

    parent::__construct(
      $messenger,
      $logger,
      $twig,
      $moduleList,
      $moduleHandler,
      $moduleInstaller,
      $configFactory,
      $typedConfigManager,
      $entityTypeManager,
      $fs
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Taxonomy vocabulary - all in one');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Creates a Taxonomy vocabulary with all the bells and whistles');
  }

  public function applyDefaultValues(array $settings): array {
    $settings = parent::applyDefaultValues($settings);
    assert(!empty($settings['vid']), 'vid is required');

    $taxonomyVocabularyId = $settings['vid'];

    $settings = array_replace_recursive(
      [
        'create' => [
          'enabled' => TRUE,
          'settings' => [
            'values' => [
              'vid' => $taxonomyVocabularyId,
            ],
          ],
        ],
        'role' => [
          'enabled' => TRUE,
          'settings' => [
            'machine_name' => $taxonomyVocabularyId,
          ],
        ],
        'migration' => [
          'enabled' => TRUE,
          'settings' => [
            'machine_name' => $taxonomyVocabularyId,
          ],
        ],
        'behat' => [
          'enabled' => TRUE,
          'settings' => [
            'machine_name' => $taxonomyVocabularyId,
          ],
        ],
      ],
      $settings,
    );

    $settings['create']['settings'] = $this->taxonomyVocabularyCreateSpell->applyDefaultValues($settings['create']['settings']);
    $settings['role']['settings'] = $this->taxonomyVocabularyRoleSpell->applyDefaultValues($settings['role']['settings']);
    $settings['migration']['settings'] = $this->taxonomyVocabularyMigrationSpell->applyDefaultValues($settings['migration']['settings']);
    $settings['behat']['settings'] = $this->taxonomyVocabularyBehatSpell->applyDefaultValues($settings['behat']['settings']);

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormBuild(array $parents, FormStateInterface $formState): array {
    $form = [
      '#type' => 'container',
      '#tree' => TRUE,
      'vid' => [
        '#type' => 'machine_name',
        // @todo Core does not support #label for machine_name elements.
        '#label' => $this->t('Machine-readable name of the new Content type'),
        '#default_value' => '',
        '#maxlength' => 64,
        '#description' => $this->t('A unique name for this Content type. It must only contain lowercase letters, numbers, and underscores.'),
        '#machine_name' => [
          // @todo The taxonomy module maybe not enabled yet.
          'exists' => [Vocabulary::class, 'load'],
          'standalone' => TRUE,
        ],
      ],
    ];

    foreach ($this->getIncludedSpells() as $key => $spell) {
      $subParents = $parents;
      $subParents[] = $key;

      $form[$key] = [
        '#type' => 'fieldset',
        '#title' => $spell->label(),
        '#tree' => TRUE,
        'enabled' => [
          '#title' => $this->t('Active'),
          '#default_value' => TRUE,
          '#type' => 'checkbox',
        ],
        'settings' => [
          '#type' => 'container',
          '#tree' => TRUE,
        ] + $spell->settingsFormBuild($subParents, $formState),
      ];

      $togglerSelector = sprintf(
        ':input[name="%s"]',
        Utils::inputName(array_merge($subParents, ['enabled'])),
      );
      $form[$key]['settings']['#states']['visible'][$togglerSelector] = ['checked' => TRUE];

      if ($key === 'create') {
        $form[$key]['enabled']['#access'] = FALSE;
      }

      if (isset($form[$key]['settings']['machine_name'])) {
        $form[$key]['settings']['machine_name']['#required'] = FALSE;
        $form[$key]['settings']['machine_name']['#access'] = FALSE;
      }

      if (isset($form[$key]['settings']['values']['vid'])) {
        $form[$key]['settings']['values']['vid']['#required'] = FALSE;
        $form[$key]['settings']['values']['vid']['#access'] = FALSE;
      }
    }

    return $form;
  }

  public function validate(array $values): ConstraintViolationListInterface {
    $violations = $this
      ->typedConfigManager
      ->createFromNameAndData($this->configName(), $values)
      ->validate();

    $machineNameErrors = [
      'role.settings.machine_name',
      'migration.settings.machine_name',
      'behat.settings.machine_name',
    ];
    /** @var \Symfony\Component\Validator\ConstraintViolationInterface $violation */
    foreach ($violations as $id => $violation) {
      if (in_array($violation->getPropertyPath(), $machineNameErrors)) {
        $violations->remove($id);
      }
    }

    return $violations;
  }

  /**
   * {@inheritdoc}
   */
  public function abracadabra(array $settings) {
    $this->context = $settings;

    $this
      ->doItTaxonomyVocabularyCreate()
      ->doItTaxonomyVocabularyRole()
      ->doItTaxonomyVocabularyMigration()
      ->doItTaxonomyVocabularyBehat();

    return $this;
  }

  /**
   * @return $this
   */
  protected function doItTaxonomyVocabularyCreate() {
    $this->triggerSpell($this->taxonomyVocabularyCreateSpell, $this->context['create']);

    return $this;
  }

  /**
   * @return $this
   */
  protected function doItTaxonomyVocabularyRole() {
    $this->triggerSpell($this->taxonomyVocabularyRoleSpell, $this->context['role']);
    return $this;
  }

  /**
   * @return $this
   */
  protected function doItTaxonomyVocabularyMigration() {
    $this->triggerSpell($this->taxonomyVocabularyMigrationSpell, $this->context['migration']);

    return $this;
  }

  /**
   * @return $this
   */
  protected function doItTaxonomyVocabularyBehat() {
    $this->triggerSpell($this->taxonomyVocabularyBehatSpell, $this->context['behat']);

    return $this;
  }

  protected function triggerSpell(SpellInterface $spell, array $config) {
    if (!empty($config['enabled'])) {
      $spell->abracadabra($config['settings']);
    }

    return $this;
  }

  /**
   * @return \Drupal\devel_wizard\SpellInterface[]
   */
  protected function getIncludedSpells(): array {
    return [
      'create' => $this->taxonomyVocabularyCreateSpell,
      'role' => $this->taxonomyVocabularyRoleSpell,
      'migration' => $this->taxonomyVocabularyMigrationSpell,
      'behat' => $this->taxonomyVocabularyBehatSpell,
    ];
  }

}
