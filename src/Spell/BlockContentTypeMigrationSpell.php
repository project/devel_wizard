<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Utility\Random;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\devel_wizard\SpellInterface;
use Symfony\Component\Filesystem\Filesystem;
use Webmozart\PathUtil\Path;

class BlockContentTypeMigrationSpell extends ConfigEntityMigrationSpellBase {

  /**
   * {@inheritdoc}
   */
  protected string $id = 'devel_wizard_block_content_type_migration';

  /**
   * {@inheritdoc}
   */
  protected string $provider = 'block_content';

  protected string $configEntityTypeId = 'block_content_type';

  protected string $contentEntityTypeId = 'block_content';

  protected SpellInterface $moduleSpell;

  protected UuidInterface $uuid;

  protected string $sitePath;

  protected Random $random;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    MessengerInterface $messenger,
    LoggerChannelInterface $logger,
    TwigEnvironment $twig,
    ModuleExtensionList $moduleList,
    ModuleHandlerInterface $moduleHandler,
    ModuleInstallerInterface $moduleInstaller,
    SpellInterface $moduleSpell,
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    EntityTypeManagerInterface $entityTypeManager,
    UuidInterface $uuid,
    string $sitePath = 'sites/default',
    ?Filesystem $fs = NULL,
    ?Random $random = NULL
  ) {
    $this->moduleSpell = $moduleSpell;
    $this->uuid = $uuid;
    $this->sitePath = $sitePath;
    $this->random = $random ?: new Random();

    parent::__construct(
      $messenger,
      $logger,
      $twig,
      $moduleList,
      $moduleHandler,
      $moduleInstaller,
      $configFactory,
      $typedConfigManager,
      $entityTypeManager,
      $fs,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Block content type - migration');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Creates a migration definitions and data sources for an existing Block content type');
  }

  public function applyDefaultValues(array $settings): array {
    $settings = parent::applyDefaultValues($settings);
    assert(!empty($settings['machine_name']), 'machine_name is required');
    $machineName = $settings['machine_name'];

    return array_replace_recursive(
      [
        'active' => TRUE,
        'machine_name' => $machineName,
        'migration_module' => 'app_dc',
        'migration_group' => 'app_dummy',
        'dummy_entities' => [
          'short' => [
            'active' => TRUE,
            'str_len' => 10,
          ],
          'long' => [
            'active' => TRUE,
            'str_len' => 255,
          ],
        ],
      ],
      $settings,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function abracadabra(array $settings) {
    $this->context = $settings;

    $this
      ->composerRequire()
      ->createModules()
      ->createMigration();
  }

  protected function composerRequire() {
    $this->requiredDevPackages['drupal/migrate_tools'] = '^5.0';
    $this->requiredDevPackages['drupal/migrate_plus'] = '^5.0';
    $this->requiredDevPackages['drupal/migrate_dc'] = '1.x-dev';

    $this->installComposerPackages('prod', $this->getRequiredPackages());
    $this->installComposerPackages('dev', $this->getRequiredDevPackages());

    return $this;
  }

  protected function createModules() {
    $machineName = $this->context['migration_module'];
    if ($this->moduleList->exists($machineName)) {
      return $this;
    }

    $subSettings = [
      'machine_name' => $machineName,
      'info.yml' => [
        'dependencies' => [
          'migrate_dc:migrate_dc',
        ],
      ],
    ];
    $subSettings = $this->moduleSpell->applyDefaultValues($subSettings);
    $errors = $this->moduleSpell->validate($subSettings);
    if ($errors->count()) {
      throw new \Exception($errors->get(0)->getMessage());
    }

    $this->moduleSpell->abracadabra($subSettings);
    drupal_flush_all_caches();

    return $this;
  }

  protected function createMigration() {
    $this->messenger()->addStatus($this->t(
      'Create migration for @block_content_type Block content type',
      [
        '@block_content_type' => $this->context['machine_name'],
      ],
    ));

    return $this
      ->createMigrationGroupYml()
      ->createMigrationDefinitionYml()
      ->createMigrationSourceYml();
  }

  protected function createMigrationGroupYml() {
    $moduleName = $this->context['migration_module'];
    $modulePath = $this->getModulePath($moduleName);
    $migrationGroup = $this->context['migration_group'];

    $fileName = Path::join(
      $modulePath,
      'config',
      'install',
      "migrate_plus.migration_group.{$migrationGroup}.yml",
    );

    $values = [
      'migration_group' => $migrationGroup,
      'migration_module' => $moduleName,
    ];

    $templatePath = "@devel_wizard/migration/devel_wizard.migration_group.yml.twig";

    $rendered = $this->twig->render($templatePath, $values);

    $this->dumpFile($fileName, $rendered);

    return $this;
  }

  protected function createMigrationDefinitionYml() {
    $blockContentType = $this->context['machine_name'];
    $moduleName = $this->context['migration_module'];
    $modulePath = $this->getModulePath($moduleName);
    $migrationGroup = $this->context['migration_group'];
    $migrationId = "{$migrationGroup}__block_content__{$blockContentType}";

    $fileName = Path::join(
      $modulePath,
      'config',
      'install',
      "migrate_plus.migration.{$migrationId}.yml",
    );

    $values = [
      'entity_type' => $this->contentEntityTypeId,
      'migration_id' => $migrationId,
      'bundle' => $blockContentType,
      'migration_group' => $migrationGroup,
    ];

    $templatePath = "@devel_wizard/migration/devel_wizard.migration_definition.{$this->contentEntityTypeId}.yml.twig";

    $rendered = $this->twig->render($templatePath, $values);

    $this->dumpFile($fileName, $rendered);

    return $this;
  }

  protected function createMigrationSourceYml() {
    $blockContentType = $this->context['machine_name'];
    $migrationGroup = $this->context['migration_group'];

    $fileName = Path::join(
      '..',
      $this->sitePath,
      'content',
      $migrationGroup,
      'block_content',
      "{$blockContentType}.yml",
    );

    $sourceData = [];
    foreach ($this->context['dummy_entities'] as $name => $dummy) {
      if (!$dummy['active']) {
        continue;
      }

      $sourceData["my_{$blockContentType}_{$name}"] = [
        'uuid' => $this->uuid->generate(),
        'info' => "My {$blockContentType} {$name}",
      ];
    }

    $this->dumpFile($fileName, Yaml::encode($sourceData));

    return $this;
  }

}
