<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Core\Form\FormStateInterface;
use Drupal\devel_wizard\Utils;

abstract class ConfigEntityMigrationSpellBase extends ConfigEntitySpellBase {

  /**
   * {@inheritdoc}
   */
  public function settingsFormBuild(array $parents, FormStateInterface $formState): array {
    $element = [
      'machine_name' => [
        '#type' => 'select',
        '#required' => TRUE,
        '#options' => [],
        '#empty_option' => $this->t('- Select -'),
        '#default_value' => '',
      ],
      'migration_module' => [
        '#title' => $this->t('Migration module'),
        '#type' => 'textfield',
        '#default_value' => $this->context['migration_module'] ?? 'app_dc',
        '#description' => $this->t('A custom module machine-name where migration definition YAML files will be placed, under {machine-name}/config/install folder.'),
        '#autocomplete_route_name' => 'devel_wizard.module.autocomplete',
      ],
      'migration_group' => [
        '#title' => $this->t('Migration group'),
        '#type' => 'textfield',
        '#default_value' => $this->context['migration_group'] ?? 'app_dummy',
        '#description' => $this->t('A name for the migration group eg: app_dummy, app_default.'),
      ],
    ];

    $configStorage = $this->getConfigStorage();
    if (!$configStorage) {
      $this->messageMissingModules($this->getRequiredModules());

      return $element;
    }

    $element['machine_name']['#title'] = $configStorage->getEntityType()->getLabel();
    $element['machine_name']['#options'] = Utils::configEntityChoices($configStorage->loadMultiple());
    $element['machine_name']['#description'] = $this->t(
      'The machine-name of the @entity_type.label to create the migration for.',
      [
        '@entity_type.label' => $configStorage->getEntityType()->getLabel(),
      ],
    );

    return $element;
  }

}
