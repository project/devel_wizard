<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Composer\InstalledVersions as ComposerInstalledVersions;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\Exception\UnknownExtensionException;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Component\Serialization\Yaml as Yaml;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Drupal\user\RoleInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Yaml\Yaml as SymfonyYaml;
use Webmozart\PathUtil\Path;

abstract class SpellBase implements SpellInterface, LoggerAwareInterface {

  use LoggerAwareTrait;

  use MessengerTrait;

  use StringTranslationTrait;

  /**
   * Machine-name of this spell.
   *
   * @var string
   */
  protected string $id = '';

  protected Filesystem $fs;

  protected TwigEnvironment $twig;

  protected ModuleExtensionList $moduleList;

  protected ModuleHandlerInterface $moduleHandler;

  protected ModuleInstallerInterface $moduleInstaller;

  protected ConfigFactoryInterface $configFactory;

  protected TypedConfigManagerInterface $typedConfigManager;

  protected EntityTypeManagerInterface $entityTypeManager;

  protected int $jsonEncodeFlags = \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE;

  /**
   * @todo Rename to settings.
   */
  protected array $context = [];

  protected string $phpExecutable = '';

  protected string $composerExecutable = 'composer';

  /**
   * Composer requirements.
   *
   * Package name as key, value is the version number.
   *
   * @var string[]|false[]
   */
  protected array $requiredPackages = [];

  protected function getRequiredPackages(): array {
    return $this->requiredPackages;
  }

  /**
   * Adds items to the ::$requiredPackages array regardless of the configuration.
   *
   * @return $this
   */
  protected function initRequiredPackages() {
    return $this;
  }

  protected array $requiredDevPackages = [];

  protected function getRequiredDevPackages(): array {
    return $this->requiredDevPackages;
  }

  /**
   * Adds items to the ::$requiredDevPackages array regardless of the configuration.
   *
   * @return $this
   */
  protected function initRequiredDevPackages() {
    return $this;
  }

  /**
   * @var bool[]
   */
  protected array $requiredModules = [];

  public function getRequiredModules(): array {
    return $this->requiredModules;
  }

  /**
   * @return $this
   */
  protected function initRequiredModules() {
    return $this;
  }

  public function __construct(
    MessengerInterface $messenger,
    LoggerChannelInterface $logger,
    TwigEnvironment $twig,
    ModuleExtensionList $moduleList,
    ModuleHandlerInterface $moduleHandler,
    ModuleInstallerInterface $moduleInstaller,
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    EntityTypeManagerInterface $entityTypeManager,
    ?Filesystem $fs = NULL
  ) {
    $this->fs = $fs ?: new Filesystem();
    $this->setMessenger($messenger);
    $this->setLogger($logger);
    $this->twig = $twig;
    $this->moduleList = $moduleList;
    $this->moduleHandler = $moduleHandler;
    $this->moduleInstaller = $moduleInstaller;
    $this->configFactory = $configFactory;
    $this->typedConfigManager = $typedConfigManager;
    $this->entityTypeManager = $entityTypeManager;

    $this
      ->initRequiredPackages()
      ->initRequiredDevPackages()
      ->initRequiredModules();
  }

  public function id(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function help(): TranslatableMarkup {
    return $this->description();
  }

  abstract public function description(): TranslatableMarkup;

  /**
   * {@inheritdoc}
   */
  public function configName(): string {
    return sprintf('devel_wizard.spell.%s.settings', $this->id());
  }

  /**
   * {@inheritdoc}
   */
  public function config(array $values = []): Config {
    return $this
      ->configFactory
      ->getEditable($this->configName())
      ->setData($values);
  }

  /**
   * {@inheritdoc}
   */
  public function applyDefaultValues(array $settings): array {
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(array $values): ConstraintViolationListInterface {
    return $this
      ->typedConfigManager
      ->createFromNameAndData($this->configName(), $values)
      ->validate();
  }

  /**
   * @return $this
   */
  protected function dumpFile(string $fileName, string $fileContent) {
    $this->fs->mkdir(Path::getDirectory($fileName));
    $this->fs->dumpFile($fileName, $fileContent);
    $this->messageFileCreate($fileName);

    return $this;
  }

  protected function installComposerPackages(string $mode, array $packages) {
    $installedPackages = ComposerInstalledVersions::getInstalledPackages();

    $packagesToInstall = array_intersect_key(
      $packages,
      array_flip(array_keys($packages, TRUE, FALSE)),
    );

    if (!$packagesToInstall) {
      return $this;
    }

    $missingPackages = array_diff_key(
      $packagesToInstall,
      array_flip($installedPackages),
    );

    $alreadyInstalledPackages = array_intersect_key(
      $packages,
      array_flip($installedPackages),
    );

    $messenger = $this->messenger();
    $args = [
      '@spell' => $this->id(),
      '@packages.required' => implode(', ', array_keys($packages)),
      '@packages.missing' => implode(', ', array_keys($missingPackages)),
      '@packages.already' => implode(', ', array_keys($alreadyInstalledPackages)),
    ];

    $messenger->addStatus($this->t(
      '@spell - install Composer packages<br />required: @packages.required<br />missing: @packages.missing<br />already: @packages.already',
      $args,
    ));

    if (!$missingPackages) {
      return $this;
    }

    $command = [
      $this->composerExecutable,
      'require',
    ];

    if ($mode === 'dev') {
      $command[] = '--dev';
    }

    foreach ($missingPackages as $package => $version) {
      $command[] = "$package:$version";
    }

    // @todo Detect project root.
    $projectRoot = '..';
    // @todo Dependency injection.
    $processFactory = \Drupal::getContainer()->get('devel_wizard.shell_process_factory');
    $process = $processFactory->createInstance($command, $projectRoot, getenv());
    $process->run();

    $this->reFetchDependencies();

    $args = [
      '@spell' => $this->id(),
      '@projectRoot' => $projectRoot,
      '@cwd' => getcwd(),
      '@wd' => $process->getWorkingDirectory(),
      '@command' => $process->getCommandLine(),
      '@exitCode' => $process->getExitCode(),
      '@stdOutput' => $process->getOutput(),
      '@stdError' => $process->getErrorOutput(),
    ];

    if ($process->getExitCode()) {
      $messenger->addError($this->t(
        '@spell - shell command failed:<hr />project root: @projectRoot<hr />cwd: @cwd<hr />wd: @wd<hr />command: @command<hr />exit code: @exitCode<hr />stdOutput: <pre>@stdOutput</pre><hr />stdError: <pre>@stdError</pre>',
        $args,
      ));

      return $this;
    }

    $messenger->addStatus($this->t(
      '@spell - shell command success:<hr />project root: @projectRoot<hr />cwd: @cwd<hr />command: @command<hr />exit code: @exitCode<hr />stdOutput: <pre>@stdOutput</pre><hr />stdError: <pre>@stdError</pre>',
      $args,
    ));

    return $this;
  }

  /**
   * @return $this
   */
  protected function ensureModuleExists(array $settings) {
    if ($this->moduleList->exists($settings['machine_name'])) {
      return $this;
    }

    $settings = $this->moduleSpell->applyDefaultValues($settings);
    // @todo Validation?
    $this->moduleSpell->abracadabra($settings);

    return $this;
  }

  protected function ensureModuleInstalled(string $moduleName): bool {
    $messenger = $this->messenger();
    $args = [
      '@spell' => $this->id(),
      '%module' => $moduleName,
    ];

    $installedModules = $this->moduleList->getAllInstalledInfo();
    if (isset($installedModules[$moduleName])) {
      $messenger->addStatus($this->t('@spell - module %module already installed', $args));

      return TRUE;
    }

    $this->installModules([$moduleName => TRUE]);

    return TRUE;
  }

  protected function installModules(array $moduleNames): bool {
    $moduleNames = array_keys($moduleNames, TRUE, FALSE);
    if (!$moduleNames) {
      return TRUE;
    }

    try {
      $this->moduleInstaller->install($moduleNames, TRUE);
      $this->reFetchDependencies();

      return TRUE;
    }
    catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
    }

    return FALSE;
  }

  protected function reFetchDependencies() {
    // @todo PHPCS should complain about this line, but it doesn't.
    $container = \Drupal::getContainer();
    $this->moduleList = $container->get('extension.list.module');
    $this->moduleList->reset();

    $this->moduleHandler = $container->get('module_handler');

    $this->moduleInstaller = $container->get('module_installer');

    $this->entityTypeManager = $container->get('entity_type.manager');
    $this->entityTypeManager->clearCachedDefinitions();
    $this->entityTypeManager->useCaches(FALSE);
  }

  protected function getConfigTemplate(string $name): array {
    $path = Path::Join(
      $this->moduleHandler->getModule('devel_wizard')->getPath(),
      'config',
      'template',
      $name,
    );

    return Yaml::decode(file_get_contents($path));
  }

  /**
   * Replaces elements in the $fileName YAML file.
   *
   * @return $this
   *
   * @todo Move this into Utils.
   */
  protected function ymlFileReplace(string $fileName, array $entries, array $parents = []) {
    if (!$entries) {
      return $this;
    }

    // @todo Comment friendly way.
    $content = $this->fs->exists($fileName) ? file_get_contents($fileName) : '{}';
    $root = Yaml::decode($content);
    if (!NestedArray::keyExists($root, $parents)) {
      NestedArray::setValue($root, $parents, []);
    }
    $data =& NestedArray::getValue($root, $parents);
    $countBefore = count($data);
    foreach ($entries as $entryId => $entry) {
      $data[$entryId] = $entry;
    }

    $this->fs->dumpFile($fileName, SymfonyYaml::dump($root, 2, 2));

    $numOfAdded = count($data) - $countBefore;
    $numOfUpdated = count($entries) - $numOfAdded;
    $this->messenger()->addStatus($this->t(
      '@spell - <a href="@file-href">@file-name</a> - added: @num_of_added; updated: @num_of_updated;',
      [
        '@spell' => $this->id(),
        '@file-href' => Utils::openFileUrl($fileName),
        '@file-name' => $fileName,
        '@num_of_added' => $numOfAdded,
        '@num_of_updated' => $numOfUpdated,
      ],
    ));

    return $this;
  }

  protected function messageMissingModules(array $modules) {
    $missingModules = array_keys($modules, TRUE, TRUE);
    if (!$missingModules) {
      return $this;
    }

    $this->messenger()->addError($this->formatPlural(
      count($missingModules),
      'The following module has to be enabled: @modules',
      'The following modules have to be enabled: @modules',
      [
        '@modules' => implode(', ', $missingModules),
      ],
    ));

    return $this;
  }

  /**
   * @todo Rename this method to "messageFilesystemEntryCreate".
   * Not to be confused with File entity create.
   */
  protected function messageFileCreate(string $fileName) {
    $this->messenger()->addStatus($this->t(
      '@spell - file has been created: <a href="@file.href">@file.name</a>',
      [
        '@spell' => $this->id(),
        '@file.href' => Utils::openFileUrl($fileName),
        '@file.name' => $fileName,
      ],
    ));

    return $this;
  }

  protected function messageConfigEntityCreate(ConfigEntityInterface $entity) {
    $editable = $entity->getEntityType()->hasLinkTemplate('edit-form');
    $this->messenger()->addStatus($this->t(
      '@spell - @entity_type.label has been created: <a href="@entity_type.url.canonical">@bundle.label</a>',
      [
        '@spell' => $this->id(),
        '@entity_type.label' => $entity->getEntityType()->getLabel(),
        '@entity_type.url.canonical' => $editable ? $entity->toUrl()->toString() : '#',
        '@bundle.label' => $entity->label(),
      ],
    ));

    return $this;
  }

  protected function messageConfigEntityExists(ConfigEntityInterface $entity) {
    $editable = $entity->getEntityType()->hasLinkTemplate('edit-form');
    $this->messenger()->addStatus($this->t(
      '@spell - @entity_type already exists: <a href="@href-canonical">@label</a>',
      [
        // @todo Uniformize the placeholder names.
        '@spell' => $this->id(),
        '@entity_type' => $entity->getEntityType()->getLabel(),
        '@href-canonical' => $editable ? $entity->toUrl()->toString() : '#',
        '@label' => $entity->label(),
      ],
    ));

    return $this;
  }

  protected function messageConfigEntityUpdated(ConfigEntityInterface $entity) {
    $editable = $entity->getEntityType()->hasLinkTemplate('edit-form');
    $this->messenger()->addStatus($this->t(
      '@spell - @entity_type updated: <a href="@href-canonical">@label</a>',
      [
        // @todo Uniformize the placeholder names.
        '@spell' => $this->id(),
        '@entity_type' => $entity->getEntityType()->getLabel(),
        '@href-canonical' => $editable ? $entity->toUrl()->toString() : '#',
        '@label' => $entity->label(),
      ],
    ));

    return $this;
  }

  /**
   * Use this function only for newly created modules.
   */
  protected function getModulePath(string $moduleName): string {
    try {
      return $this->moduleList->getPath($moduleName);
    }
    catch (UnknownExtensionException $e) {
      // Nothing to do.
    }

    $candidates = [
      Path::join('modules', 'custom', $moduleName),
      Path::join('modules', 'contrib', $moduleName),
    ];

    foreach ($candidates as $candidate) {
      if ($this->fs->exists("$candidate/$moduleName.info.yml")) {
        return $candidate;
      }
    }

    throw new UnknownExtensionException("The module '$moduleName' does not exist.");
  }

  protected function ensureUserRole(array $values): RoleInterface {
    $storage = $this->entityTypeManager->getStorage('user_role');
    /** @var \Drupal\user\RoleInterface $userRole */
    $userRole = $storage->load($values['id']);
    if ($userRole) {
      $this->messageConfigEntityExists($userRole);

      return $userRole;
    }

    $userRole = $storage->create($values);
    $userRole->save();
    $this->messageConfigEntityCreate($userRole);

    return $userRole;
  }

}
