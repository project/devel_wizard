<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Core\StringTranslation\TranslatableMarkup;

class TaxonomyVocabularyRoleSpell extends ConfigEntityRoleSpellBase {

  /**
   * {@inheritdoc}
   */
  protected string $id = 'devel_wizard_taxonomy_vocabulary_role';

  protected string $provider = 'taxonomy';

  protected string $configEntityTypeId = 'taxonomy_vocabulary';

  protected string $contentEntityTypeId = 'taxonomy_term';

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Taxonomy vocabulary - role');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Creates a new user role which grants full control over a specific Taxonomy vocabulary');
  }

  /**
   * {@inheritdoc}
   */
  public function abracadabra(array $settings) {
    $this->context = $settings;

    $this->enableFineGrainPermissionHandlerModule();
    parent::abracadabra($settings);

    return $this;
  }

  protected function enableFineGrainPermissionHandlerModule() {
    $this->installComposerPackages('prod', ['drupal/taxonomy_access_fix' => '^3.1']);
    $this->ensureModuleInstalled('taxonomy_access_fix');

    return $this;
  }

  /**
   * @param array $settings
   *
   * @return bool[]
   */
  protected function getDefaultPermissions(array $settings): array {
    $machineName = $settings['machine_name'];

    return [
      'access administration pages' => TRUE,
      'view the administration theme' => TRUE,
      "create terms in $machineName" => TRUE,
      "edit terms in $machineName" => TRUE,
      "delete terms in $machineName" => TRUE,
      "reorder terms in $machineName" => TRUE,
      "view terms in $machineName" => TRUE,
    ];
  }

}
