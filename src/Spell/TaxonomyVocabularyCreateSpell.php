<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\taxonomy\Entity\Vocabulary;

class TaxonomyVocabularyCreateSpell extends ConfigEntitySpellBase {

  protected string $id = 'devel_wizard_taxonomy_vocabulary_create';

  /**
   * {@inheritdoc}
   */
  protected string $provider = 'taxonomy';

  protected string $configEntityTypeId = 'taxonomy_vocabulary';

  protected string $contentEntityTypeId = 'taxonomy_term';

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Taxonomy vocabulary - create');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Creates a new Taxonomy vocabulary without any extra');
  }

  public function applyDefaultValues(array $settings): array {
    $settings = parent::applyDefaultValues($settings);
    assert(!empty($settings['values']['vid']), 'vid is required');
    $machineName = $settings['values']['vid'];

    return array_replace_recursive(
      [
        'values' => [
          'vid' => $machineName,
          'name' => $machineName,
          'description' => 'Useless description',
          'weight' => 1,
        ],
      ],
      $settings,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormBuild(array $parents, FormStateInterface $formState): array {
    return [
      'values' => [
        '#tree' => TRUE,
        'vid' => [
          '#type' => 'machine_name',
          '#default_value' => '',
          '#maxlength' => 64,
          '#description' => $this->t('A unique name for this item. It must only contain lowercase letters, numbers, and underscores.'),
          '#machine_name' => [
            // @todo The taxonomy module maybe not enabled yet.
            'exists' => [Vocabulary::class, 'load'],
            'standalone' => TRUE,
          ],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function abracadabra(array $settings) {
    $this->context = $settings;
    $this->createConfigInstance($this->context['values']);

    return $this;
  }

}
