<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Core\Form\FormStateInterface;
use Drupal\devel_wizard\Utils;
use Drupal\user\RoleInterface;

abstract class ConfigEntityRoleSpellBase extends ConfigEntitySpellBase {

  /**
   * {@inheritdoc}
   */
  public function applyDefaultValues(array $settings): array {
    $settings = parent::applyDefaultValues($settings);
    assert(!empty($settings['machine_name']), 'machine_name is required');
    $machineName = $settings['machine_name'];

    $settings = array_replace_recursive(
      [
        'user_role' => [
          'active' => TRUE,
          'values' => [
            'status' => TRUE,
            'id' => "{$machineName}_admin",
            'label' => "{$machineName} administrator",
            'weight' => 10,
            'is_admin' => FALSE,
            'permissions' => $this->getDefaultPermissions($settings),
          ],
        ],
        'role_delegation' => [
          'active' => TRUE,
          'roles' => [],
        ],
      ],
      $settings,
    );

    $settings['user_role']['values']['permissions'] = array_keys(
      $settings['user_role']['values']['permissions'],
      TRUE,
      TRUE,
    );

    return $settings;
  }

  abstract protected function getDefaultPermissions(array $settings): array;

  /**
   * {@inheritdoc}
   */
  public function settingsFormBuild(array $parents, FormStateInterface $formState): array {
    $element = [
      'machine_name' => [
        '#type' => 'select',
        '#required' => TRUE,
        '#options' => [],
        '#empty_value' => '',
        '#empty_option' => $this->t('- Select -'),
        '#default_value' => '',
      ],
    ];

    $configStorage = $this->getConfigStorage();
    if (!$configStorage) {
      $this->messageMissingModules($this->getRequiredModules());

      return $element;
    }

    $element['machine_name']['#title'] = $configStorage->getEntityType()->getLabel();
    $element['machine_name']['#options'] = Utils::configEntityChoices($configStorage->loadMultiple());
    $element['machine_name']['#description'] = $this->t(
      'The machine name of the @entity_type.label to create the new role for.',
      [
        '@entity_type.label' => $configStorage->getEntityType()->getLabel(),
      ],
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function abracadabra(array $settings) {
    $this->context = $settings;

    return $this
      ->createUserRole()
      ->roleDelegation();
  }

  /**
   * @return $this
   */
  protected function createUserRole() {
    $args = [
      '@spell' => $this->id(),
      '@name' => $this->context['user_role']['values']['id'] ?? '',
    ];

    if (!$this->context['user_role']['active']) {
      $this->messenger()->addWarning($this->t(
        '@spell - user role creation is disabled',
        $args,
      ));

      return $this;
    }

    // @todo If the role is already exists with the given machine-name,
    // then throw an error or update the permissions.
    $storage = $this->entityTypeManager->getStorage('user_role');
    /** @var \Drupal\user\Entity\Role $role */
    $role = $storage->create($this->context['user_role']['values']);
    $role->save();

    $this->updatePermissions($role);
    $this->messageConfigEntityCreate($role);

    return $this;
  }

  /**
   * @return $this
   */
  protected function roleDelegation() {
    $messenger = $this->messenger();
    $args = [
      '@spell' => $this->id(),
      '%module' => 'role_delegation',
    ];

    if (!$this->context['role_delegation']['active']) {
      $messenger->addWarning($this->t(
        '@spell - role_delegation support is disabled',
        $args,
      ));

      return $this;
    }

    $this->installComposerPackages('prod', ['drupal/role_delegation' => '^1.1']);
    if (!$this->ensureModuleInstalled('role_delegation')) {
      // @todo Message about what went wrong.
      return $this;
    }

    $userRoleStorage = $this->entityTypeManager->getStorage('user_role');
    $bundleAdminRoleId = $this->context['user_role']['values']['id'];
    $bundleAdminRole = $userRoleStorage->load($bundleAdminRoleId);
    $args['@user_role_id'] = $bundleAdminRoleId;
    if (!$bundleAdminRole) {
      $messenger->addWarning($this->t(
        '@spell - role_delegation can not be configured because role @user_role_id is missing',
        $args,
      ));

      return $this;
    }

    $roleDelegatorRole = $this->ensureUserRole([
      'status' => TRUE,
      'weight' => 10,
      // @todo Make this configurable.
      'id' => 'role_delegator',
      // @todo Translatable.
      'label' => 'Role delegator',
      'is_admin' => FALSE,
      'permissions' => [],
    ]);

    $roleDelegatorRole
      ->grantPermission("assign {$bundleAdminRoleId} role")
      ->save();

    $this->messageConfigEntityUpdated($roleDelegatorRole);

    return $this;
  }

  /**
   * Update permissions for the role if needed.
   *
   * @param \Drupal\user\RoleInterface $role
   *   The role we want to update permissions for.
   *
   * @return $this
   */
  protected function updatePermissions(RoleInterface $role) {
    return $this;
  }

}
