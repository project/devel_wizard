<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Core\Form\FormStateInterface;
use Drupal\devel_wizard\Utils;

abstract class ConfigEntityBehatSpellBase extends ConfigEntitySpellBase {

  public function applyDefaultValues(array $settings): array {
    $settings = parent::applyDefaultValues($settings);
    assert(!empty($settings['machine_name']), 'machine_name is required');
    $machineName = $settings['machine_name'];

    return array_replace_recursive(
      [
        'machine_name' => $machineName,
        // @todo Make this path configurable or autodetect based on the behat.yml.
        'features_dir' => '../tests/behat/features',
      ],
      $settings,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormBuild(array $parents, FormStateInterface $formState): array {
    $element = [
      'machine_name' => [
        '#type' => 'select',
        '#title' => $this->t('Bundle'),
        '#required' => TRUE,
        '#options' => [],
        '#empty_option' => $this->t('- Select -'),
        '#default_value' => '',
      ],
    ];

    $configStorage = $this->getConfigStorage();
    if (!$configStorage) {
      $this->messageMissingModules($this->getRequiredModules());

      return $element;
    }

    $configEntityType = $this->getConfigEntityType();

    $element['machine_name']['#title'] = $configEntityType->getLabel();
    $element['machine_name']['#options'] = Utils::configEntityChoices($configStorage->loadMultiple());
    $element['machine_name']['#description'] = $this->t(
      'The machine-name of the @entity_type.label to create the Behat tests for.',
      [
        '@entity_type.label' => $configEntityType->getLabel(),
      ],
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function abracadabra(array $settings) {
    $this->context = $settings;
    $this
      ->installComposerPackages('prod', $this->getRequiredPackages())
      ->installComposerPackages('dev', $this->getRequiredDevPackages())
      ->installModules($this->getRequiredModules());
    $this->generateTests();
  }

  /**
   * @return $this
   */
  abstract protected function generateTests();

}
