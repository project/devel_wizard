<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\node\Entity\NodeType;

class NodeTypeCreateSpell extends ConfigEntitySpellBase {

  protected string $id = 'devel_wizard_node_type_create';

  /**
   * {@inheritdoc}
   */
  protected string $provider = 'node';

  protected string $configEntityTypeId = 'node_type';

  protected string $contentEntityTypeId = 'node';

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Content type - create');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Creates a new Content type without any extra');
  }

  public function applyDefaultValues(array $settings): array {
    $settings = parent::applyDefaultValues($settings);
    assert(!empty($settings['values']['type']), 'type is required');
    $machineName = $settings['values']['type'];

    return array_replace_recursive(
      [
        'values' => [
          'name' => $machineName,
          'type' => $machineName,
          'description' => 'Useless description',
          'help' => 'Useless help',
          'new_revision' => TRUE,
          'preview_mode' => 1,
          'display_submitted' => FALSE,
        ],
      ],
      $settings,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormBuild(array $parents, FormStateInterface $formState): array {
    return [
      'values' => [
        '#tree' => TRUE,
        'type' => [
          '#type' => 'machine_name',
          '#default_value' => '',
          '#maxlength' => 64,
          '#description' => $this->t('A unique name for this item. It must only contain lowercase letters, numbers, and underscores.'),
          '#machine_name' => [
            // @todo The node module maybe not enabled yet.
            'exists' => [NodeType::class, 'load'],
            'standalone' => TRUE,
          ],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function abracadabra(array $settings) {
    $this->context = $settings;
    $this->createConfigInstance($this->context['values']);

    return $this;
  }

}
