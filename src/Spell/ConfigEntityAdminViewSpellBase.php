<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\devel_wizard\Utils;
use Stringy\StaticStringy;
use Webmozart\PathUtil\Path;

/**
 * Creates a page with views for administrators to list content instances for a specific bundle.
 *
 * @package Drupal\devel_wizard\Spell
 */
abstract class ConfigEntityAdminViewSpellBase extends ConfigEntitySpellBase {

  protected ?ConfigEntityStorageInterface $viewStorage = NULL;

  protected UuidInterface $uuid;

  protected string $primaryTabPath = '';

  protected string $bundleIdInConfigTemplate = 'article';

  /**
   * {@inheritdoc}
   */
  protected function initRequiredModules() {
    $this->requiredModules['views'] = TRUE;

    return parent::initRequiredModules();
  }

  public function settingsFormBuild(array $parents, FormStateInterface $formState): array {
    $configStorage = $this->getConfigStorage();
    if (!$configStorage) {
      $this->messageMissingModules($this->getRequiredModules());

      return [];
    }

    $configEntityType = $this->getConfigEntityType();
    $machineNamePattern = '[a-z][a-z0-9_]*';

    return [
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => $this->t('Admin view'),
      'machine_name' => [
        '#type' => 'select',
        '#required' => TRUE,
        '#title' => $configEntityType->getLabel(),
        '#default_value' => '',
        '#empty_option' => $this->t('- Select -'),
        '#empty_value' => '',
        '#options' => Utils::configEntityChoices($configStorage->loadMultiple()),
        '#description' => $this->t('The %type you want to create the admin view for.', [
          '%type' => $configEntityType->getLabel(),
        ]),
      ],
      'module' => [
        '#tree' => TRUE,
        '#type' => 'container',
        'machine_name' => [
          '#type' => 'textfield',
          '#required' => TRUE,
          '#title' => $this->t('Module machine-name'),
          '#description' => $this->t('Machine-name of the module to put the new code files into. If not exists then it will be created in the modules/custom directory'),
          '#default_value' => 'app_core',
          '#pattern' => $machineNamePattern,
          '#autocomplete_route_name' => 'devel_wizard.module.autocomplete',
        ],
      ],
    ];
  }

  protected function getSecondaryBundleTabView(): array {
    $contentEntityType = $this->getContentEntityType();
    $configEntityType = $this->getConfigEntityType();
    $configPrefix = $configEntityType->getConfigPrefix();
    $bundleKey = $contentEntityType->getKey('bundle');

    $machineName = $this->context['machine_name'];
    $selfPath = $this->moduleHandler->getModule('devel_wizard')->getPath();
    $fileName = "$selfPath/config/template/views.view.{$this->contentEntityTypeId}.bundle_admin.yml";
    $data = Yaml::decode(file_get_contents($fileName));

    $data['uuid'] = $this->uuid->generate();
    $data['id'] = "{$machineName}_admin";
    $data['label'] = "{$machineName}_admin";

    $key = array_search("{$this->provider}.{$configPrefix}.{$this->bundleIdInConfigTemplate}", $data['dependencies']);
    if ($key !== FALSE) {
      unset($data['dependencies'][$key]);
    }

    $data['dependencies'][] = "{$this->provider}.{$configPrefix}.{$machineName}";

    $data['display']['default']['display_options']['title'] = $machineName;
    $data['display']['overview']['display_options']['menu']['title'] = $machineName;
    $data['display']['default']['display_options']['filters'][$bundleKey]['value'] = [$machineName => $machineName];

    return $data;
  }

  /**
   * Helper method to determine if derivatives exist in a given module.
   *
   * @param string $moduleMachineName
   *   The module machine name we want to check if derivatives exist.
   * @param string $configEntityTypeId
   *   The config entity type id we want to check derivatives for.
   * @param string $derivativeType
   *   The type of derivative we want to check if they exist.
   *
   * @return bool
   *   Returns a boolean FALSE if there were no errors during the check.
   */
  protected function checkDerivativeExists(
    string $moduleMachineName,
    string $configEntityTypeId,
    string $derivativeType = ''
  ) {
    $modulePath = $this->getModulepath($moduleMachineName);
    $configEntityTypeName = StaticStringy::upperCamelize($configEntityTypeId);
    $moduleName = StaticStringy::upperCamelize($moduleMachineName);
    $derivativeClassName = "{$moduleName}{$configEntityTypeName}Local{$derivativeType}";
    $file = Path::join(
      $modulePath,
      'src',
      'Plugin',
      'Derivative',
      "$derivativeClassName.php",
    );
    // If the derivative already exist, do not override it.
    if (!$this->fs->exists($file)) {
      return FALSE;
    }
    else {
      // @todo Add warning message.
      // Warning message to the user, if the derivative already exists with the
      // same name in a different module.
      $configEntityType = $this->getConfigEntityType();
      $this
        ->messenger()
        ->addWarning($this->t(
          "The derivative type '%type' for the '%config_entity' already exists in the given module. There were no file system changes.",
          [
            '%type' => $derivativeType,
            '%config_entity' => $configEntityType->getLabel(),
          ],
        ));
      return TRUE;
    }
  }

  /**
   * Creates derivative definitions for the Admin View spell secondary tabs.
   *
   * @param string $configEntityTypeId
   *   The config entity type id the derivatives are created for.
   * @param array $derivativeTypes
   *   The type of derivatives created.
   *
   * @return $this
   *
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   */
  protected function createDerivatives(
    string $configEntityTypeId,
    array $derivativeTypes = ['action', 'task']
  ) {
    $settings = $this->context;
    $this->ensureModuleExists($settings['module']);
    $moduleMachineName = $settings['module']['machine_name'];
    $modulePath = $this->getModulePath($moduleMachineName);
    $moduleName = StaticStringy::upperCamelize($moduleMachineName);
    $configEntityTypeName = StaticStringy::upperCamelize($configEntityTypeId);
    foreach ($derivativeTypes as $derivativeType) {
      $ucDerivativeType = ucfirst($derivativeType);
      if ($this->checkDerivativeExists($moduleMachineName, $configEntityTypeId, $ucDerivativeType)) {
        continue;
      }
      $derivativeClassName = "{$moduleName}{$configEntityTypeName}Local{$ucDerivativeType}";
      $settings[$configEntityTypeId]['class'] = $derivativeClassName;
      $settings[$configEntityTypeId]['derivative']['namespace'] = "Drupal\\{$moduleMachineName}\\Plugin\\Derivative";
      $derivativeFileName = Path::join(
        $modulePath,
        'src',
        'Plugin',
        'Derivative',
        "{$derivativeClassName}.php",
      );
      $this->createDerivativeLinks($derivativeType, $derivativeClassName);
      $this->createDerivativePermissions();
      // @todo Static check?
      if ($configEntityTypeId == 'block_content_type') {
        $this->createDerivativeRouting();
      }
      // Create the Derivative classes.
      $this->dumpFile(
        $derivativeFileName,
        $this->twig->render(
          "@devel_wizard/derivative/{$derivativeType}/devel_wizard.{$configEntityTypeId}.class.php.twig",
          $settings
        ),
      );
    }

    return $this;
  }

  /**
   * Creates custom routing entries.
   */
  protected function createDerivativeRouting() {
    $settings = $this->context;
    $moduleMachineName = $settings['module']['machine_name'];
    $configEntityType = $this->getConfigEntityType();
    $contentEntityType = $this->getContentEntityType();
    $dashedContentEntityTypeId = StaticStringy::dasherize($contentEntityType->id());
    $modulePath = $this->getModulePath($moduleMachineName);
    $ymlFileName = Path::join(
      $modulePath,
      "{$moduleMachineName}.routing.yml",
    );
    $entries = [
      "{$moduleMachineName}.{$contentEntityType->id()}.collection" => [
        'path' => "/admin/content/{$dashedContentEntityTypeId}",
        'defaults' => [
          '_title' => $configEntityType->getLabel()->render(),
          '_entity_list' => $contentEntityType->id(),
        ],
        'requirements' => [
          // @todo Permission.
          '_permission' => 'access content',
        ],
      ],
    ];
    $this->ymlFileReplace($ymlFileName, $entries);
  }

  /**
   * Creates custom links.$derivativeType.yml definitions.
   *
   * @param string $derivativeType
   *   The derivative type 'action' or 'task'.
   * @param string $derivativeClassName
   *   The full derivative class name.
   */
  protected function createDerivativeLinks(
    string $derivativeType,
    string $derivativeClassName
  ): void {
    $settings = $this->context;
    $moduleMachineName = $settings['module']['machine_name'];
    $configEntityType = $this->getConfigEntityType();
    $ucDerivativeType = ucfirst($derivativeType);
    $modulePath = $this->getModulePath($moduleMachineName);
    $ymlFileName = Path::join(
      $modulePath,
      "{$moduleMachineName}.links.{$derivativeType}.yml",
    );
    $entries = [
      "{$moduleMachineName}_{$configEntityType->id()}_{$derivativeType}" => [
        'class' => "\Drupal\Core\Menu\Local{$ucDerivativeType}Default",
        'deriver' => "\Drupal\\{$moduleMachineName}\Plugin\Derivative\\{$derivativeClassName}",
      ],
    ];
    $this->ymlFileReplace($ymlFileName, $entries);
  }

  /**
   * Creates custom permission entries.
   */
  protected function createDerivativePermissions(): void {
    $settings = $this->context;
    $moduleMachineName = $settings['module']['machine_name'];
    $configEntityType = $this->getConfigEntityType();
    $modulePath = $this->getModulePath($moduleMachineName);
    $ymlFileName = Path::join(
      $modulePath,
      "{$moduleMachineName}.permissions.yml",
    );
    $entries = [
      "{$moduleMachineName}.{$configEntityType->id()}.overview" => [
        'title' => "Access to {$configEntityType->getLabel()} overview page",
        'description' => "Allows users to access to the {$configEntityType->getLabel()} overview page.",
      ],
    ];

    $this->ymlFileReplace($ymlFileName, $entries);
  }

}
