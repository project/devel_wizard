<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Drupal\node\Entity\NodeType;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class NodeTypeSpell extends ConfigEntitySpellBase {

  protected string $id = 'devel_wizard_node_type';

  protected string $provider = 'node';

  protected string $configEntityTypeId = 'node_type';

  protected string $contentEntityTypeId = 'node';

  protected NodeTypeCreateSpell $nodeTypeCreateSpell;

  protected NodeTypeRoleSpell $nodeTypeRoleSpell;

  protected NodeTypeMigrationSpell $nodeTypeMigrationSpell;

  protected NodeTypeBehatSpell $nodeTypeBehatSpell;

  protected NodeTypeAdminViewSpell $nodeTypeAdminViewSpell;

  public function __construct(
    MessengerInterface $messenger,
    LoggerChannelInterface $logger,
    TwigEnvironment $twig,
    ModuleExtensionList $moduleList,
    ModuleHandlerInterface $moduleHandler,
    ModuleInstallerInterface $moduleInstaller,
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    EntityTypeManagerInterface $entityTypeManager,
    NodeTypeCreateSpell $nodeTypeCreateSpell,
    NodeTypeRoleSpell $nodeTypeRoleSpell,
    NodeTypeMigrationSpell $nodeTypeMigrationSpell,
    NodeTypeBehatSpell $nodeTypeBehatSpell,
    NodeTypeAdminViewSpell $nodeTypeAdminViewSpell,
    ?Filesystem $fs = NULL
  ) {
    $this->nodeTypeCreateSpell = $nodeTypeCreateSpell;
    $this->nodeTypeRoleSpell = $nodeTypeRoleSpell;
    $this->nodeTypeMigrationSpell = $nodeTypeMigrationSpell;
    $this->nodeTypeBehatSpell = $nodeTypeBehatSpell;
    $this->nodeTypeAdminViewSpell = $nodeTypeAdminViewSpell;

    parent::__construct(
      $messenger,
      $logger,
      $twig,
      $moduleList,
      $moduleHandler,
      $moduleInstaller,
      $configFactory,
      $typedConfigManager,
      $entityTypeManager,
      $fs
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Content type - all in one');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Creates a Content type with all the bells and whistles');
  }

  public function applyDefaultValues(array $settings): array {
    $settings = parent::applyDefaultValues($settings);
    assert(!empty($settings['machine_name']), 'machine_name is required');

    $nodeTypeId = $settings['machine_name'];

    $settings = array_replace_recursive(
      [
        'create' => [
          'enabled' => TRUE,
          'settings' => [
            'values' => [
              'type' => $nodeTypeId,
            ],
          ],
        ],
        'admin_view' => [
          'enabled' => TRUE,
          'settings' => [
            'machine_name' => $nodeTypeId,
          ],
        ],
        'behat' => [
          'enabled' => TRUE,
          'settings' => [
            'machine_name' => $nodeTypeId,
          ],
        ],
        'migration' => [
          'enabled' => TRUE,
          'settings' => [
            'machine_name' => $nodeTypeId,
          ],
        ],
        'role' => [
          'enabled' => TRUE,
          'settings' => [
            'machine_name' => $nodeTypeId,
          ],
        ],
      ],
      $settings,
    );

    $settings['create']['settings'] = $this->nodeTypeCreateSpell->applyDefaultValues($settings['create']['settings']);
    $settings['role']['settings'] = $this->nodeTypeRoleSpell->applyDefaultValues($settings['role']['settings']);
    $settings['migration']['settings'] = $this->nodeTypeMigrationSpell->applyDefaultValues($settings['migration']['settings']);
    $settings['behat']['settings'] = $this->nodeTypeBehatSpell->applyDefaultValues($settings['behat']['settings']);
    $settings['admin_view']['settings'] = $this->nodeTypeAdminViewSpell->applyDefaultValues($settings['admin_view']['settings']);

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormBuild(array $parents, FormStateInterface $formState): array {
    $form = [
      '#type' => 'container',
      '#tree' => TRUE,
      'machine_name' => [
        '#type' => 'machine_name',
        '#required' => TRUE,
        '#title' => $this->t('Machine-readable name of the new Content type'),
        '#default_value' => '',
        '#maxlength' => 32,
        '#description' => $this->t('A unique name for this Content type. It must only contain lowercase letters, numbers, and underscores.'),
        '#machine_name' => [
          'exists' => [NodeType::class, 'load'],
          'standalone' => TRUE,
        ],
      ],
    ];

    foreach ($this->getIncludedSpells() as $key => $spell) {
      $subParents = $parents;
      $subParents[] = $key;

      $form[$key] = [
        '#type' => 'fieldset',
        '#title' => $spell->label(),
        '#tree' => TRUE,
        'enabled' => [
          '#title' => $this->t('Enabled'),
          '#default_value' => TRUE,
          '#type' => 'checkbox',
        ],
        'settings' => [
          '#type' => 'container',
          '#tree' => TRUE,
        ] + $spell->settingsFormBuild($subParents, $formState),
      ];

      $togglerSelector = sprintf(
        ':input[name="%s"]',
        Utils::inputName(array_merge($subParents, ['enabled'])),
      );
      $form[$key]['settings']['#states']['visible'][$togglerSelector] = ['checked' => TRUE];

      if ($key === 'create') {
        $form[$key]['enabled']['#access'] = FALSE;
      }

      if (isset($form[$key]['settings']['machine_name'])) {
        $form[$key]['settings']['machine_name']['#required'] = FALSE;
        $form[$key]['settings']['machine_name']['#access'] = FALSE;
      }

      if (isset($form[$key]['settings']['values']['type'])) {
        $form[$key]['settings']['values']['type']['#required'] = FALSE;
        $form[$key]['settings']['values']['type']['#access'] = FALSE;
      }
    }

    return $form;
  }

  public function validate(array $values): ConstraintViolationListInterface {
    $violations = $this
      ->typedConfigManager
      ->createFromNameAndData($this->configName(), $values)
      ->validate();

    $machineNameErrors = [
      'role.settings.machine_name',
      'migration.settings.machine_name',
      'behat.settings.machine_name',
      'admin_view.settings.machine_name',
    ];
    /** @var \Symfony\Component\Validator\ConstraintViolationInterface $violation */
    foreach ($violations as $id => $violation) {
      if (in_array($violation->getPropertyPath(), $machineNameErrors)) {
        $violations->remove($id);
      }
    }

    return $violations;
  }

  /**
   * {@inheritdoc}
   */
  public function abracadabra(array $settings) {
    $this->context = $settings;

    $this
      ->doItNodeTypeCreate()
      ->doItNodeTypeRole()
      ->doItNodeTypeMigration()
      ->doItNodeTypeBehat()
      ->doItNodeTypeAdminView();

    return $this;
  }

  /**
   * @return $this
   */
  protected function doItNodeTypeCreate() {
    $this->triggerSpell($this->nodeTypeCreateSpell, $this->context['create']);

    return $this;
  }

  /**
   * @return $this
   */
  protected function doItNodeTypeRole() {
    $this->triggerSpell($this->nodeTypeRoleSpell, $this->context['role']);

    return $this;
  }

  /**
   * @return $this
   */
  protected function doItNodeTypeMigration() {
    $this->triggerSpell($this->nodeTypeMigrationSpell, $this->context['migration']);

    return $this;
  }

  /**
   * @return $this
   */
  protected function doItNodeTypeBehat() {
    $this->triggerSpell($this->nodeTypeBehatSpell, $this->context['behat']);

    return $this;
  }

  /**
   * @return $this
   */
  protected function doItNodeTypeAdminView() {
    $this->triggerSpell($this->nodeTypeAdminViewSpell, $this->context['admin_view']);

    return $this;
  }

  protected function triggerSpell(SpellInterface $spell, array $config) {
    if (!empty($config['enabled'])) {
      $spell->abracadabra($config['settings']);
    }

    return $this;
  }

  /**
   * @return \Drupal\devel_wizard\SpellInterface[]
   */
  protected function getIncludedSpells(): array {
    return [
      'create' => $this->nodeTypeCreateSpell,
      'role' => $this->nodeTypeRoleSpell,
      'migration' => $this->nodeTypeMigrationSpell,
      'behat' => $this->nodeTypeBehatSpell,
      'admin_view' => $this->nodeTypeAdminViewSpell,
    ];
  }

}
