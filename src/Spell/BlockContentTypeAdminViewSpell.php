<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\devel_wizard\SpellInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @todo Validate that a views.view with the same name does not exists.
 */
class BlockContentTypeAdminViewSpell extends ConfigEntityAdminViewSpellBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected string $id = 'devel_wizard_block_content_type_admin_view';

  /**
   * {@inheritdoc}
   */
  protected string $provider = 'block_content';

  protected string $configEntityTypeId = 'block_content_type';

  protected string $contentEntityTypeId = 'block_content';

  protected string $primaryTabPath = 'admin/content/block-content';

  protected ModuleExtensionList $moduleList;

  /**
   * @var \Drupal\devel_wizard\SpellInterface
   */
  protected SpellInterface $moduleSpell;

  protected ConfigEntityStorageInterface $blockContentTypeStorage;

  protected UuidInterface $uuid;

  public function __construct(
    MessengerInterface $messenger,
    LoggerChannelInterface $logger,
    TwigEnvironment $twig,
    ModuleExtensionList $moduleList,
    ModuleHandlerInterface $moduleHandler,
    ModuleInstallerInterface $moduleInstaller,
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    EntityTypeManagerInterface $entityTypeManager,
    SpellInterface $moduleSpell,
    UuidInterface $uuid,
    ?Filesystem $fs = NULL
  ) {
    $this->moduleSpell = $moduleSpell;
    $this->uuid = $uuid;

    if ($entityTypeManager->hasDefinition($this->contentEntityTypeId)) {
      $this->entityType = $entityTypeManager->getDefinition($this->contentEntityTypeId);
    }

    parent::__construct(
      $messenger,
      $logger,
      $twig,
      $moduleList,
      $moduleHandler,
      $moduleInstaller,
      $configFactory,
      $typedConfigManager,
      $entityTypeManager,
      $fs,
    );
  }

  protected array $requiredModules = [
    'block' => TRUE,
    'block_content' => TRUE,
    'views' => TRUE,
  ];

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Block Content type - admin view');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Creates a view for administrators for an existing Block Content type');
  }

  public function applyDefaultValues(array $settings): array {
    $settings = parent::applyDefaultValues($settings);
    assert(!empty($settings['machine_name']), 'block_content_type machine_name is required');
    $blockContentTypeMachineName = $settings['machine_name'];

    return array_replace_recursive(
      [
        'active' => TRUE,
        'machine_name' => $blockContentTypeMachineName,
        'module' => [
          'machine_name' => 'app_core',
        ],
      ],
      $settings,
    );
  }

  public function abracadabra(array $settings) {
    $this->context = $settings;

    $this->createDerivatives($this->configEntityTypeId);

    $this->installComposerPackages('prod', $this->getRequiredPackages());
    $this->installComposerPackages('dev', $this->getRequiredDevPackages());

    $this->installModules($this->getRequiredModules());

    $this->viewStorage = $this->entityTypeManager->getStorage('view');

    // @todo Config views.view.content" is required.
    // @todo Check that if the links.task entry for the main route is exists or not.
    $viewRaw = $this->getMainTabView();

    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $view */
    $view = $this->viewStorage->load($viewRaw['id']);
    if ($view) {
      $this->messageConfigEntityExists($view);
    }
    else {
      $view = $this->viewStorage->create($viewRaw);
      $view->save();
      $this->messageConfigEntityCreate($view);
    }

    $viewRaw = $this->getSecondaryBundleTabView();

    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $view */
    $view = $this->viewStorage->load($viewRaw['id']);
    if ($view) {
      $this->messageConfigEntityExists($view);
    }
    else {
      $view = $this->viewStorage->create($viewRaw);
      $view->save();
      $this->messageConfigEntityCreate($view);
    }

    // Flushing caches so the action items show up on the created view pages.
    drupal_flush_all_caches();
  }

  protected function getMainTabView(): array {
    $machineName = $this->context['machine_name'];
    $moduleMachineName = $this->context['module']['machine_name'];
    $selfPath = $this->moduleHandler->getModule('devel_wizard')->getPath();
    $fileName = "$selfPath/config/template/views.view.block_content_admin.yml";
    $data = Yaml::decode(file_get_contents($fileName));

    $data['uuid'] = $this->uuid->generate();
    $data['id'] = "{$moduleMachineName}_custom_blocks";
    $data['label'] = $this->t('Custom blocks');

    $data['dependencies'][] = "{$this->contentEntityTypeId}.type.{$machineName}";

    return $data;
  }

  protected function getSecondaryBundleTabView(): array {
    $machineName = $this->context['machine_name'];
    $selfPath = $this->moduleHandler->getModule('devel_wizard')->getPath();
    $fileName = "$selfPath/config/template/views.view.block_content_bundle_admin.yml";
    $data = Yaml::decode(file_get_contents($fileName));

    $data['uuid'] = $this->uuid->generate();
    $data['id'] = "{$machineName}_admin";
    $data['label'] = "{$machineName}_admin";

    $data['dependencies'][] = "{$this->contentEntityTypeId}.type.{$machineName}";

    $data['display']['default']['display_options']['access']['options']['perm'] = "delete any $machineName block content";
    $data['display']['default']['display_options']['filters']['type']['value'] = [$machineName => $machineName];
    $data['display']['default']['display_options']['title'] = $machineName;
    $data['display']['overview']['display_options']['menu']['title'] = $machineName;
    $data['display']['overview']['display_options']['path'] = "admin/content/block-content/{$machineName}";

    return $data;
  }

}
