<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\block_content\Entity\BlockContentType;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class BlockContentTypeSpell extends SpellBase {

  /**
   * {@inheritdoc}
   */
  protected string $id = 'devel_wizard_block_content_type';

  protected string $provider = 'block_content';

  protected string $configEntityTypeId = 'block_content_type';

  protected string $contentEntityTypeId = 'block_content';

  protected BlockContentTypeCreateSpell $blockContentTypeCreateSpell;

  protected BlockContentTypeRoleSpell $blockContentTypeRoleSpell;

  protected BlockContentTypeMigrationSpell $blockContentTypeMigrationSpell;

  protected BlockContentTypeBehatSpell $blockContentTypeBehatSpell;

  protected BlockContentTypeAdminViewSpell $blockContentTypeAdminViewSpell;

  public function __construct(
    MessengerInterface $messenger,
    LoggerChannelInterface $logger,
    TwigEnvironment $twig,
    ModuleExtensionList $moduleList,
    ModuleHandlerInterface $moduleHandler,
    ModuleInstallerInterface $moduleInstaller,
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    EntityTypeManagerInterface $entityTypeManager,
    BlockContentTypeCreateSpell $blockContentTypeCreateSpell,
    BlockContentTypeRoleSpell $blockContentTypeRoleSpell,
    BlockContentTypeMigrationSpell $blockContentTypeMigrationSpell,
    BlockContentTypeBehatSpell $blockContentTypeBehatSpell,
    BlockContentTypeAdminViewSpell $blockContentTypeAdminViewSpell,
    ?Filesystem $fs = NULL
  ) {
    $this->blockContentTypeCreateSpell = $blockContentTypeCreateSpell;
    $this->blockContentTypeRoleSpell = $blockContentTypeRoleSpell;
    $this->blockContentTypeMigrationSpell = $blockContentTypeMigrationSpell;
    $this->blockContentTypeBehatSpell = $blockContentTypeBehatSpell;
    $this->blockContentTypeAdminViewSpell = $blockContentTypeAdminViewSpell;

    parent::__construct(
      $messenger,
      $logger,
      $twig,
      $moduleList,
      $moduleHandler,
      $moduleInstaller,
      $configFactory,
      $typedConfigManager,
      $entityTypeManager,
      $fs
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Block content type - all in one');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Creates a Block content type with all the bells and whistles');
  }

  public function applyDefaultValues(array $settings): array {
    $settings = parent::applyDefaultValues($settings);
    assert(!empty($settings['machine_name']), 'machine_name is required');

    $blockContentTypeId = $settings['machine_name'];

    $settings = array_replace_recursive(
      [
        'create' => [
          'enabled' => TRUE,
          'settings' => [
            'values' => [
              'id' => $blockContentTypeId,
            ],
          ],
        ],
        'role' => [
          'enabled' => TRUE,
          'settings' => [
            'machine_name' => $blockContentTypeId,
          ],
        ],
        'migration' => [
          'enabled' => TRUE,
          'settings' => [
            'machine_name' => $blockContentTypeId,
          ],
        ],
        'behat' => [
          'enabled' => TRUE,
          'settings' => [
            'machine_name' => $blockContentTypeId,
          ],
        ],
        'admin_view' => [
          'enabled' => TRUE,
          'settings' => [
            'machine_name' => $blockContentTypeId,
          ],
        ],
      ],
      $settings,
    );

    $settings['create']['settings'] = $this->blockContentTypeCreateSpell->applyDefaultValues($settings['create']['settings']);
    $settings['role']['settings'] = $this->blockContentTypeRoleSpell->applyDefaultValues($settings['role']['settings']);
    $settings['migration']['settings'] = $this->blockContentTypeMigrationSpell->applyDefaultValues($settings['migration']['settings']);
    $settings['behat']['settings'] = $this->blockContentTypeBehatSpell->applyDefaultValues($settings['behat']['settings']);
    $settings['admin_view']['settings'] = $this->blockContentTypeAdminViewSpell->applyDefaultValues($settings['admin_view']['settings']);

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormBuild(array $parents, FormStateInterface $formState): array {
    $form = [
      '#type' => 'container',
      '#tree' => TRUE,
      // @todo This input field is not necessary,
      // the create[settings][values][id] could be used for the same purpose.
      'machine_name' => [
        '#type' => 'machine_name',
        // @todo Core does not support #label for machine_name elements.
        '#label' => $this->t('Machine-readable name of the new Block content type'),
        '#default_value' => '',
        '#maxlength' => 32,
        '#description' => $this->t('A unique name for this Block content type. It must only contain lowercase letters, numbers, and underscores.'),
        '#machine_name' => [
          'exists' => [BlockContentType::class, 'load'],
          'standalone' => TRUE,
        ],
      ],
    ];

    foreach ($this->getIncludedSpells() as $key => $spell) {
      $subParents = $parents;
      $subParents[] = $key;

      $form[$key] = [
        '#type' => 'fieldset',
        '#title' => $spell->label(),
        '#tree' => TRUE,
        'enabled' => [
          '#title' => $this->t('Enabled'),
          '#default_value' => TRUE,
          '#type' => 'checkbox',
        ],
        'settings' => [
          '#type' => 'container',
          '#tree' => TRUE,
        ] + $spell->settingsFormBuild($subParents, $formState),
      ];

      $togglerSelector = sprintf(
        ':input[name="%s"]',
        Utils::inputName(array_merge($subParents, ['enabled'])),
      );
      $form[$key]['settings']['#states']['visible'][$togglerSelector] = ['checked' => TRUE];

      if ($key === 'create') {
        $form[$key]['enabled']['#access'] = FALSE;
      }

      if (isset($form[$key]['settings']['machine_name'])) {
        $form[$key]['settings']['machine_name']['#required'] = FALSE;
        $form[$key]['settings']['machine_name']['#access'] = FALSE;
      }

      if (isset($form[$key]['settings']['values']['id'])) {
        $form[$key]['settings']['values']['id']['#required'] = FALSE;
        $form[$key]['settings']['values']['id']['#access'] = FALSE;
      }
    }

    return $form;
  }

  public function validate(array $values): ConstraintViolationListInterface {
    $violations = $this
      ->typedConfigManager
      ->createFromNameAndData($this->configName(), $values)
      ->validate();

    $machineNameErrors = [
      'role.settings.machine_name',
      'migration.settings.machine_name',
      'behat.settings.machine_name',
      'admin_view.settings.machine_name',
    ];
    /** @var \Symfony\Component\Validator\ConstraintViolationInterface $violation */
    foreach ($violations as $id => $violation) {
      if (in_array($violation->getPropertyPath(), $machineNameErrors)) {
        $violations->remove($id);
      }
    }

    return $violations;
  }

  /**
   * {@inheritdoc}
   */
  public function abracadabra(array $settings) {
    $this->context = $settings;

    $this
      ->doItBlockContentTypeCreate()
      ->doItBlockContentTypeRole()
      ->doItBlockContentTypeMigration()
      ->doItBlockContentTypeBehat()
      ->doItBlockContentTypeAdminView();

    return $this;
  }

  /**
   * @return $this
   */
  protected function doItBlockContentTypeCreate() {
    $this->triggerSpell($this->blockContentTypeCreateSpell, $this->context['create']);

    return $this;
  }

  /**
   * @return $this
   */
  protected function doItBlockContentTypeRole() {
    $this->triggerSpell($this->blockContentTypeRoleSpell, $this->context['role']);

    return $this;
  }

  /**
   * @return $this
   */
  protected function doItBlockContentTypeMigration() {
    $this->triggerSpell($this->blockContentTypeMigrationSpell, $this->context['migration']);

    return $this;
  }

  /**
   * @return $this
   */
  protected function doItBlockContentTypeBehat() {
    $this->triggerSpell($this->blockContentTypeBehatSpell, $this->context['behat']);

    return $this;
  }

  /**
   * @return $this
   */
  protected function doItBlockContentTypeAdminView() {
    $this->triggerSpell($this->blockContentTypeAdminViewSpell, $this->context['admin_view']);

    return $this;
  }

  protected function triggerSpell(SpellInterface $spell, array $config) {
    if (!empty($config['enabled'])) {
      $spell->abracadabra($config['settings']);
    }

    return $this;
  }

  /**
   * @return \Drupal\devel_wizard\SpellInterface[]
   */
  protected function getIncludedSpells(): array {
    return [
      'create' => $this->blockContentTypeCreateSpell,
      'role' => $this->blockContentTypeRoleSpell,
      'migration' => $this->blockContentTypeMigrationSpell,
      'behat' => $this->blockContentTypeBehatSpell,
      'admin_view' => $this->blockContentTypeAdminViewSpell,
    ];
  }

}
