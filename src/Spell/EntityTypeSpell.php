<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Stringy\StaticStringy;
use Symfony\Component\Filesystem\Filesystem;
use Webmozart\PathUtil\Path;

class EntityTypeSpell extends SpellBase {

  protected string $id = 'devel_wizard_entity_type';

  protected SpellInterface $moduleSpell;

  public function __construct(
    MessengerInterface $messenger,
    LoggerChannelInterface $logger,
    TwigEnvironment $twig,
    ModuleExtensionList $moduleList,
    ModuleHandlerInterface $moduleHandler,
    ModuleInstallerInterface $moduleInstaller,
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    EntityTypeManagerInterface $entityTypeManager,
    SpellInterface $moduleSpell,
    ?Filesystem $fs = NULL
  ) {
    $this->moduleSpell = $moduleSpell;

    parent::__construct(
      $messenger,
      $logger,
      $twig,
      $moduleList,
      $moduleHandler,
      $moduleInstaller,
      $configFactory,
      $typedConfigManager,
      $entityTypeManager,
      $fs,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Entity type');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Generates PHP and YML files for a bundable, fieldable, revisionable content and config entity pair.');
  }

  public function settingsFormBuild(array $parents, FormStateInterface $formState): array {
    $machineNamePattern = '[a-z][a-z0-9_]*';

    return [
      '#tree' => TRUE,
      'module' => [
        '#tree' => TRUE,
        '#type' => 'fieldset',
        '#title' => $this->t('Module'),
        'machine_name' => [
          '#type' => 'textfield',
          '#required' => TRUE,
          '#title' => $this->t('Machine-name'),
          '#description' => $this->t('Machine-name of the module to put the new code files into. If not exists then it will be created in the modules/custom directory'),
          '#default_value' => '',
          '#pattern' => $machineNamePattern,
          '#autocomplete_route_name' => 'devel_wizard.module.autocomplete',
        ],
      ],
      'config' => [
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#title' => $this->t('Config entity'),
        'id' => [
          '#type' => 'textfield',
          '#title' => $this->t('Machine-name'),
          '#required' => TRUE,
          '#default_value' => '',
          '#maxlength' => 64,
          '#description' => $this->t('A unique name for this item. It must only contain lowercase letters, numbers, and underscores.'),
          '#pattern' => $machineNamePattern,
        ],
      ],
      'content' => [
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#title' => $this->t('Content entity'),
        'id' => [
          '#type' => 'textfield',
          '#title' => $this->t('Machine-name'),
          '#required' => TRUE,
          '#default_value' => '',
          '#maxlength' => 64,
          '#description' => $this->t('A unique name for this item. It must only contain lowercase letters, numbers, and underscores.'),
          '#pattern' => $machineNamePattern,
        ],
      ],
    ];
  }

  public function applyDefaultValues(array $settings): array {
    $settings = parent::applyDefaultValues($settings);
    assert(!empty($settings['content']['id']), 'ID of the content entity type is given');

    if (empty($settings['module']['machine_name'])) {
      $settings['module']['machine_name'] = $settings['content']['id'];
    }

    if (empty($settings['config']['id'])) {
      $settings['config']['id'] = "{$settings['content']['id']}_type";
    }

    $settings['module'] += [
      'machine_name_dash' => StaticStringy::dasherize($settings['module']['machine_name']),
      'namespace' => "Drupal\\{$settings['module']['machine_name']}",
      'info.yml' => [
        'configure' => "entity.{$settings['config']['id']}.collection",
      ],
    ];
    $settings['module']['info.yml'] += [
      'configure' => "entity.{$settings['config']['id']}.collection",
    ];

    $settings['config'] += [
      'id_dash' => StaticStringy::dasherize($settings['config']['id']),
      'namespace' => "{$settings['module']['namespace']}\\ConfigEntity",
      'class' => StaticStringy::upperCamelize($settings['config']['id']),
      'label' => StaticStringy::humanize($settings['config']['id']),
    ];
    $settings['config'] += [
      'label_plural' => $settings['config']['label'] . 's',
      'class_fqn' => "{$settings['module']['namespace']}\\Entity\\{$settings['config']['class']}",
      'interface' => "{$settings['config']['class']}Interface",
    ];
    $settings['config'] += [
      'interface_fqn' => "{$settings['module']['namespace']}\\{$settings['config']['interface']}",
    ];

    $settings['content'] += [
      'id_dash' => StaticStringy::dasherize($settings['content']['id']),
      'namespace' => "{$settings['module']['namespace']}\\ContentEntity",
      'class' => StaticStringy::upperCamelize($settings['content']['id']),
      'label' => StaticStringy::humanize($settings['content']['id']),
    ];
    $settings['content'] += [
      'label_plural' => $settings['content']['label'] . 's',
      'class_fqn' => "{$settings['module']['namespace']}\\Entity\\{$settings['content']['class']}",
      'interface' => "{$settings['content']['class']}Interface",
    ];
    $settings['content'] += [
      'interface_fqn' => "{$settings['module']['namespace']}\\{$settings['content']['interface']}",
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function abracadabra(array $settings) {
    $this->ensureModuleExists($settings['module']);
    $settings['module']['dir'] = $this->getModulePath($settings['module']['machine_name']);

    $this->configEntityType($settings);
    $this->contentEntityType($settings);

    return $this;
  }

  protected function configEntityType(array $settings) {
    $this->configEntityTypeClass($settings);
    $this->configEntityTypeClassInterface($settings);

    $this->configEntityTypeHandler($settings, 'add_form');
    $this->configEntityTypeHandler($settings, 'edit_form');
    $this->configEntityTypeHandler($settings, 'delete_form');
    $this->configEntityTypeHandler($settings, 'access_control_handler');
    $this->configEntityTypeHandler($settings, 'list_builder');
    $this->configEntityTypeHandler($settings, 'route_provider');
    $this->configEntityTypeHandler($settings, 'storage');
    $this->configEntityTypeHandler($settings, 'comparer');

    $this->configEntityTypeRouting($settings);
    $this->configEntityTypeServices($settings);
    $this->configEntityTypePermissions($settings);
    $this->configEntityTypeLinksAction($settings);
    $this->configEntityTypeLinksMenu($settings);
    $this->configEntityTypeLinksTask($settings);
    $this->configEntityTypeSchema($settings);
    $this->configEntityTypeTests($settings);

    return $this;
  }

  protected function configEntityTypeClassInterface(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      'src',
      "{$settings['config']['class']}Interface.php",
    );
    $this->dumpFile(
      $fileName,
      $this->twig->render(
        '@devel_wizard/config_entity/devel_wizard.config_entity.interface.php.twig',
        $settings,
      ),
    );

    return $this;
  }

  protected function configEntityTypeClass(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      'src',
      'Entity',
      "{$settings['config']['class']}.php",
    );
    $this->dumpFile(
      $fileName,
      $this->twig->render(
        '@devel_wizard/config_entity/devel_wizard.config_entity.class.php.twig',
        $settings,
      ),
    );
  }

  /**
   * @todo DRY - This method is very similar to ::contentEntityTypeHandler.
   */
  protected function configEntityTypeHandler(array $settings, string $handler) {
    $type = 'config';
    $handlerUpperCamel = StaticStringy::upperCamelize($handler);

    $name = "{$settings[$type]['class']}$handlerUpperCamel";
    $fileName = Path::join(
      $settings['module']['dir'],
      'src',
      Utils::relativeNamespaceDir($settings[$type]['namespace']),
      "$name.php",
    );
    $this->dumpFile(
      $fileName,
      $this->twig->render(
        "@devel_wizard/{$type}_entity/devel_wizard.{$type}_entity.$handler.php.twig",
        $settings,
      ),
    );

    return $this;
  }

  protected function configEntityTypeRouting(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      "{$settings['module']['machine_name']}.routing.yml",
    );
    $this->ymlFileReplace($fileName, $this->getConfigEntityTypeRoutes($settings));

    return $this;
  }

  protected function configEntityTypeServices(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      "{$settings['module']['machine_name']}.services.yml",
    );
    $this->ymlFileReplace(
      $fileName,
      $this->getConfigEntityTypeServices($settings),
      ['services'],
    );

    return $this;
  }

  /**
   * @return $this
   */
  protected function configEntityTypePermissions(array $settings) {
    return $this->entityTypePermissions($settings, 'config');
  }

  /**
   * @return $this
   */
  protected function contentEntityTypePermissions(array $settings) {
    return $this->entityTypePermissions($settings, 'content');
  }

  /**
   * @return $this
   */
  protected function entityTypePermissions(array $settings, string $type) {
    $id = $settings[$type]['id'];

    // Add entries to *.permissions.yml.
    $fileName = Path::join(
      $settings['module']['dir'],
      "{$settings['module']['machine_name']}.permissions.yml",
    );
    $fileContent = $this->fs->exists($fileName) ? file_get_contents($fileName) : '{}';
    $permissions = Yaml::decode($fileContent);
    $callable = "entity.$id.permission_provider:getPermissions";
    settype($permissions['permission_callbacks'], 'array');
    if (!in_array($callable, $permissions['permission_callbacks'])) {
      $permissions['permission_callbacks'][] = $callable;
      $this->dumpFile($fileName, Yaml::encode($permissions));
    }

    // Add entries to *.services.yml.
    $fileName = Path::join(
      $settings['module']['dir'],
      "{$settings['module']['machine_name']}.services.yml",
    );
    $this->ymlFileReplace(
      $fileName,
      [
        "entity.$id.permission_provider" => [
          'class' => "{$settings['module']['namespace']}\\{$settings['content']['class']}PermissionProvider",
          'arguments' => [
            '@entity_type.manager',
          ],
          'calls' => [
            ['setEntityTypeId', [$id]],
          ],
        ],
      ],
      ['services'],
    );

    // Create the required classes.
    // @todo Only once.
    $handlers = [
      'permission_provider_interface',
      'permission_provider',
    ];
    foreach ($handlers as $handler) {
      $this->commonEntityTypeHandler($settings, $handler);
    }

    return $this;
  }

  protected function configEntityTypeLinksAction(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      "{$settings['module']['machine_name']}.links.action.yml",
    );
    $this->ymlFileReplace($fileName, $this->getConfigEntityTypeLinksAction($settings));

    return $this;
  }

  protected function configEntityTypeLinksMenu(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      "{$settings['module']['machine_name']}.links.menu.yml",
    );
    $this->ymlFileReplace($fileName, $this->getConfigEntityTypeLinksMenu($settings));

    return $this;
  }

  protected function configEntityTypeLinksTask(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      "{$settings['module']['machine_name']}.links.task.yml",
    );
    $this->ymlFileReplace($fileName, $this->getConfigEntityTypeLinksTask($settings));

    return $this;
  }

  protected function configEntityTypeSchema(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      'config',
      'schema',
      "{$settings['module']['machine_name']}.schema.yml",
    );
    $this->ymlFileReplace($fileName, $this->getConfigEntityTypeSchema($settings));

    return $this;
  }

  protected function configEntityTypeTests(array $settings) {
    $tmpPrefix = '@devel_wizard/config_entity';
    $fileNames = [
      'tests/src/Functional/TestBase.php' => "$tmpPrefix/devel_wizard.config_entity.test.functional.base.php.twig",
      'tests/src/Functional/ConfigEntityCrudTest.php' => "$tmpPrefix/devel_wizard.config_entity.test.functional.crud.php.twig",
    ];

    foreach ($fileNames as $dst => $tpl) {
      $fileName = Path::join($settings['module']['dir'], $dst);
      $this->dumpFile(
        $fileName,
        $this->twig->render($tpl, $settings),
      );
    }

    return $this;
  }

  protected function getConfigEntityTypeRoutes(array $settings): array {
    return [];
  }

  protected function getConfigEntityTypeServices(array $settings): array {
    return [];
  }

  protected function getConfigEntityTypeLinksAction(array $settings): array {
    $id = $settings['config']['id'];

    return [
      "entity.$id.add_form" => [
        'route_name' => "entity.$id.add_form",
        'title' => "Add {$settings['config']['label']}",
        'appears_on' => [
          "entity.$id.collection",
        ],
      ],
    ];
  }

  protected function getConfigEntityTypeLinksMenu(array $settings): array {
    $id = $settings['config']['id'];

    return [
      "entity.$id.collection" => [
        'title' => $settings['config']['label_plural'],
        'parent' => 'system.admin_structure',
        'description' => "Create and manage fields, forms, and display settings for your {$settings['content']['label']}.",
        'route_name' => "entity.$id.collection",
      ],
    ];
  }

  protected function getConfigEntityTypeLinksTask(array $settings): array {
    $id = $settings['config']['id'];

    return [
      "entity.$id.edit_form" => [
        'title' => 'Edit',
        'route_name' => "entity.$id.edit_form",
        'base_route' => "entity.$id.edit_form",
      ],
    ];
  }

  protected function getConfigEntityTypeSchema(array $settings): array {
    $module = $settings['module']['machine_name'];
    $id = $settings['config']['id'];

    return [
      "$module.$id.*" => [
        'type' => 'config_entity',
        'label' => $settings['config']['label'],
        'mapping' => [
          'id' => [
            'type' => 'string',
            'label' => 'Machine-readable name',
          ],
          'label' => [
            'type' => 'label',
            'label' => 'Label',
          ],
          'description' => [
            'type' => 'text',
            'label' => 'Description',
          ],
          'help' => [
            'type' => 'text',
            'label' => 'Explanation or submission guidelines',
          ],
          'weight' => [
            'type' => 'integer',
            'label' => 'Weight',
          ],
        ],
      ],
    ];
  }

  protected function contentEntityType(array $settings) {
    $this->contentEntityTypeClass($settings);
    $this->contentEntityTypeInterface($settings);

    $this->contentEntityTypeHandler($settings, 'access_control_handler');
    $this->contentEntityTypeHandler($settings, 'add_form');
    $this->contentEntityTypeHandler($settings, 'edit_form');
    $this->contentEntityTypeHandler($settings, 'delete_form');
    $this->contentEntityTypeHandler($settings, 'list_builder');
    $this->contentEntityTypeHandler($settings, 'storage');
    $this->contentEntityTypeHandler($settings, 'storage_interface');
    $this->contentEntityTypeHandler($settings, 'storage_schema');
    $this->contentEntityTypeHandler($settings, 'translation_handler');
    $this->contentEntityTypeHandler($settings, 'view_builder');
    $this->contentEntityTypeHandler($settings, 'view_controller');
    $this->contentEntityTypeHandler($settings, 'controller');
    $this->contentEntityTypeHandler($settings, 'route_provider');
    $this->contentEntityTypeHandler($settings, 'revision_delete_form');
    $this->contentEntityTypeHandler($settings, 'revision_revert_form');
    $this->contentEntityTypeHandler($settings, 'revision_revert_translation_form');

    $this->contentEntityTypePermissions($settings);
    $this->contentEntityTypeLinksAction($settings);
    $this->contentEntityTypeLinksContextual($settings);
    $this->contentEntityTypeLinksMenu($settings);
    $this->contentEntityTypeLinksTask($settings);
    $this->contentEntityTypeRouting($settings);

    $this->contentEntityTypeModule($settings);
    $this->contentEntityTypeTemplate($settings);

    return $this;
  }

  protected function contentEntityTypeClass(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      'src',
      'Entity',
      "{$settings['content']['class']}.php",
    );
    $this->dumpFile(
      $fileName,
      $this->twig->render(
        '@devel_wizard/content_entity/devel_wizard.content_entity.class.php.twig',
        $settings,
      ),
    );

    return $this;
  }

  protected function contentEntityTypeInterface(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      'src',
      "{$settings['content']['class']}Interface.php",
    );
    $this->dumpFile(
      $fileName,
      $this->twig->render(
        '@devel_wizard/content_entity/devel_wizard.content_entity.interface.php.twig',
        $settings,
      ),
    );

    return $this;
  }

  protected function contentEntityTypeHandler(array $settings, string $handler) {
    $handlerUpperCamel = StaticStringy::upperCamelize($handler);

    $name = "{$settings['content']['class']}$handlerUpperCamel";
    $fileName = Path::join(
      $settings['module']['dir'],
      'src',
      Utils::relativeNamespaceDir($settings['content']['namespace']),
      "$name.php",
    );
    $this->dumpFile(
      $fileName,
      $this->twig->render(
        "@devel_wizard/content_entity/devel_wizard.content_entity.$handler.php.twig",
        $settings,
      ),
    );

    return $this;
  }

  protected function commonEntityTypeHandler(array $settings, string $handler) {
    $handlerCamel = StaticStringy::upperCamelize($handler);
    $name = "{$settings['content']['class']}$handlerCamel";
    $fileName = Path::join(
      $settings['module']['dir'],
      'src',
      "$name.php",
    );
    $this->dumpFile(
      $fileName,
      $this->twig->render(
        "@devel_wizard/common_entity/devel_wizard.common_entity.$handler.php.twig",
        $settings,
      ),
    );

    return $this;
  }

  protected function contentEntityTypeLinksAction(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      "{$settings['module']['machine_name']}.links.action.yml",
    );

    $this->ymlFileReplace($fileName, $this->getContentEntityTypeLinksAction($settings));

    return $this;
  }

  protected function getContentEntityTypeLinksAction(array $settings): array {
    $id = $settings['content']['id'];

    return [
      "entity.$id.add_page" => [
        'route_name' => "entity.$id.add_page",
        'title' => "Create a new {$settings['content']['label']}",
        'appears_on' => [
          "entity.$id.collection",
        ],
      ],
    ];
  }

  protected function contentEntityTypeLinksContextual(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      "{$settings['module']['machine_name']}.links.contextual.yml",
    );

    $this->ymlFileReplace($fileName, $this->getContentEntityTypeLinksContextual($settings));

    return $this;
  }

  protected function getContentEntityTypeLinksContextual(array $settings): array {
    $id = $settings['content']['id'];

    return [
      "entity.$id.edit_form" => [
        'route_name' => "entity.$id.edit_form",
        'group' => $id,
        'title' => 'Edit',
      ],
      "entity.$id.delete_form" => [
        'route_name' => "entity.$id.delete_form",
        'group' => $id,
        'title' => 'Delete',
        'weight' => 10,
      ],
    ];
  }

  protected function contentEntityTypeLinksMenu(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      "{$settings['module']['machine_name']}.links.menu.yml",
    );

    $this->ymlFileReplace($fileName, $this->getContentEntityTypeLinksMenu($settings));

    return $this;
  }

  protected function getContentEntityTypeLinksMenu(array $settings): array {
    $id = $settings['content']['id'];

    return [
      "entity.$id.collection" => [
        'title' => $settings['content']['label_plural'],
        'parent' => 'system.admin_content',
        'description' => "List of {$settings['content']['label']} contents.",
        'route_name' => "entity.$id.collection",
      ],
    ];
  }

  protected function contentEntityTypeLinksTask(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      "{$settings['module']['machine_name']}.links.task.yml",
    );

    $this->ymlFileReplace($fileName, $this->getContentEntityTypeLinksTask($settings));

    return $this;
  }

  protected function getContentEntityTypeLinksTask(array $settings): array {
    $id = $settings['content']['id'];

    return [
      "entity.$id.collection" => [
        'title' => $settings['content']['label_plural'],
        'route_name' => "entity.$id.collection",
        'base_route' => 'system.admin_content',
      ],
      "entity.$id.canonical" => [
        'route_name' => "entity.$id.canonical",
        'base_route' => "entity.$id.canonical",
        'title' => 'View',
      ],
      "entity.$id.edit_form" => [
        'route_name' => "entity.$id.edit_form",
        'base_route' => "entity.$id.canonical",
        'title' => 'Edit',
      ],
      "entity.$id.revision_history" => [
        'route_name' => "entity.$id.revision_history",
        'base_route' => "entity.$id.canonical",
        'title' => 'Revisions',
        'weight' => 20,
      ],
      "entity.$id.delete_form" => [
        'route_name' => "entity.$id.delete_form",
        'base_route' => "entity.$id.canonical",
        'title' => 'Delete',
        'weight' => 10,
      ],
    ];
  }

  protected function contentEntityTypeRouting(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      "{$settings['module']['machine_name']}.routing.yml",
    );

    $this->ymlFileReplace($fileName, $this->getContentEntityTypeRoutes($settings));

    return $this;
  }

  protected function getContentEntityTypeRoutes(array $settings): array {
    return [];
  }

  protected function contentEntityTypeModule(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      $settings['module']['machine_name'] . '.module',
    );

    $fileExists = $this->fs->exists($fileName);
    $fileContent = implode("\n", [
      '<?php',
      '',
      'declare(strict_types = 1);',
      '',
      '/**',
      ' * @file',
      ' * Common hooks.',
      ' */',
      '',
    ]);
    if ($fileExists) {
      $fileContent = file_get_contents($fileName);
    }

    $hookTheme = $this->twig->render(
      '@devel_wizard/content_entity/devel_wizard.content_entity.hook_theme.php.twig',
      $settings,
    );

    $hookThemeName = "{$settings['module']['machine_name']}_theme";
    if (mb_strpos($fileContent, "function {$hookThemeName}(") === FALSE) {
      $fileContent .= $hookTheme;
    }
    else {
      $this->messenger()->addWarning($this->t(
        'Function @name already exists. It has to be extended with the following code: <pre>@hook_theme</pre>',
        [
          '@name' => $hookThemeName,
          '@hook_theme' => $hookTheme,
        ],
      ));
    }

    if (mb_strpos($fileContent, "function template_preprocess_{$settings['content']['id']}(") === FALSE) {
      $fileContent .= $this->twig->render(
        '@devel_wizard/content_entity/devel_wizard.content_entity.module.php.twig',
        $settings,
      );
    }

    // @todo Prevent duplication.
    $fileContent = Utils::addUseStatements(
      [
        'use Drupal\Core\Entity\EntityInterface;',
        'use Drupal\Core\Render\Element;',
      ],
      $fileContent
    );
    $this->dumpFile($fileName, $fileContent);

    return $this;
  }

  protected function contentEntityTypeTemplate(array $settings) {
    $fileName = Path::join(
      $settings['module']['dir'],
      'templates',
      "{$settings['module']['machine_name_dash']}.{$settings['content']['id_dash']}.html.twig",
    );

    $this->dumpFile(
      $fileName,
      $this->twig->render(
        '@devel_wizard/content_entity/devel_wizard.content_entity.twig.twig',
        $settings,
      ),
    );

    return $this;
  }

}
