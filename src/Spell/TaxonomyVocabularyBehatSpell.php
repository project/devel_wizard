<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Spell;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\user\RoleInterface;
use Symfony\Component\Filesystem\Filesystem;
use Webmozart\PathUtil\Path;

class TaxonomyVocabularyBehatSpell extends ConfigEntityBehatSpellBase {

  protected string $id = 'devel_wizard_taxonomy_vocabulary_behat';

  protected string $provider = 'taxonomy';

  protected string $configEntityTypeId = 'taxonomy_vocabulary';

  protected string $contentEntityTypeId = 'taxonomy_term';

  protected EntityTypeBundleInfoInterface $bundleInfo;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    MessengerInterface $messenger,
    LoggerChannelInterface $logger,
    TwigEnvironment $twig,
    ModuleExtensionList $moduleList,
    ModuleHandlerInterface $moduleHandler,
    ModuleInstallerInterface $moduleInstaller,
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    EntityTypeManagerInterface $entityTypeManager,
    EntityTypeBundleInfoInterface $bundleInfo,
    ?Filesystem $fs = NULL
  ) {
    $this->bundleInfo = $bundleInfo;

    parent::__construct(
      $messenger,
      $logger,
      $twig,
      $moduleList,
      $moduleHandler,
      $moduleInstaller,
      $configFactory,
      $typedConfigManager,
      $entityTypeManager,
      $fs,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Behat tests for a Taxonomy vocabulary');
  }

  /**
   * {@inheritdoc}
   */
  public function description(): TranslatableMarkup {
    return $this->t('Generates Behat tests for an existing Taxonomy vocabulary');
  }

  protected function generateTests() {
    $bundleMachineName = $this->context['machine_name'];

    $configStorage = $this->getConfigStorage();
    $bundle = $configStorage->load($bundleMachineName);

    $messenger = $this->messenger();
    $args = [
      '@spell' => $this->id(),
      '@machine_name' => $bundleMachineName,
      '@bundle.url.canonical' => $bundle->toUrl()->toString(),
      '@bundle.label' => $bundle->label(),
    ];
    $messenger->addStatus($this->t(
      '@spell - Create behat test for <a href="@bundle.url.canonical">@bundle.label</a>',
      $args,
    ));

    $tests = [
      'access' => [
        'create',
        'view',
        'edit',
        'delete',
      ],
      'form' => [
        'create',
        'edit',
      ],
    ];

    foreach ($tests as $tesType => $operations) {
      foreach ($operations as $operation) {
        $this->generateTest($tesType, $operation, $bundleMachineName, $bundle->label());
      }
    }

    return $this;
  }

  protected function generateTest(
    string $testType,
    string $operation,
    string $bundleMachineName,
    string $bundleLabel
  ) {
    $fileName = Path::join(
      $this->context['features_dir'],
      $bundleMachineName,
      "{$bundleMachineName}.{$testType}.{$operation}.feature",
    );

    $templatePath = "@devel_wizard/behat/{$this->configEntityTypeId}/devel_wizard.behat_{$testType}_{$operation}.feature.twig";

    $values = [
      'machine_name' => $bundleMachineName,
      'label' => $bundleLabel,
      'roles' => $this->getRolesWithStatusCode($bundleMachineName),
    ];

    $rendered = $this->twig->render($templatePath, $values);

    $this->dumpFile($fileName, $rendered);
    $this->messageFileCreate($fileName);
  }

  /**
   * Loads all roles in the system with status codes.
   *
   * Helper method to load all roles in the system with status codes
   * determining if the role has access to various operations, also provides
   * the number of spaces needed between TableNode keys.
   *
   * @param string $bundleMachineName
   *   The bundle machine-name we are creating the Behat tests for.
   *
   * @return array
   *   Returns an array with the roles and status codes.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getRolesWithStatusCode(string $bundleMachineName): array {
    $roles = [];
    $userRoleStorage = $this
      ->entityTypeManager
      ->getStorage('user_role');
    $adminRoles = $userRoleStorage
      ->getQuery()
      ->condition('is_admin', TRUE)
      ->execute();
    $adminRoles[] = "{$bundleMachineName}_admin";

    $allRoles = $userRoleStorage->loadMultiple();
    // Remove anonymous role.
    unset($allRoles[RoleInterface::ANONYMOUS_ID]);
    $roleNames = array_keys($allRoles);
    $longestRoleLength = (int) max(array_map('strlen', $roleNames));
    foreach ($roleNames as $role) {
      // Determine how many spaces we need in each row of TableNode.
      $roles[$role]['spaces'] = $longestRoleLength - strlen($role) + 1;
      if (!in_array($role, $adminRoles)) {
        $roles[$role]['code'] = 403;
        continue;
      }
      $roles[$role]['code'] = 200;
    }
    // Sort the array by code.
    array_multisort(array_column($roles, 'code'), SORT_ASC, $roles);
    // Determine how many spaces we need in TableNode header after 'role'.
    $roles['headerSpaces'] = $longestRoleLength - 4 + 1;

    return $roles;
  }

}
