<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Filesystem\Filesystem;
use Webmozart\PathUtil\Path;

class Utils {

  const DRUPAL_PROJECT_TYPES = [
    'drush',
    'profile',
    'module',
    'theme',
    'engine',
  ];

  /**
   * Matches lowercase letters, numbers, underscores.
   */
  const MACHINE_NAME_REGEXP = '/^[a-z][a-z0-9_]*$/';

  public static function selfProjectRootDir(): string {
    return dirname(__DIR__);
  }

  public static function formatByExpiryDate(
    string $text,
    \DateTimeInterface $expiry_date,
    int $warning_days = 180,
    int $error_day = 90
  ): string {
    $now = new \DateTimeImmutable();
    $diff = $expiry_date->diff($now);

    if ($diff->invert === 0 || $diff->days < $error_day) {
      // Already in the past or in the near future.
      return "<fg=red>$text</>";
    }

    if ($diff->invert === 1 && $diff->days < $warning_days) {
      // In the future but not far enough.
      return "<fg=yellow>$text</>";
    }

    return $text;
  }

  public static function phpVersions(): array {
    return [
      '8.0' => [
        'version' => '8.0',
        'release' => new \DateTimeImmutable('2020-11-30'),
        'support' => new \DateTimeImmutable('2022-11-30'),
        'security' => new \DateTimeImmutable('2023-11-30'),
        'features' => [
          'union types',
          'named arguments',
          'attributes',
          'match expression',
        ],
      ],
      '7.4' => [
        'version' => '7.4',
        'release' => new \DateTimeImmutable('2019-11-28'),
        'support' => new \DateTimeImmutable('2021-11-28'),
        'security' => new \DateTimeImmutable('2022-11-28'),
        'features' => [
          'typed properties',
          'short closures',
          'null coalescing operator',
        ],
      ],
      '7.3' => [
        'version' => '7.3',
        'release' => new \DateTimeImmutable('2018-12-06'),
        'support' => new \DateTimeImmutable('2020-12-06'),
        'security' => new \DateTimeImmutable('2021-12-06'),
        'features' => [
          'references in list assignments',
          'flexible heredocs',
        ],
      ],
    ];
  }

  public static function phpVersionChoices(
    ?array $php_versions = NULL,
    string $pattern = '{version} - active: {support}; security: {security}'
  ): array {
    if ($php_versions === NULL) {
      $php_versions = static::phpVersions();
    }

    $choices = [];
    $date_formatter = \Drupal::getContainer()->get('date.formatter');
    foreach ($php_versions as $php) {
      $args = [];
      foreach ($php as $key => $value) {
        switch (gettype($value)) {
          case 'array':
            $args["{{$key}}"] = implode(', ', $value);
            break;

          case 'object':
            if ($value instanceof \DateTimeInterface) {
              $date = $date_formatter->format($value->getTimestamp(), 'html_date');
              $args["{{$key}}"] = $key === 'release' ? $date : Utils::formatByExpiryDate($date, $value);
            }
            break;

          default:
            $args["{{$key}}"] = $value;
            break;
        }
      }
      $choices[$php['version']] = strtr($pattern, $args);
    }

    return $choices;
  }

  /**
   * @return \Symfony\Component\Console\Question\Question[]
   */
  public static function questionsPhpMinimum(): array {
    $questions = [];

    $questions['php_minimum'] = (new ChoiceQuestion('Minimum PHP version', Utils::phpVersionChoices(), '7.4'))
      ->setValidator([static::class, 'validateRequired']);

    return $questions;
  }

  /**
   * @return string[]
   */
  public static function projectDestinationChoices(
    string $type,
    string $rootProjectDir,
    string $drupalRootDir
  ): array {
    assert(
      in_array($type, static::DRUPAL_PROJECT_TYPES),
      "invalid project type: '$type'",
    );

    $choices = [];

    $vendorDrupalDir = "$rootProjectDir/../../drupal";
    $fs = new Filesystem();
    if ($fs->exists($vendorDrupalDir)) {
      $choices[] = $fs->makePathRelative($vendorDrupalDir, $drupalRootDir);
    }

    switch ($type) {
      case 'profile':
      case 'module':
      case 'theme':
        $choices[] = $fs->makePathRelative("$drupalRootDir/{$type}s/custom", $drupalRootDir);
        break;

      case 'theme_engine':
        $choices[] = $fs->makePathRelative("$drupalRootDir/themes/custom", $drupalRootDir);
        break;
    }

    return $choices;
  }

  public static function getStackedValidator(callable ...$validators): callable {
    return function ($input) use ($validators) {
      foreach ($validators as $validator) {
        $input = $validator($input);
      }

      return $input;
    };
  }

  public static function getConfigEntityIdExistsValidator(string $message, ConfigEntityStorageInterface $storage, bool $hasToBeExists = FALSE): callable {
    return function ($value) use ($message, $storage, $hasToBeExists) {
      if ($value === NULL || $value === '') {
        return $value;
      }

      $msgText = '';
      $entity = $storage->load($value);
      if ($entity && !$hasToBeExists) {
        $msgText = '@message: @entity_type_label %machine_name already exists';
      }
      elseif (!$entity && $hasToBeExists) {
        $msgText = '@message: @entity_type_label %machine_name not exists';
      }

      if ($msgText) {
        $msgArgs = [
          '@message' => $message,
          '@entity_type_label' => $storage->getEntityType()->getLabel(),
          '%machine_name' => $value,
        ];

        throw new \InvalidArgumentException(strtr($msgText, $msgArgs), 1);
      }

      return $value;
    };
  }

  public static function getRequiredValidator(string $message): callable {
    return function ($input) use ($message) {
      // FALSE is not considered as empty value because question helper uses
      // it as negative answer on confirmation questions.
      if ($input === NULL || $input === '') {
        throw new \UnexpectedValueException($message);
      }

      return $input;
    };
  }

  public static function getRegexpValidator(string $message, string $pattern, bool $invert = FALSE): callable {
    return function ($input) use ($message, $pattern, $invert) {
      if ($input === NULL || $input === '') {
        return $input;
      }

      $valid = (bool) preg_match($pattern, $input);
      $valid = $invert ? !$valid : $valid;
      if ($valid) {
        return $input;
      }

      throw new \InvalidArgumentException(
        strtr(
          '@message; pattern @pattern does not match to input: @input',
          [
            '@message' => $message,
            '@pattern' => $pattern,
            '@input' => $input,
          ],
        ),
        1,
      );
    };
  }

  /**
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface[] $entities
   *
   * @return string[]
   */
  public static function configEntityChoices(iterable $entities): array {
    $choices = [];
    foreach ($entities as $entity) {
      $choices[$entity->id()] = $entity->label();
    }

    return $choices;
  }

  /**
   * @param \Drupal\views\Entity\View[] $views
   * @param string $path
   */
  public static function findViewDisplayByPath(iterable $views, string $path): ?array {
    foreach ($views as $view) {
      foreach ($view->get('display') as $displayId => $display) {
        $actual = $display['display_options']['path'] ?? NULL;
        if ($actual === $path) {
          return [
            'view' => $view,
            'display_id' => $displayId,
          ];
        }
      }
    }

    return NULL;
  }

  public static function openFileUrl(string $fileName, int $line = 0, int $column = 0, string $basePath = '') {
    if ($basePath === '') {
      $basePath = getcwd();
    }

    return strtr(
      ini_get('xdebug.file_link_format') ?: 'phpstorm://open?file=%f&line=%l&column=%c',
      [
        '%f' => urlencode(Path::makeAbsolute($fileName, $basePath)),
        '%l' => $line,
        '%c' => $column,
      ],
    );
  }

  public static function className(string $fqn): string {
    return preg_replace('@^.*\\\\@', '', $fqn);
  }

  /**
   * @param string $namespace
   *   Fully qualified name.
   *
   * @return string
   *   Relative path from the MODULE/src directory.
   */
  public static function relativeNamespaceDir(string $namespace): string {
    $namespaceParts = array_slice(explode('\\', $namespace), 2);

    return Path::join($namespaceParts) ?: '.';
  }

  public static function addUseStatements(array $useStatements, string $fileContent): string {
    $pos = strpos($fileContent, " */\n");
    if ($pos !== FALSE) {
      $pos += 4;
    }
    else {
      $pos = strpos($fileContent, "\nuse ");
      if ($pos !== FALSE) {
        $pos -= 5;
      }
    }

    if ($pos === FALSE) {
      // @todo Better detection.
      return $fileContent;
    }

    // @todo Sort.
    foreach (array_reverse($useStatements) as $useStatement) {
      if (strpos($fileContent, $useStatement) !== FALSE) {
        continue;
      }

      $fileContent = substr_replace(
        $fileContent,
        "$useStatement\n",
        $pos,
        0,
      );
    }

    return $fileContent;
  }

  public static function inputName(array $parents): string {
    $name = array_shift($parents);
    if ($parents) {
      $name .= '[' . implode('][', $parents) . ']';
    }

    return $name;
  }

}
