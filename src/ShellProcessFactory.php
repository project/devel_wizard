<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard;

use Symfony\Component\Process\Process;

class ShellProcessFactory implements ShellProcessFactoryInterface {

  protected string $processClass;

  public function __construct(string $processClass) {
    $this->processClass = $processClass;
  }

  public function createInstance(
    array $command,
    string $cwd = NULL,
    array $env = NULL,
    $input = NULL,
    ?float $timeout = 60
  ): Process {
    return new $this->processClass($command, $cwd, $env, $input, $timeout);
  }

  public function createInstanceFromString(
    string $command,
    string $cwd = NULL,
    array $env = NULL,
    $input = NULL,
    ?float $timeout = 60
  ): Process {
    return $this->processClass::fromShellCommandline($command, $cwd, $env, $input, $timeout);
  }

}
