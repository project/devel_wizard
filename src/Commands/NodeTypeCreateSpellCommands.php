<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Console\Input\InputInterface;

class NodeTypeCreateSpellCommands extends SpellCommandsBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($spell);
  }

  /**
   * @hook interact devel-wizard:spell:node-type:create
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'nodeType';
    $nodeTypeId = $input->getArgument($argName);
    if (!$nodeTypeId) {
      $nodeTypeStorage = $this->entityTypeManager->getStorage('node_type');
      $nodeTypeId = $io->ask(
        "1/1 argument <comment>$argName</comment> - Machine name of the new Content type",
        NULL,
        Utils::getStackedValidator(
          Utils::getRegexpValidator(
            "format of the $argName is invalid",
            Utils::MACHINE_NAME_REGEXP,
          ),
          Utils::getConfigEntityIdExistsValidator(
            "$argName already exists",
            $nodeTypeStorage,
          ),
        ),
      );
      $input->setArgument($argName, $nodeTypeId);
    }
  }

  /**
   * Creates a new Content type without any extra.
   *
   * @param string $nodeType
   *   The machine-name of the new Content Type.
   *
   * @command devel-wizard:spell:node-type:create
   * @aliases dw:s:nt:create
   * @bootstrap full
   * @validate-module-enabled node
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(string $nodeType): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $nodeTypeId = $this->input()->getArgument('nodeType');

    $settings = [
      'values' => [
        'type' => $nodeTypeId,
      ],
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
