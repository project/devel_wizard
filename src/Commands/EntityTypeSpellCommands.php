<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Console\Input\InputInterface;

class EntityTypeSpellCommands extends SpellCommandsBase {

  /**
   * @hook interact devel-wizard:spell:entity-type
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'contentEntityTypeId';
    $contentEntityTypeId = $input->getArgument($argName);
    if (!$contentEntityTypeId) {
      $contentEntityTypeId = $io->ask(
        "1/1 argument <comment>$argName</comment> - Machine-name of the new content entity type",
        NULL,
        Utils::getStackedValidator(
          Utils::getRequiredValidator("argument $argName is required"),
          Utils::getRegexpValidator("format of the $argName is invalid", Utils::MACHINE_NAME_REGEXP),
        ),
      );
      $input->setArgument($argName, (string) $contentEntityTypeId);
    }
  }

  /**
   * @param string $contentEntityTypeId
   *   Machine-name of the new content entity type.
   * @param string $configEntityTypeId
   *   Machine-name of the new config entity type.
   * @param string $module
   *   Machine-name of the module to put the code files into.
   *
   * @command devel-wizard:spell:entity-type
   * @aliases dw:s:et
   * @bootstrap full
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(
    string $contentEntityTypeId,
    string $configEntityTypeId = '',
    string $module = ''
  ): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $input = $this->input();

    $settings = [
      'module' => [
        'machine_name' => $input->getArgument('module'),
      ],
      'config' => [
        'id' => $input->getArgument('configEntityTypeId'),
      ],
      'content' => [
        'id' => $input->getArgument('contentEntityTypeId'),
      ],
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
