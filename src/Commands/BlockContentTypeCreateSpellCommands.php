<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Console\Input\InputInterface;

class BlockContentTypeCreateSpellCommands extends SpellCommandsBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($spell);
  }

  /**
   * @hook interact devel-wizard:spell:block-content-type:create
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'blockContentType';
    $blockContentTypeId = $input->getArgument($argName);
    if (!$blockContentTypeId) {
      $blockContentTypeStorage = $this
        ->entityTypeManager
        ->getStorage('block_content_type');
      $blockContentTypeId = $io->ask(
        "1/1 argument <comment>$argName</comment> - Machine name of the new Block content type",
        NULL,
        Utils::getStackedValidator(
          Utils::getRegexpValidator(
            "format of the $argName is invalid",
            Utils::MACHINE_NAME_REGEXP,
          ),
          Utils::getConfigEntityIdExistsValidator(
            "$argName already exists",
            $blockContentTypeStorage,
          ),
        ),
      );
      $input->setArgument($argName, $blockContentTypeId);
    }
  }

  /**
   * Creates a new Block content type without any extra.
   *
   * @param string $blockContentType
   *   Machine name of the new Block content type.
   *
   * @command devel-wizard:spell:block-content-type:create
   * @aliases dw:s:bct:create
   * @bootstrap full
   * @validate-module-enabled block_content
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(string $blockContentType): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $blockContentType = $this->input()->getArgument('blockContentType');

    $settings = [
      'values' => [
        'id' => $blockContentType,
      ],
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
