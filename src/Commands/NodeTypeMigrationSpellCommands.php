<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Console\Input\InputInterface;

class NodeTypeMigrationSpellCommands extends SpellCommandsBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($spell);
  }

  /**
   * @hook interact devel-wizard:spell:node-type:migration
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'nodeType';
    $argValue = $input->getArgument($argName);
    if (!$argValue) {
      $nodeTypeStorage = $this->entityTypeManager->getStorage('node_type');
      $argValue = $io->choice(
        "1/1 argument <comment>$argName</comment> - Machine-name of the Content type to generate migration for",
        Utils::configEntityChoices($nodeTypeStorage->loadMultiple()),
      );
      $input->setArgument($argName, (string) $argValue);
    }

    // @todo Get the "app_" prefix from $config['field_ui.settings']['field_prefix'].
    $prefix = 'app_';
    $options = [
      'migration-module' => [
        'value' => $input->getOption('migration-module'),
        'question' => 'option <comment>--migration-module</comment> - Machine name of the module that should contain the migration definition',
        'default' => "{$prefix}dc",
      ],
      'migration-group' => [
        'value' => $input->getOption('migration-group'),
        'question' => "option <comment>--migration-group</comment> - The name of the migration_group eg: {$prefix}dummy or {$prefix}default",
        'default' => "{$prefix}dummy",
      ],
    ];

    foreach ($options as $name => $option) {
      if ($option['value']) {
        continue;
      }

      $value = $io->ask($option['question'], $option['default'] ?? NULL);
      $input->setOption($name, (string) $value);
    }
  }

  /**
   * Creates a migration definitions and data sources for an existing Content type.
   *
   * @param string $nodeType
   *   Machine-name of the Content type to generate migration for.
   * @param array $options
   *
   * @option string $migration-module
   *   Machine name of the module that should contain the migration definition.
   * @option string $migration-group
   *   The name of the migration group eg: app_dummy or app_default.
   *
   * @command devel-wizard:spell:node-type:migration
   * @aliases dw:s:nt:migration
   * @bootstrap full
   * @validate-module-enabled node
   * @validate-entity-load node_type nodeType
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(
    string $nodeType = '',
    array $options = [
      'migration-module' => '',
      'migration-group' => '',
    ]
  ): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $input = $this->input();

    $settings = [
      'machine_name' => $input->getArgument('nodeType'),
      'migration_module' => $input->getOption('migration-module'),
      'migration_group' => $input->getOption('migration-group'),
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
