<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Console\Input\InputInterface;

class NodeTypeAdminViewSpellCommands extends SpellCommandsBase {

  protected string $contentEntityTypeId = 'node';

  protected EntityTypeManagerInterface $entityTypeManager;

  protected ConfigEntityStorageInterface $bundleStorage;

  protected PathValidatorInterface $pathValidator;

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager,
    PathValidatorInterface $pathValidator
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->pathValidator = $pathValidator;

    parent::__construct($spell);
  }

  /**
   * @hook init
   */
  public function hookInit() {
    /** @var \Drupal\Core\Entity\EntityTypeInterface $contentEntityType */
    $contentEntityType = $this->entityTypeManager->getDefinition($this->contentEntityTypeId);
    $configEntityTypeId = $contentEntityType->getBundleEntityType();

    /** @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $bundleStorage */
    $bundleStorage = $this->entityTypeManager->getStorage($configEntityTypeId);
    $this->bundleStorage = $bundleStorage;
  }

  /**
   * @hook interact devel-wizard:spell:node-type:admin-view
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'machineName';
    $machineName = $input->getArgument($argName);
    if (!$machineName) {
      $choices = array_filter(
        Utils::configEntityChoices($this->bundleStorage->loadMultiple()),
        $this->getBundleFilter('/admin/content/node/list-{{ bundle }}'),
        ARRAY_FILTER_USE_KEY,
      );

      if (!$choices) {
        $this->logger()->error('There is no Content type without admin list');
      }

      $machineName = $io->choice(
        "1/1 argument <comment>$argName</comment> - Machine-name of an existing Content type",
        $choices,
      );

      $input->setArgument($argName, $machineName);
    }
    $options = [
      'module' => [
        'value' => $input->getOption('module'),
        'question' => 'option <comment>---module</comment> - Machine name of the module that should contain the derivatives',
        'default' => "app_core",
      ],
    ];

    foreach ($options as $name => $option) {
      if ($option['value']) {
        continue;
      }

      $value = $io->ask($option['question'], $option['default'] ?? NULL);
      $input->setOption($name, (string) $value);
    }
  }

  /**
   * Creates a view for administrators for an existing Content type.
   *
   * @param string $machineName
   *   Machine-name of an existing Content type.
   * @param array $options
   *
   * @option string $module
   *   Machine name of the module that should contain the derivatives.
   *
   * @command devel-wizard:spell:node-type:admin-view
   * @aliases dw:s:nt:admin-view
   * @bootstrap full
   * @validate-module-enabled node
   * @validate-entity-load node_type machineName
   *
   * @noinspection PhpUnusedParameterInspection
   *
   * @todo Rename $machineName argument to $nodeType. It'll affect other classes as well.
   */
  public function cast(
    string $machineName,
    array $options = [
      'module' => '',
    ]
  ): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $input = $this->input();

    $settings = [
      'machine_name' => $input->getArgument('machineName'),
      'module' => [
        'machine_name' => $input->getOption('module'),
      ],
    ];

    return $this->spell->applyDefaultValues($settings);
  }

  protected function getBundleFilter(string $pathPattern, bool $invert = FALSE): callable {
    return function ($bundle) use ($pathPattern, $invert) {
      $path = strtr(
        $pathPattern,
        [
          '{{ bundle }}' => $bundle,
        ],
      );

      $result = $this->pathValidator->getUrlIfValid($path) !== NULL;

      return $invert ? !$result : $result;
    };
  }

}
