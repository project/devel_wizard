<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\devel_wizard\SpellInterface;
use Drush\Commands\DrushCommands;
use Psr\Log\LoggerAwareInterface;
use Symfony\Component\Console\Command\Command;

abstract class SpellCommandsBase extends DrushCommands {

  protected SpellInterface $spell;

  public function __construct(SpellInterface $spell) {
    parent::__construct();
    $this->spell = $spell;
  }

  /**
   * @hook option
   */
  public function hookOption(Command $command) {
    if (!$command->getHelp()) {
      $command->setHelp((string) $this->spell->help());
    }

    if (!$command->getDescription()) {
      $command->setDescription((string) $this->spell->description());
    }
  }

  /**
   * @hook validate
   */
  public function validate(CommandData $commandData) {
    $settings = $this->buildSpellSettingsFromInput();

    $logger = $this->logger();
    $validationErrors = $this->spell->validate($settings);
    /** @var \Symfony\Component\Validator\ConstraintViolationInterface $validationError */
    foreach ($validationErrors as $validationError) {
      $logger->error($validationError->getMessage());
    }

    if ($validationErrors->count() > 0) {
      throw new \Exception('There were some problems with your input');
    }
  }

  protected function doIt(): CommandResult {
    $data = [];
    $exitCode = 0;

    $this->prepareSpellLogger();
    $settings = $this->buildSpellSettingsFromInput();
    $this->spell->abracadabra($settings);

    return CommandResult::dataWithExitCode($data, $exitCode);
  }

  /**
   * @return $this
   */
  protected function prepareSpellLogger() {
    if ($this->spell instanceof LoggerAwareInterface) {
      $this->spell->setLogger($this->logger());
    }

    return $this;
  }

  abstract protected function buildSpellSettingsFromInput(): array;

}
