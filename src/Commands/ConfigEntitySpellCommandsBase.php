<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Drupal\devel_wizard\ConfigEntitySpellTrait;
use Drupal\devel_wizard\Utils as WizardUtils;
use Stringy\StaticStringy;

abstract class ConfigEntitySpellCommandsBase extends SpellCommandsBase {

  use ConfigEntitySpellTrait;

  protected function getConfigEntityIdInteractValidator(string $argType = 'argument', string $argName = '', bool $hasToBeExists = FALSE): \Closure {
    $argName = $argName ?: StaticStringy::camelize($this->configEntityTypeId);

    $machineNameValidator = WizardUtils::getRegexpValidator(
      "$argType $argName",
      WizardUtils::MACHINE_NAME_REGEXP,
    );

    $configStorage = $this->getConfigStorage();
    if (!$configStorage) {
      return $machineNameValidator;
    }

    // @todo When $hasToBeExists is TRUE then, the $machineNameValidator is unnecessary.
    return WizardUtils::getStackedValidator(
      $machineNameValidator,
      WizardUtils::getConfigEntityIdExistsValidator(
        "$argType $argName",
        $configStorage,
        $hasToBeExists,
      ),
    );
  }

}
