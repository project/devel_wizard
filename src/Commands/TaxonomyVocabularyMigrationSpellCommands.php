<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Console\Input\InputInterface;

class TaxonomyVocabularyMigrationSpellCommands extends SpellCommandsBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($spell);
  }

  /**
   * @hook interact devel-wizard:spell:taxonomy-vocabulary:migration
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'taxonomyVocabulary';
    $argValue = $input->getArgument($argName);
    if (!$argValue) {
      $taxonomyVocabularyStorage = $this->entityTypeManager->getStorage('taxonomy_vocabulary');
      $argValue = $io->choice(
        "argument <comment>$argName</comment> - Machine-name of the Taxonomy vocabulary to generate migration for",
        Utils::configEntityChoices($taxonomyVocabularyStorage->loadMultiple()),
      );
      $input->setArgument($argName, (string) $argValue);
    }

    // @todo Get the "app_" prefix from $config['field_ui.settings']['field_prefix'].
    $prefix = 'app_';
    $options = [
      'migration-module' => [
        'value' => $input->getOption('migration-module'),
        'question' => 'option <comment>--migration-module</comment> - Machine name of the module that should contain the migration definition',
        'default' => "{$prefix}dc",
      ],
      'migration-group' => [
        'value' => $input->getOption('migration-group'),
        'question' => "option <comment>--migration-group</comment> - The name of the migration_group eg: {$prefix}dummy or {$prefix}default",
        'default' => "{$prefix}default",
      ],
    ];

    foreach ($options as $name => $option) {
      if ($option['value']) {
        continue;
      }

      $value = $io->ask($option['question'], $option['default'] ?? NULL);
      $input->setOption($name, (string) $value);
    }
  }

  /**
   * Creates a migration definitions and data sources for an existing Taxonomy vocabulary.
   *
   * @param string $taxonomyVocabulary
   *   Machine-name of the Taxonomy vocabulary to generate migration for.
   * @param array $options
   *
   * @option string $migration-module
   *   Machine name of the module that should contain the migration definition.
   * @option string $migration-group
   *   The name of the migration group eg: app_dummy or app_default.
   *
   * @command devel-wizard:spell:taxonomy-vocabulary:migration
   * @aliases dw:s:tv:migration
   * @bootstrap full
   * @validate-module-enabled taxonomy
   * @validate-entity-load taxonomy_vocabulary taxonomyVocabulary
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(
    string $taxonomyVocabulary = '',
    array $options = [
      'migration-module' => '',
      'migration-group' => '',
    ]
  ): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $input = $this->input();

    $settings = [
      'machine_name' => $input->getArgument('taxonomyVocabulary'),
      'migration_module' => $input->getOption('migration-module'),
      'migration_group' => $input->getOption('migration-group'),
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
