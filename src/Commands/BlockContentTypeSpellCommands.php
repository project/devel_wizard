<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Console\Input\InputInterface;

class BlockContentTypeSpellCommands extends SpellCommandsBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($spell);
  }

  /**
   * @hook interact devel-wizard:spell:block-content-type
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'blockContentType';
    $blockContentTypeId = $input->getArgument($argName);
    if (!$blockContentTypeId) {
      $blockContentTypeStorage = $this
        ->entityTypeManager
        ->getStorage('block_content_type');
      $blockContentTypeId = $io->ask(
        "1/1 argument <comment>$argName</comment> - Machine name of the new Block content type",
        NULL,
        Utils::getStackedValidator(
          Utils::getRegexpValidator(
            "argument $argName required",
            Utils::MACHINE_NAME_REGEXP,
          ),
          Utils::getConfigEntityIdExistsValidator(
            "format of the $argName is invalid",
            $blockContentTypeStorage,
          ),
        ),
      );

      $input->setArgument($argName, $blockContentTypeId);
    }
  }

  /**
   * Creates a Block content type with all the bells and whistles.
   *
   * @param string $blockContentType
   *   Machine-name of the new Block content type.
   *
   * @command devel-wizard:spell:block-content-type
   * @aliases dw:s:bct
   * @bootstrap full
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(string $blockContentType): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $input = $this->input();
    $arguments = $input->getArguments();

    $settings = [
      'machine_name' => $arguments['blockContentType'],
      'create' => [
        'settings' => [
          'values' => [
            'id' => $arguments['blockContentType'],
          ],
        ],
      ],
      'role' => [
        'settings' => [
          'machine_name' => $arguments['blockContentType'],
        ],
      ],
      'migration' => [
        'settings' => [
          'machine_name' => $arguments['blockContentType'],
        ],
      ],
      'behat' => [
        'settings' => [
          'machine_name' => $arguments['blockContentType'],
        ],
      ],
      'admin_view' => [
        'settings' => [
          'machine_name' => $arguments['blockContentType'],
        ],
      ],
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
