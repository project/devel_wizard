<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\devel_wizard\SpellInterface;

abstract class TaxonomyVocabularySpellCommandsBase extends ConfigEntitySpellCommandsBase {

  protected string $provider = 'taxonomy';

  protected string  $configEntityTypeId = 'taxonomy_vocabulary';

  protected string $contentEntityTypeId = 'taxonomy_term';

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($spell);
  }

}
