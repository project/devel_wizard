<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Console\Input\InputInterface;

class NodeTypeRoleSpellCommands extends SpellCommandsBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($spell);
  }

  /**
   * @hook interact devel-wizard:spell:node-type:role
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'nodeType';
    $nodeTypeId = $input->getArgument($argName);
    if (!$nodeTypeId) {
      $nodeTypeStorage = $this->entityTypeManager->getStorage('node_type');
      $nodeTypeId = $io->choice(
        "1/1 argument <comment>$argName</comment> - The machine-name of the Content type to create the new role for",
        Utils::configEntityChoices($nodeTypeStorage->loadMultiple()),
      );

      $input->setArgument($argName, $nodeTypeId);
    }
  }

  /**
   * Creates a new user role which grants full control over a specific Content type.
   *
   * @param string $nodeType
   *   The machine name of the Content type to create the new role for.
   *
   * @command devel-wizard:spell:node-type:role
   * @aliases dw:s:nt:role
   * @bootstrap full
   * @validate-module-enabled node
   * @validate-entity-load node_type nodeType
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(string $nodeType): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $input = $this->input();

    $settings = [
      'machine_name' => $input->getArgument('nodeType'),
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
