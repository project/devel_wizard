<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\devel_wizard\Utils as WizardUtils;
use Symfony\Component\Console\Input\InputInterface;

class TaxonomyVocabularyRoleSpellCommands extends TaxonomyVocabularySpellCommandsBase {

  /**
   * @hook interact devel-wizard:spell:taxonomy-vocabulary:role
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'taxonomyVocabulary';
    $taxonomyVocabularyId = $input->getArgument($argName);
    if (!$taxonomyVocabularyId) {
      $configStorage = $this->getConfigStorage();
      $taxonomyVocabularyId = $io->choice(
        "1/1 argument <comment>$argName</comment> - The machine-name of the Taxonomy vocabulary to create the new role for",
        WizardUtils::configEntityChoices($configStorage->loadMultiple()),
      );

      $input->setArgument($argName, $taxonomyVocabularyId);
    }
  }

  /**
   * Creates a new user role which grants full control over a specific Taxonomy vocabulary.
   *
   * @param string $taxonomyVocabulary
   *   The machine name of the Taxonomy vocabulary to create the new role for.
   *
   * @command devel-wizard:spell:taxonomy-vocabulary:role
   * @aliases dw:s:tv:role
   * @bootstrap full
   * @validate-module-enabled taxonomy
   * @validate-entity-load taxonomy_vocabulary taxonomyVocabulary
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(string $taxonomyVocabulary): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $input = $this->input();

    $settings = [
      'machine_name' => $input->getArgument('taxonomyVocabulary'),
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
