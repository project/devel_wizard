<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Console\Input\InputInterface;

class TaxonomyVocabularyBehatSpellCommands extends SpellCommandsBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($spell);
  }

  /**
   * @hook interact devel-wizard:spell:taxonomy-vocabulary:behat
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'taxonomyVocabulary';
    $taxonomyVocabularyId = $input->getArgument($argName);
    if (!$taxonomyVocabularyId) {
      $taxonomyVocabularyStorage = $this->entityTypeManager->getStorage('taxonomy_vocabulary');
      $taxonomyVocabularyId = $io->choice(
        "1/1 argument <comment>$argName</comment> - The machine-name of the Taxonomy vocabulary to create Behat tests for",
        Utils::configEntityChoices($taxonomyVocabularyStorage->loadMultiple()),
      );
      $input->setArgument($argName, $taxonomyVocabularyId);
    }
  }

  /**
   * Generates Behat tests for an existing Taxonomy vocabulary.
   *
   * @param string $taxonomyVocabulary
   *   The machine-name of the Taxonomy vocabulary to create Behat tests for.
   *
   * @command devel-wizard:spell:taxonomy-vocabulary:behat
   * @aliases dw:s:tv:behat
   * @bootstrap full
   * @validate-module-enabled taxonomy
   * @validate-entity-load taxonomy_vocabulary taxonomyVocabulary
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(string $taxonomyVocabulary): CommandResult {
    return $this->doIt();
  }

  /**
   * {@inheritdoc}
   */
  protected function buildSpellSettingsFromInput(): array {
    $taxonomyVocabularyId = $this->input()->getArgument('taxonomyVocabulary');

    $settings = [
      'machine_name' => $taxonomyVocabularyId,
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
