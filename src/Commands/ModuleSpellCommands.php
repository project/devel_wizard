<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Webmozart\PathUtil\Path;

class ModuleSpellCommands extends SpellCommandsBase {

  /**
   * @hook interact devel-wizard:spell:module
   */
  public function interact(InputInterface $input) {
    $config = $this->getConfig();
    $io = $this->io();

    $type = 'module';
    $rootProjectDir = $config->get('runtime.project');
    $drupalRootDir = $config->get('options.root');

    $argName = 'machineName';
    $machineName = $input->getArgument($argName);
    if (!$machineName) {
      $machineName = $io->ask(
        "1/2 argument <comment>$argName</comment> - Machine-name of the new module",
        NULL,
        Utils::getStackedValidator(
          Utils::getRequiredValidator(
            "$argName argument is required",
          ),
          Utils::getRegexpValidator(
            "format of the $argName is invalid",
            Utils::MACHINE_NAME_REGEXP,
          ),
        ),
      );

      $input->setArgument($argName, $machineName);
    }

    $optionName = 'parent-dir';
    $parentDir = $input->getOption($optionName);
    if (!$parentDir) {
      $choices = Utils::projectDestinationChoices($type, $rootProjectDir, $drupalRootDir);
      $question = new ChoiceQuestion(
        "2/2 option <comment>--{$optionName}</comment> - Parent directory",
        $choices,
      );
      $parentDir = $io->askQuestion($question);
      $input->setOption($optionName, $parentDir);
    }
  }

  /**
   * @param string $machineName
   *   Machine-name of the new module.
   * @param array $options
   *
   * @option string $parent-dir
   *   Most of the time it is: DRUPAL_ROOT/modules/contrib/
   *
   * @command devel-wizard:spell:module
   * @aliases dw:s:m
   * @bootstrap full
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(
    string $machineName,
    array $options = [
      'parent-dir' => '',
    ]
  ): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $config = $this->getConfig();
    $input = $this->input();

    $drupalRootDir = $config->get('options.root');
    $parentDir = $input->getOption('parent-dir');

    $commonBasePath = Path::getLongestCommonBasePath([
      $drupalRootDir,
      Path::makeAbsolute($parentDir, $drupalRootDir),
    ]);

    $settings = [
      'machine_name' => $input->getArgument('machineName'),
      'parent_dir' => $parentDir,
      'is_standalone' => in_array($commonBasePath, ['/', NULL]),
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
