<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Console\Input\InputInterface;

class BlockContentTypeBehatSpellCommands extends SpellCommandsBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($spell);
  }

  /**
   * @hook interact devel-wizard:spell:block-content-type:behat
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'blockContentType';
    $blockContentTypeId = $input->getArgument($argName);
    if (!$blockContentTypeId) {
      $blockContentTypeStorage = $this
        ->entityTypeManager
        ->getStorage('block_content_type');
      $blockContentTypeId = $io->choice(
        "1/1 argument <comment>$argName</comment> - The machine-name of the Block content type to create Behat tests for",
        Utils::configEntityChoices($blockContentTypeStorage->loadMultiple()),
      );
      $input->setArgument($argName, $blockContentTypeId);
    }
  }

  /**
   * Generates Behat tests for an existing Block content type.
   *
   * @param string $blockContentType
   *   The machine-name of the Block content type to create Behat tests for.
   *
   * @command devel-wizard:spell:block-content-type:behat
   * @aliases dw:s:bct:behat
   * @bootstrap full
   * @validate-module-enabled block_content
   * @validate-entity-load block_content_type blockContentType
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(string $blockContentType): CommandResult {
    return $this->doIt();
  }

  /**
   * {@inheritdoc}
   */
  protected function buildSpellSettingsFromInput(): array {
    $blockContentTypeId = $this->input()->getArgument('blockContentType');

    $settings = [
      'machine_name' => $blockContentTypeId,
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
