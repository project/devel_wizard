<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils as WizardUtils;
use Symfony\Component\Console\Input\InputInterface;

class TaxonomyVocabularySpellCommands extends SpellCommandsBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($spell);
  }

  /**
   * @hook interact devel-wizard:spell:taxonomy-vocabulary
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'taxonomyVocabulary';
    $taxonomyVocabularyId = $input->getArgument($argName);
    if (!$taxonomyVocabularyId) {
      $taxonomyVocabularyStorage = $this->entityTypeManager->getStorage('taxonomy_vocabulary');
      $taxonomyVocabularyId = $io->ask(
        "1/1 argument <comment>$argName</comment> - Machine name of the new Taxonomy vocabulary",
        NULL,
        WizardUtils::getStackedValidator(
          WizardUtils::getRegexpValidator(
            'argument taxonomyVocabulary',
            WizardUtils::MACHINE_NAME_REGEXP,
          ),
          WizardUtils::getConfigEntityIdExistsValidator(
            "argument $argName",
            $taxonomyVocabularyStorage,
          ),
        ),
      );

      $input->setArgument($argName, $taxonomyVocabularyId);
    }
  }

  /**
   * Creates a Taxonomy vocabulary with all the bells and whistles.
   *
   * @param string $taxonomyVocabulary
   *   Machine-name of the new Content type.
   *
   * @command devel-wizard:spell:taxonomy-vocabulary
   * @aliases dw:s:tv
   * @bootstrap full
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(string $taxonomyVocabulary): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $input = $this->input();
    $arguments = $input->getArguments();

    $settings = [
      'vid' => $arguments['taxonomyVocabulary'],
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
