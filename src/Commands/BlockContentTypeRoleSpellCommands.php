<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Console\Input\InputInterface;

class BlockContentTypeRoleSpellCommands extends SpellCommandsBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($spell);
  }

  /**
   * @hook interact devel-wizard:spell:block-content-type:role
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'blockContentType';
    $blockContentTypeId = $input->getArgument($argName);
    if (!$blockContentTypeId) {
      $blockContentTypeStorage = $this
        ->entityTypeManager
        ->getStorage('block_content_type');
      $blockContentTypeId = $io->choice(
        "1/1 argument <comment>$argName</comment> - The machine-name of the Block content type to create the new role for",
        Utils::configEntityChoices($blockContentTypeStorage->loadMultiple()),
      );

      $input->setArgument($argName, $blockContentTypeId);
    }
  }

  /**
   * Creates a new user role which grants full control over a specific Block content type.
   *
   * @param string $blockContentType
   *   The machine name of the Block content type to create the new role for.
   *
   * @command devel-wizard:spell:block-content-type:role
   * @aliases dw:s:bct:role
   * @bootstrap full
   * @validate-module-enabled block_content
   * @validate-entity-load block_content_type blockContentType
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(string $blockContentType): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $input = $this->input();

    $settings = [
      'machine_name' => $input->getArgument('blockContentType'),
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
