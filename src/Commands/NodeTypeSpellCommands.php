<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Stringy\StaticStringy;
use Symfony\Component\Console\Input\InputInterface;

class NodeTypeSpellCommands extends SpellCommandsBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  protected string $configEntityTypeId = 'node_type';

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($spell);
  }

  /**
   * @hook interact devel-wizard:spell:node-type
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = StaticStringy::camelize($this->configEntityTypeId);
    $bundleId = $input->getArgument($argName);
    if (!$bundleId) {
      /** @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $configEntityStorage */
      $configEntityStorage = $this->entityTypeManager->getStorage($this->configEntityTypeId);
      $bundleId = $io->ask(
        "1/1 argument <comment>$argName</comment> - Machine name of the new Content type",
        NULL,
        Utils::getStackedValidator(
          Utils::getRegexpValidator(
            "format of the $argName is invalid",
            Utils::MACHINE_NAME_REGEXP,
          ),
          Utils::getConfigEntityIdExistsValidator(
            "$argName already exists",
            $configEntityStorage,
          ),
        ),
      );

      $input->setArgument($argName, $bundleId);
    }
  }

  /**
   * Creates a Content type with all the bells and whistles.
   *
   * @param string $nodeType
   *   Machine-name of the new Content type.
   *
   * @command devel-wizard:spell:node-type
   * @aliases dw:s:nt
   * @bootstrap full
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(string $nodeType): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $input = $this->input();
    $arguments = $input->getArguments();

    $argName = StaticStringy::camelize($this->configEntityTypeId);
    $settings = [
      'machine_name' => $arguments[$argName],
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
