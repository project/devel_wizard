<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\devel_wizard\SpellInterface;
use Drupal\devel_wizard\Utils;
use Symfony\Component\Console\Input\InputInterface;

class NodeTypeBehatSpellCommands extends SpellCommandsBase {

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    SpellInterface $spell,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($spell);
  }

  /**
   * @hook interact devel-wizard:spell:node-type:behat
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'nodeType';
    $nodeTypeId = $input->getArgument($argName);
    if (!$nodeTypeId) {
      $nodeTypeStorage = $this->entityTypeManager->getStorage('node_type');
      $nodeTypeId = $io->choice(
        "1/1 argument <comment>$argName</comment> - The machine-name of the Content type to create Behat tests for",
        Utils::configEntityChoices($nodeTypeStorage->loadMultiple()),
      );
      $input->setArgument($argName, $nodeTypeId);
    }
  }

  /**
   * Generates Behat tests for an existing Content type.
   *
   * @param string $nodeType
   *   The machine-name of the Content type to create Behat tests for.
   *
   * @command devel-wizard:spell:node-type:behat
   * @aliases dw:s:nt:behat
   * @bootstrap full
   * @validate-module-enabled node
   * @validate-entity-load node_type nodeType
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(string $nodeType): CommandResult {
    return $this->doIt();
  }

  /**
   * {@inheritdoc}
   */
  protected function buildSpellSettingsFromInput(): array {
    $nodeTypeId = $this->input()->getArgument('nodeType');

    $settings = [
      'machine_name' => $nodeTypeId,
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
