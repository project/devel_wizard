<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Symfony\Component\Console\Input\InputInterface;

class TaxonomyVocabularyCreateSpellCommands extends TaxonomyVocabularySpellCommandsBase {

  /**
   * @hook interact devel-wizard:spell:taxonomy-vocabulary:create
   */
  public function interact(InputInterface $input) {
    $io = $this->io();

    $argName = 'taxonomyVocabulary';
    $configEntityId = $input->getArgument($argName);
    if (!$configEntityId) {
      $configEntityId = $io->ask(
        "1/1 argument <comment>$argName</comment> - Machine name of the new Taxonomy vocabulary",
        NULL,
        $this->getConfigEntityIdInteractValidator('argument', $argName),
      );
      $input->setArgument($argName, $configEntityId);
    }
  }

  /**
   * Creates a new Taxonomy vocabulary without any extra.
   *
   * @param string $taxonomyVocabulary
   *   The machine-name of the new Taxonomy vocabulary.
   *
   * @command devel-wizard:spell:taxonomy-vocabulary:create
   * @aliases dw:s:tv:create
   * @bootstrap full
   * @validate-module-enabled taxonomy
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function cast(string $taxonomyVocabulary): CommandResult {
    return $this->doIt();
  }

  protected function buildSpellSettingsFromInput(): array {
    $taxonomyVocabularyId = $this->input()->getArgument('taxonomyVocabulary');

    $settings = [
      'values' => [
        'vid' => $taxonomyVocabularyId,
      ],
    ];

    return $this->spell->applyDefaultValues($settings);
  }

}
