<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Plugin\Validation\Constraint;

use Drupal\devel_wizard\Constraints\MigrationGroupValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Validates if migration group exists.
 *
 * @Constraint(
 *   id = "MigrationGroup",
 *   label = @Translation("Migration group"),
 *   type = {"string"},
 * )
 */
class MigrationGroupConstraint extends Constraint {

  /**
   * {@inheritdoc}
   */
  public function validatedBy() {
    return MigrationGroupValidator::class;
  }

}
