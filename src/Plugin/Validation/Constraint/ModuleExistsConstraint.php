<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Plugin\Validation\Constraint;

use Drupal\devel_wizard\Constraints\ModuleExistsValidator;
use Symfony\Component\Validator\Constraint;

/**
 * ModuleExists constraint.
 *
 * @Constraint(
 *   id = "ModuleExists",
 *   label = @Translation("Module exists"),
 *   type = {"string"},
 * )
 */
class ModuleExistsConstraint extends Constraint {

  /**
   * {@inheritdoc}
   */
  public function validatedBy() {
    return ModuleExistsValidator::class;
  }

}
