<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Plugin\Validation\Constraint;

use Drupal\devel_wizard\Constraints\ConfigEntityExistsValidator;
use Symfony\Component\Validator\Constraint;

/**
 * @Constraint(
 *   id = "DevelWizardConfigEntityExists",
 *   label = @Translation("Config entity exists or not"),
 *   type = {"string"},
 * )
 */
class ConfigEntityExistsConstraint extends Constraint {

  public string $messageEntityTypeIsMissing = 'Entity type %entity_type.id is missing';

  /**
   * @todo This message can expose all the Content type names for an unprivileged user.
   */
  public string $messageExpectedToBeExists = '@entity_type.label machine-name %bundle.id is invalid, it has to be one of: @bundle_list';

  public string $messageExpectedNotToBeExists = '@entity_type.label machine-name %bundle.id is already exists';

  public string $entityTypeId = '';

  public bool $hasToBeExists = TRUE;

  /**
   * {@inheritdoc}
   */
  public function getRequiredOptions() {
    $options = parent::getRequiredOptions();
    $options[] = 'entityTypeId';

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOption() {
    return 'entityTypeId';
  }

  /**
   * {@inheritdoc}
   */
  public function validatedBy() {
    return ConfigEntityExistsValidator::class;
  }

}
