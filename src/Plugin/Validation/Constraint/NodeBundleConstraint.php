<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Plugin\Validation\Constraint;

use Drupal\devel_wizard\Constraints\NodeBundleValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Validates if value is a Node bundle.
 *
 * @Constraint(
 *   id = "NodeBundle",
 *   label = @Translation("Node bundle"),
 *   type = {"string"},
 * )
 *
 * @package Drupal\devel_wizard\Plugin\Validation\Constraint
 */
class NodeBundleConstraint extends Constraint {

  /**
   * {@inheritdoc}
   */
  public function validatedBy() {
    return NodeBundleValidator::class;
  }

}
