<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleExtensionList;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ModuleAutocompleteController extends ControllerBase {

  protected ModuleExtensionList $moduleList;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('extension.list.module'),
    );
  }

  public function __construct(ModuleExtensionList $moduleList) {
    $this->moduleList = $moduleList;
  }

  public function body(Request $request) {
    $minLength = 3;
    $value = $request->query->get('q', '');
    if (mb_strlen($value) < $minLength) {
      return new JsonResponse([]);
    }

    $installedModules = $this->moduleList->getAllInstalledInfo();

    $matches = [];
    foreach ($installedModules as $machineName => $moduleInfo) {
      if (strpos($machineName, $value) !== FALSE) {
        $matches[] = [
          'value' => $machineName,
          'label' => $moduleInfo['name'],
        ];
      }
    }

    return new JsonResponse($matches);
  }

}
