<?php

declare(strict_types = 1);

namespace Drupal\devel_wizard;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * @todo Method ::checkRequirements().
 * @todo Method ::validate().
 * @todo Method ::contextFormBuild().
 * @todo Method ::contextFormValidate().
 * @todo Method ::contextFormSubmit().
 */
interface SpellInterface {

  /**
   * Machine-name of this spell.
   */
  public function id(): string;

  /**
   * Human readable label.
   */
  public function label(): TranslatableMarkup;

  /**
   * Short description.
   *
   * @see \Symfony\Component\Console\Command\Command::getHelp()
   */
  public function help(): TranslatableMarkup;

  /**
   * Long description.
   *
   * @see \Symfony\Component\Console\Command\Command::getDescription()
   */
  public function description(): TranslatableMarkup;

  public function configName(): string;

  /**
   * Creates a new config object from raw values.
   */
  public function config(array $values = []): Config;

  /**
   * Injects the default values into $settings.
   */
  public function applyDefaultValues(array $settings): array;

  /**
   * Validates the user input.
   *
   * @param array $values
   *   User input with default values.
   *
   * @see ::applyDefaultValues()
   */
  public function validate(array $values): ConstraintViolationListInterface;

  public function settingsFormBuild(array $parents, FormStateInterface $formState): array;

  /**
   * @return $this
   */
  public function abracadabra(array $settings);

}
