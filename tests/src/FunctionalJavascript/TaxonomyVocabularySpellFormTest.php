<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Drupal\taxonomy\Entity\Vocabulary;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.taxonomy_vocabulary
 * @group devel_wizard.spell.taxonomy_vocabulary.all_in_one
 *
 * @covers \Drupal\devel_wizard\Spell\TaxonomyVocabularySpell
 * @covers \Drupal\devel_wizard\SpellForm\TaxonomyVocabularySpellForm
 */
class TaxonomyVocabularySpellFormTest extends SpellFormTestBase {

  protected string $spellId = 'devel_wizard_taxonomy_vocabulary';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'taxonomy',
    'devel_wizard',
  ];

  /**
   * @group browser.chromium
   */
  public function testSubmitSuccess() {
    $taxonomyVocabularyId = 'dummy';
    $taxonomyVocabulary = Vocabulary::load($taxonomyVocabularyId);
    static::assertNull($taxonomyVocabulary, "taxonomy vocabulary $taxonomyVocabularyId not exists");

    $adminUser = $this->drupalCreateUserForSpell();
    $this->drupalLogin($adminUser);
    $this->navigateToSpellForm();

    $page = $this->getSession()->getPage();
    sleep(1);
    $page->fillField('Machine-readable name', $taxonomyVocabularyId);
    $page
      ->find(...$this->spellFormFinder(FALSE))
      ->submit();
    sleep(1);
    $this->waitForDocumentReady(60);

    $assert = $this->assertSession();
    $assert->pageTextContains("devel_wizard_taxonomy_vocabulary_create - Taxonomy vocabulary has been created: $taxonomyVocabularyId");

    // @todo The spell is unsuccessful because of the missing role_delegation module.
    $taxonomyVocabulary = Vocabulary::load($taxonomyVocabularyId);
    static::assertNotNull($taxonomyVocabulary, "taxonomy vocabulary $taxonomyVocabularyId exists");
    // @todo Assert all the other things. (Migration, Behat etc...).
  }

}
