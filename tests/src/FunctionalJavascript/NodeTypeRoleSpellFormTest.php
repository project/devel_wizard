<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.node_type
 * @group devel_wizard.spell.node_type.role
 *
 * @covers \Drupal\devel_wizard\Spell\NodeTypeRoleSpell
 * @covers \Drupal\devel_wizard\SpellForm\NodeTypeRoleSpellForm
 */
class NodeTypeRoleSpellFormTest extends SpellFormTestBase {

  protected string $spellId = 'devel_wizard_node_type_role';

  /**
   * @group browser.chromium
   */
  public function testNodeTypeFormSuccess() {
    $nodeTypeId = 'dummy';
    $this->drupalCreateContentType([
      'name' => ucfirst($nodeTypeId),
      'type' => $nodeTypeId,
    ]);
    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);
    $this->navigateToSpellForm();

    $page = $this->getSession()->getPage();
    $page->selectFieldOption('Content type', $nodeTypeId);
    $page
      ->find(...$this->spellFormFinder(FALSE))
      ->submit();
    $this->waitForElement(30, 'css', 'div[data-drupal-messages]');

    // @todo Resolve missing role_delegation module issue.
    $assert = $this->assertSession();
    $assert->pageTextContains("{$this->spellId} - Role has been created: $nodeTypeId administrator");
  }

}
