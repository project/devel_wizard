<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Drupal\Tests\devel_wizard\Traits\TaxonomyVocabularyCreationTrait;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.taxonomy_vocabulary
 * @group devel_wizard.spell.taxonomy_vocabulary.migration
 *
 * @covers \Drupal\devel_wizard\Spell\TaxonomyVocabularyMigrationSpell
 * @covers \Drupal\devel_wizard\SpellForm\TaxonomyVocabularyMigrationSpellForm
 */
class TaxonomyVocabularyMigrationSpellFormTest extends SpellFormTestBase {

  use TaxonomyVocabularyCreationTrait;

  protected string $spellId = 'devel_wizard_taxonomy_vocabulary_migration';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'taxonomy',
    'devel_wizard',
  ];

  /**
   * @group browser.chromium
   */
  public function testTaxonomyVocabularyFormSuccess() {
    $taxonomyVocabularyId = 'dummy';
    $this->createTaxonomyVocabulary([
      'vid' => $taxonomyVocabularyId,
    ]);

    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);
    $this->navigateToSpellForm();

    $page = $this->getSession()->getPage();
    $page->selectFieldOption('Taxonomy vocabulary', $taxonomyVocabularyId);
    $page
      ->find(...$this->spellFormFinder(FALSE))
      ->submit();
    sleep(1);
    $this->waitForDocumentReady(30);

    /* @todo Install composer packages migrate related modules. */
    $assert = $this->assertSession();
    $assert->pageTextContains('devel_wizard_module - file has been created: modules/custom/app_dc/app_dc.info.yml');
    // @todo Assert file exists.
  }

}
