<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

abstract class SpellFormTestBase extends TestBase {

  protected string $spellId;

  protected function drupalCreateUserForSpell(string $spellId = '') {
    $spellId = $spellId ?: $this->spellId;

    return $this->drupalCreateUser([
      'devel_wizard.spell',
      $this->spellPermission($spellId),
    ]);
  }

  protected function navigateToSpellForm(string $spellId = '') {
    $spellId = $spellId ?: $this->spellId;
    $spellIdHyphen = str_replace('_', '-', $spellId);

    $this->drupalGet('admin/config/development/devel-wizard-spell');

    $href = "/admin/config/development/devel-wizard-spell/$spellId";
    $page = $this->getSession()->getPage();
    $link = $page->find('css', "a[href='$href']");
    static::assertNotNull($link, "link to $spellId spell form exists on the overview page");
    $link->click();

    $this->waitForElement(
      5,
      'css',
      "form[data-drupal-selector=\"$spellIdHyphen-spell-form\"]",
    );
  }

  protected function spellPermission(string $spellId = ''): string {
    return preg_replace(
      '/^devel_wizard_/',
      'devel_wizard.spell.',
      $spellId ?: $this->spellId,
    );
  }

  protected function spellFormFinder(bool $assoc = TRUE): array {
    $finder = [
      'selector' => 'css',
      'locator' => sprintf(
        'form[data-drupal-selector="%s-spell-form"]',
        str_replace('_', '-', $this->spellId),
      ),
    ];

    return $assoc ? $finder : array_values($finder);
  }

}
