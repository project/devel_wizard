<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Drupal\block_content\Entity\BlockContentType;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.block_content_type
 * @group devel_wizard.spell.block_content_type.create
 *
 * @covers \Drupal\devel_wizard\Spell\BlockContentTypeCreateSpell
 * @covers \Drupal\devel_wizard\SpellForm\BlockContentTypeCreateSpellForm
 */
class BlockContentTypeCreateSpellFormTest extends SpellFormTestBase {

  protected string $spellId = 'devel_wizard_block_content_type_create';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block_content',
    'devel_wizard',
  ];

  /**
   * @group browser.chromium
   */
  public function testBlockContentFormSuccess() {
    $blockContentTypeId = 'dummy';
    $blockContentType = BlockContentType::load($blockContentTypeId);
    static::assertNull($blockContentType, "block content type $blockContentTypeId not exists");

    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);
    $this->navigateToSpellForm();

    $page = $this->getSession()->getPage();
    sleep(1);
    $page->fillField('Machine-readable name', $blockContentTypeId);
    $page
      ->find(...$this->spellFormFinder(FALSE))
      ->submit();
    sleep(1);
    $this->waitForDocumentReady(30);

    $assert = $this->assertSession();
    $assert->pageTextContains("{$this->spellId} - Custom block type has been created: $blockContentTypeId");
    $assert->pageTextContains("Spell {$this->spellId} was successful!");

    $blockContentType = BlockContentType::load($blockContentTypeId);
    static::assertNotNull($blockContentType, "block content type $blockContentTypeId exists");
  }

}
