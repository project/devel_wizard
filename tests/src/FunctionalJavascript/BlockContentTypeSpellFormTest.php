<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Drupal\block_content\Entity\BlockContentType;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.block_content_type
 * @group devel_wizard.spell.block_content_type.all_in_one
 *
 * @covers \Drupal\devel_wizard\Spell\BlockContentTypeSpell
 * @covers \Drupal\devel_wizard\SpellForm\BlockContentTypeSpellForm
 */
class BlockContentTypeSpellFormTest extends SpellFormTestBase {

  protected string $spellId = 'devel_wizard_block_content_type';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block_content',
    'devel_wizard',
  ];

  /**
   * @group browser.chromium
   */
  public function testSubmitSuccess() {
    $blockContentTypeId = 'dummy';
    $nodeType = BlockContentType::load($blockContentTypeId);
    static::assertNull($nodeType, "block content type $blockContentTypeId not exists");

    $adminUser = $this->drupalCreateUserForSpell();
    $this->drupalLogin($adminUser);
    $this->navigateToSpellForm();

    $formFinder = $this->spellFormFinder();
    $page = $this->getSession()->getPage();
    sleep(1);
    $page->fillField('Machine-readable name', $blockContentTypeId);
    $form = $page->find($formFinder['selector'], $formFinder['locator']);
    $form->submit();
    sleep(1);
    $this->waitForDocumentReady(90);

    $assert = $this->assertSession();
    $assert->pageTextContains("{$this->spellId}_create - Custom block type has been created: $blockContentTypeId");

    // @todo The spell is unsuccessful because of the missing role_delegation module.
    $blockContentType = BlockContentType::load($blockContentTypeId);
    static::assertNotNull($blockContentType, "block content type $blockContentTypeId exists");
    // @todo Assert all the other things. (Migration, Behat etc...).
  }

}
