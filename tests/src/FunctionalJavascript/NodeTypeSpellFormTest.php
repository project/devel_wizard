<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Drupal\node\Entity\NodeType;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.node_type
 * @group devel_wizard.spell.node_type.all_in_one
 *
 * @covers \Drupal\devel_wizard\Spell\NodeTypeSpell
 * @covers \Drupal\devel_wizard\SpellForm\NodeTypeSpellForm
 */
class NodeTypeSpellFormTest extends SpellFormTestBase {

  protected string $spellId = 'devel_wizard_node_type';

  /**
   * @group browser.chromium
   */
  public function testSubmitSuccess() {
    $nodeTypeId = 'dummy';
    $nodeType = NodeType::load($nodeTypeId);
    static::assertNull($nodeType, "node type $nodeTypeId not exists");

    $adminUser = $this->drupalCreateUserForSpell();
    $this->drupalLogin($adminUser);
    $this->navigateToSpellForm();

    sleep(1);
    $page = $this->getSession()->getPage();
    $page->fillField('Machine-readable name of the new Content type', $nodeTypeId);
    $page
      ->find(...$this->spellFormFinder(FALSE))
      ->submit();
    $this->waitForElement(60, 'css', 'div[data-drupal-messages]');

    $assert = $this->assertSession();
    $assert->pageTextContains("{$this->spellId}_create - Content type has been created: $nodeTypeId");

    // @todo The spell is unsuccessful because of the missing role_delegation module.
    $nodeType = NodeType::load($nodeTypeId);
    static::assertNotNull($nodeType, "node type $nodeTypeId exists");
    // @todo Assert all the other things. (Migration, Behat etc...).
  }

}
