<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Stringy\StaticStringy;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.entity_type
 *
 * @covers \Drupal\devel_wizard\Spell\EntityTypeSpell
 * @covers \Drupal\devel_wizard\SpellForm\EntityTypeSpellForm
 */
class EntityTypeSpellFormTest extends SpellFormTestBase {

  protected string $spellId = 'devel_wizard_entity_type';

  /**
   * @group browser.chromium
   */
  public function testEntityTypeFormSuccess() {
    $drupalRoot = static::getDrupalRoot();

    $spell = \Drupal::getContainer()->get('devel_wizard.spell.entity_type');
    $settings = [
      'module' => [
        'machine_name' => 'my_mod',
      ],
      'config' => [
        'id' => 'my_mod_foo_type',
      ],
      'content' => [
        'id' => 'my_mod_foo',
      ],
    ];
    $settings = $spell->applyDefaultValues($settings);

    $moduleName = $settings['module']['machine_name'];

    static::assertDirectoryDoesNotExist("$drupalRoot/modules/custom/$moduleName");

    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);

    $this->entityTypeSpellCast($settings);

    $expectedFiles = $this->entityTypeSpellExpectedFiles($settings);
    $this->assertEntityTypeSpellFiles($expectedFiles);

    $this->drupalLogout();
    $admin = $this->createUser([], NULL, TRUE);
    $this->drupalLogin($admin);
    $this->enableModules([
      'field_ui',
      $settings['module']['machine_name'],
    ]);

    $this->entityTypeSpellCreateConfig(
      $settings,
      [
        'label' => 'My type 01',
        'description' => 'My desc',
        'help' => 'My help',
        'weight' => 1,
      ],
    );
    $this->entityTypeSpellCreateContent(
      $settings,
      [
        'bundle' => 'my_type_01',
        'label' => 'My instance 01',
      ],
    );
  }

  protected function entityTypeSpellCast(array $settings) {
    $this->navigateToSpellForm();
    sleep(3);
    $page = $this->getSession()->getPage();
    $page->fillField('settings[module][machine_name]', $settings['module']['machine_name']);
    $page->fillField('settings[config][id]', $settings['config']['id']);
    $page->fillField('settings[content][id]', $settings['content']['id']);

    $submitButton = $page->findButton('Abracadabra');
    static::assertNotNull($submitButton, 'element found: Abracadabra button');
    $submitButton->click();
    $this->waitForElement(15, 'css', 'div[data-drupal-messages]');

    return $this;
  }

  protected function entityTypeSpellExpectedFiles(array $settings): array {
    $moduleName = $settings['module']['machine_name'];
    $moduleNameDash = StaticStringy::dasherize($moduleName);
    $configEntityTypeId = $settings['config']['id'];
    $contentEntityTypeId = $settings['content']['id'];
    $contentEntityTypeIdDash = StaticStringy::dasherize($contentEntityTypeId);

    $configClass = StaticStringy::upperCamelize($configEntityTypeId);
    $contentClass = StaticStringy::upperCamelize($contentEntityTypeId);

    return [
      "modules/custom/{$moduleName}/config/schema/{$moduleName}.schema.yml" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}AccessControlHandler.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}AddForm.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}Comparer.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}DeleteForm.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}EditForm.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}ListBuilder.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}RouteProvider.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}Storage.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}AccessControlHandler.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}AddForm.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}Controller.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}DeleteForm.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}EditForm.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}ListBuilder.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}RevisionDeleteForm.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}RevisionRevertForm.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}RevisionRevertTranslationForm.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}RouteProvider.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}Storage.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}StorageInterface.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}StorageSchema.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}TranslationHandler.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}ViewBuilder.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}ViewController.php" => [],
      "modules/custom/{$moduleName}/src/Entity/{$contentClass}.php" => [],
      "modules/custom/{$moduleName}/src/Entity/{$configClass}.php" => [],
      "modules/custom/{$moduleName}/src/{$contentClass}Interface.php" => [],
      "modules/custom/{$moduleName}/src/{$contentClass}PermissionProvider.php" => [],
      "modules/custom/{$moduleName}/src/{$contentClass}PermissionProviderInterface.php" => [],
      "modules/custom/{$moduleName}/src/{$configClass}Interface.php" => [],
      "modules/custom/{$moduleName}/templates/{$moduleNameDash}.{$contentEntityTypeIdDash}.html.twig" => [],
      "modules/custom/{$moduleName}/{$moduleName}.info.yml" => [],
      "modules/custom/{$moduleName}/{$moduleName}.links.action.yml" => [],
      "modules/custom/{$moduleName}/{$moduleName}.links.contextual.yml" => [],
      "modules/custom/{$moduleName}/{$moduleName}.links.menu.yml" => [],
      "modules/custom/{$moduleName}/{$moduleName}.links.task.yml" => [],
      "modules/custom/{$moduleName}/{$moduleName}.module" => [],
      "modules/custom/{$moduleName}/{$moduleName}.permissions.yml" => [],
      "modules/custom/{$moduleName}/{$moduleName}.services.yml" => [],
    ];
  }

  protected function assertEntityTypeSpellFiles(array $expected) {
    $drupalRoot = static::getDrupalRoot();

    foreach ($expected as $path => $expectedFile) {
      $expectedFile += [
        '{{ path }}' => $path,
      ];

      if (!empty($expectedFile['{{ path }}'])) {
        static::assertFileExists("$drupalRoot/{$expectedFile['{{ path }}']}");
      }
    }

    return $this;
  }

  protected function entityTypeSpellCreateConfig(array $settings, array $values) {
    $path = "/admin/structure/{$settings['config']['id']}";
    $this->drupalGet($path);
    sleep(2);
    $page = $this->getSession()->getPage();

    $page->clickLink("Add {$settings['config']['label']}");
    sleep(2);
    $page = $this->getSession()->getPage();

    $page->fillField('Label', $values['label']);
    sleep(3);
    $finder = [
      'selector' => 'css',
      'locator' => '#edit-label-machine-name-suffix .admin-link button',
    ];
    // @todo For some reason this doesn't work.
    $page->waitFor(
      5,
      function () use ($page, $finder) {
        return $page->find($finder['selector'], $finder['locator']);
      },
    );
    if (!empty($values['id'])) {
      $machineNameToggle = $page->find($finder['selector'], $finder['locator']);
      $machineNameToggle->click();
      $page->fillField('id', $values['id']);
    }

    $page->fillField('Description', $values['description'] ?? '');
    $page->fillField('Help', $values['help'] ?? '');
    $page->fillField('Weight', $values['weight'] ?? 0);

    $page->pressButton('Save');

    return $this;
  }

  protected function entityTypeSpellCreateContent(array $settings, array $values) {
    $contentIdDash = StaticStringy::dasherize($settings['content']['id']);
    $this->drupalGet("/admin/content/$contentIdDash/add/{$values['bundle']}");
    sleep(1);
    $page = $this->getSession()->getPage();
    $page->fillField('label[0][value]', $values['label']);
    $page->pressButton('Save');
    sleep(1);
    $this->assertSession()->addressMatches("@^/admin/content/$contentIdDash/manage/\d+$@");

    return $this;
  }

}
