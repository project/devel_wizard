<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Drupal\Tests\devel_wizard\Traits\TaxonomyVocabularyCreationTrait;
use Drupal\user\Entity\Role;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.taxonomy_vocabulary
 * @group devel_wizard.spell.taxonomy_vocabulary.role
 *
 * @covers \Drupal\devel_wizard\Spell\TaxonomyVocabularyRoleSpell
 * @covers \Drupal\devel_wizard\SpellForm\TaxonomyVocabularyRoleSpellForm
 */
class TaxonomyVocabularyRoleSpellFormTest extends SpellFormTestBase {

  use TaxonomyVocabularyCreationTrait;

  protected string $spellId = 'devel_wizard_taxonomy_vocabulary_role';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'taxonomy',
    'devel_wizard',
  ];

  /**
   * @group browser.chromium
   */
  public function testTaxonomyVocabularyFormSuccess() {
    $taxonomyVocabularyId = 'dummy';
    $this->createTaxonomyVocabulary([
      'vid' => $taxonomyVocabularyId,
    ]);
    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);
    $this->navigateToSpellForm();

    sleep(1);
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('Taxonomy vocabulary', $taxonomyVocabularyId);
    $page
      ->find(...$this->spellFormFinder(FALSE))
      ->submit();
    sleep(1);
    $this->waitForDocumentReady(60);

    $roleId = "{$taxonomyVocabularyId}_admin";
    $role = Role::load($roleId);
    // @todo Assert permissions.
    $this->assertNotNull($role, "role $roleId exists");

    $roleId = 'role_delegator';
    $role = Role::load($roleId);
    // @todo Assert permissions.
    $this->assertNotNull($role, "role $roleId exists");
  }

}
