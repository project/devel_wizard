<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Stringy\StaticStringy;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.node_type
 * @group devel_wizard.spell.node_type.admin_view
 *
 * @covers \Drupal\devel_wizard\Spell\NodeTypeAdminViewSpell
 * @covers \Drupal\devel_wizard\SpellForm\NodeTypeAdminViewSpellForm
 */
class NodeTypeAdminViewSpellFormTest extends SpellFormTestBase {

  protected string $spellId = 'devel_wizard_node_type_admin_view';

  /**
   * @group browser.chromium
   */
  public function testNodeTypeFormSuccess() {
    $nodeTypeId = 'dummy';
    $nodeTypeIdDash = StaticStringy::dasherize($nodeTypeId);
    $this->drupalCreateContentType([
      'name' => ucfirst($nodeTypeId),
      'type' => $nodeTypeId,
    ]);

    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);
    $this->navigateToSpellForm();

    $page = $this->getSession()->getPage();
    $page->selectFieldOption('Content type', $nodeTypeId);
    $page
      ->find(...$this->spellFormFinder(FALSE))
      ->submit();
    $this->waitForElement(30, 'css', 'div[data-drupal-messages]');

    $admin = $this->createUser([], NULL, TRUE);
    $this->drupalLogin($admin);
    $this->clearAllCaches();
    $this->drupalGet("admin/content/node/list-$nodeTypeId");
    $page = $this->getSession()->getPage();
    $assert = $this->assertSession();
    $titleElement = $page->find('css', 'h1');
    static::assertSame($nodeTypeId, $titleElement->getText(), 'page title');
    $assert->elementExists(
      'css',
      "form[data-drupal-selector=\"views-exposed-form-$nodeTypeIdDash-admin-overview\"]",
    );
  }

}
