<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Drupal\Tests\devel_wizard\Traits\BlockContentCreationTrait;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.block_content_type
 * @group devel_wizard.spell.block_content_type.admin_view
 *
 * @covers \Drupal\devel_wizard\Spell\BlockContentTypeAdminViewSpell
 * @covers \Drupal\devel_wizard\SpellForm\BlockContentTypeAdminViewSpellForm
 */
class BlockContentTypeAdminViewSpellFormTest extends SpellFormTestBase {

  use BlockContentCreationTrait;

  protected string $spellId = 'devel_wizard_block_content_type_admin_view';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'olivero';

  /**
   * @var null|string
   */
  protected $adminTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block_content',
    'devel_wizard',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpAdminTheme();
  }

  protected function setUpAdminTheme() {
    if (!$this->adminTheme) {
      return $this;
    }

    $this
      ->container
      ->get('theme_installer')
      ->install([$this->adminTheme]);

    $this
      ->config('system.theme')
      ->set('admin', $this->adminTheme)
      ->save();

    return $this;
  }

  /**
   * @group browser.chromium
   */
  public function testBlockContentFormSuccess() {
    $blockContentTypeId = 'dummy';
    $this->createBlockContentType([
      'id' => $blockContentTypeId,
      'label' => $blockContentTypeId,
    ]);

    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);
    $this->navigateToSpellForm();

    $formFinder = $this->spellFormFinder();

    // Creates derivatives in the default module.
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('Custom block type', $blockContentTypeId);
    $form = $page->find($formFinder['selector'], $formFinder['locator']);
    $form->submit();
    sleep(1);
    $this->waitForElement(30, 'css', 'div[data-drupal-messages]');

    $admin = $this->createUser([], NULL, TRUE);
    $this->drupalLogin($admin);
    $this->clearAllCaches();
    $this->enableModules(["app_core"], 0);

    $this->drupalGet("admin/content/block-content/$blockContentTypeId");
    $page = $this->getSession()->getPage();
    // @todo check Primary and Secondary tabs.
    static::assertSame(
      $blockContentTypeId,
      $page->find('css', 'h1')->getText(),
      'page title',
    );

    $assert = $this->assertSession();
    $assert->elementExists(
      'css',
      "*[data-drupal-selector=\"views-exposed-form-$blockContentTypeId-admin-overview\"]",
    );
  }

}
