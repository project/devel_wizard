<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Drupal\Tests\devel_wizard\Traits\TaxonomyVocabularyCreationTrait;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.taxonomy_vocabulary
 * @group devel_wizard.spell.taxonomy_vocabulary.behat
 *
 * @covers \Drupal\devel_wizard\Spell\TaxonomyVocabularyBehatSpell
 * @covers \Drupal\devel_wizard\SpellForm\TaxonomyVocabularyBehatSpellForm
 */
class TaxonomyVocabularyBehatSpellFormTest extends SpellFormTestBase {

  use TaxonomyVocabularyCreationTrait;

  protected string $spellId = 'devel_wizard_taxonomy_vocabulary_behat';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'taxonomy',
    'devel_wizard',
  ];

  /**
   * @group browser.chromium
   */
  public function testTaxonomyVocabularyFormSuccess() {
    $taxonomyVocabularyId = 'dummy';
    $this->createTaxonomyVocabulary([
      'vid' => $taxonomyVocabularyId,
      'name' => $taxonomyVocabularyId,
    ]);
    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);
    $this->navigateToSpellForm();

    $page = $this->getSession()->getPage();
    $page->selectFieldOption('Taxonomy vocabulary', $taxonomyVocabularyId);
    $page
      ->find(...$this->spellFormFinder(FALSE))
      ->submit();
    sleep(1);
    $this->waitForDocumentReady(30);

    $featuresCreated = [
      'access' => ['create', 'view', 'edit', 'delete'],
      'form' => ['create', 'edit'],
    ];
    $assert = $this->assertSession();
    $assert->pageTextContains("devel_wizard_taxonomy_vocabulary_behat - Create behat test for $taxonomyVocabularyId");
    foreach ($featuresCreated as $key => $operations) {
      foreach ($operations as $operation) {
        $assert->pageTextContains("devel_wizard_taxonomy_vocabulary_behat - file has been created: ../tests/behat/features/{$taxonomyVocabularyId}/{$taxonomyVocabularyId}.{$key}.{$operation}.feature");
      }
    }
    $assert->pageTextContains('Spell devel_wizard_taxonomy_vocabulary_behat was successful!');
  }

}
