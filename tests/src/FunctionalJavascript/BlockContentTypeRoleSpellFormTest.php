<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Drupal\Tests\devel_wizard\Traits\BlockContentCreationTrait;
use Drupal\user\Entity\Role;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.block_content_type
 * @group devel_wizard.spell.block_content_type.role
 *
 * @covers \Drupal\devel_wizard\Spell\BlockContentTypeRoleSpell
 * @covers \Drupal\devel_wizard\SpellForm\BlockContentTypeRoleSpellForm
 */
class BlockContentTypeRoleSpellFormTest extends SpellFormTestBase {

  use BlockContentCreationTrait;

  protected string $spellId = 'devel_wizard_block_content_type_role';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block_content',
    'devel_wizard',
  ];

  /**
   * @group browser.chromium
   */
  public function testBlockContentFormSuccess() {
    $blockContentTypeId = 'basic';
    $this->createBlockContentType([
      'id' => $blockContentTypeId,
      'label' => $blockContentTypeId,
    ]);

    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);

    $this->navigateToSpellForm();

    $formFinder = $this->spellFormFinder();
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('Custom block type', $blockContentTypeId);
    $form = $page->find($formFinder['selector'], $formFinder['locator']);
    $form->submit();
    sleep(1);
    $this->waitForDocumentReady(30);

    // @todo Resolve missing role_delegation module issue.
    $role = Role::load("{$blockContentTypeId}_admin");
    static::assertNotNull($role, "User role with ID '{$blockContentTypeId}_admin' is exists");
  }

}
