<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Behat\Mink\Exception\ElementNotFoundException;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\devel_wizard\Helper\SetUpTearDownTestTrait;
use weitzman\DrupalTestTraits\DrupalTrait;

class TestBase extends WebDriverTestBase {

  use SetUpTearDownTestTrait;
  use DrupalTrait;

  /**
   * {@inheritdoc}
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'devel_wizard',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->setUpGitCheckout();
    parent::setUp();
  }

  protected function tearDown(): void {
    $this->tearDownCleanupEntities();
    parent::tearDown();
    $this->setUpGitCheckout();
  }

  protected function tearDownCleanupEntities() {
    foreach ($this->cleanupEntities as $entity) {
      $entity->delete();
    }

    return $this;
  }

  protected function clearAllCaches() {
    $this->drupalGet('/admin/config/development/performance');
    $page = $this->getSession()->getPage();
    $page->pressButton('Clear all caches');
    sleep(2);
    $assert = $this->assertSession();
    $assert->pageTextContains('Caches cleared.');
  }

  protected function waitForElement(int $timeoutSeconds, string $selector, string $locator, ?string $sessionName = NULL) {
    $page = $this->getSession($sessionName)->getPage();

    $result = $page->waitFor(
      $timeoutSeconds,
      function () use ($selector, $locator, $sessionName) {
        return $this
          ->getSession($sessionName)
          ->getPage()
          ->find($selector, $locator);
      },
    );

    if (!$result) {
      throw new ElementNotFoundException(
        $this->getSession($sessionName),
        NULL,
        $selector,
        $locator,
      );
    }

    return $this;
  }

  protected function waitForDocumentReady(int $timeoutSeconds, ?string $sessionName = NULL) {
    $session = $this->getSession($sessionName);
    while ($timeoutSeconds > 0) {
      $state = $session->evaluateScript('return document.readyState');
      if ($state === 'complete') {
        return $this;
      }

      $timeoutSeconds -= 1;
    }

    throw new \Exception("document could not reach the 'complete' state in $timeoutSeconds seconds");
  }

  protected function enableModules(array $modules, int $timeoutSeconds = 30) {
    $this->drupalGet('admin/modules');
    $this->waitForElement(30, 'css', 'form[data-drupal-selector="system-modules"]');
    $page = $this->getSession()->getPage();

    foreach ($modules as $module) {
      $page->checkField("modules[{$module}][enable]");
    }

    $page->pressButton('Install');
    if ($timeoutSeconds > 0) {
      $this->waitForElement($timeoutSeconds, 'css', 'div[data-drupal-messages]');
    }

    return $this;
  }

}
