<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Drupal\node\Entity\NodeType;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.node_type.create
 *
 * @covers \Drupal\devel_wizard\Spell\NodeTypeCreateSpell
 * @covers \Drupal\devel_wizard\SpellForm\NodeTypeCreateSpellForm
 */
class NodeTypeCreateSpellFormTest extends SpellFormTestBase {

  protected string $spellId = 'devel_wizard_node_type_create';

  /**
   * @group browser.chromium
   */
  public function testNodeTypeFormSuccess() {
    $nodeTypeId = 'dummy';
    $nodeType = NodeType::load($nodeTypeId);
    static::assertNull($nodeType, "node type $nodeTypeId not exists");

    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);
    $this->navigateToSpellForm();

    $page = $this->getSession()->getPage();
    $page->fillField('Machine-readable name', $nodeTypeId);
    $page
      ->find(...$this->spellFormFinder(FALSE))
      ->submit();
    $this->waitForElement(30, 'css', 'div[data-drupal-messages]');

    $assert = $this->assertSession();
    $assert->pageTextContains("{$this->spellId} - Content type has been created: $nodeTypeId");
    $assert->pageTextContains("Spell {$this->spellId} was successful!");

    $nodeType = NodeType::load($nodeTypeId);
    static::assertNotNull($nodeType, "node type $nodeTypeId exists");
  }

}
