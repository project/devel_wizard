<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Drupal\Tests\devel_wizard\Traits\BlockContentCreationTrait;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.block_content_type
 * @group devel_wizard.spell.block_content_type.behat
 *
 * @covers \Drupal\devel_wizard\Spell\BlockContentTypeBehatSpell
 * @covers \Drupal\devel_wizard\SpellForm\BlockContentTypeBehatSpellForm
 */
class BlockContentTypeBehatSpellFormTest extends SpellFormTestBase {

  use BlockContentCreationTrait;

  protected string $spellId = 'devel_wizard_block_content_type_behat';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block_content',
    'devel_wizard',
  ];

  /**
   * @group browser.chromium
   */
  public function testBlockContentFormSuccess() {
    $blockContentTypeId = 'dummy';
    $this->createBlockContentType([
      'id' => $blockContentTypeId,
      'label' => $blockContentTypeId,
    ]);

    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);

    $this->navigateToSpellForm();

    $formFinder = $this->spellFormFinder();
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('Custom block type', $blockContentTypeId);
    $form = $page->find($formFinder['selector'], $formFinder['locator']);
    $form->submit();
    sleep(1);
    $this->waitForDocumentReady(30);

    $featuresCreated = [
      'access' => ['create', 'view', 'edit', 'delete'],
      'form' => ['create', 'edit'],
    ];
    $assert = $this->assertSession();
    $assert->pageTextContains("devel_wizard_block_content_type_behat - Create behat test for $blockContentTypeId");
    foreach ($featuresCreated as $key => $operations) {
      foreach ($operations as $operation) {
        $assert->pageTextContains("devel_wizard_block_content_type_behat - file has been created: ../tests/behat/features/{$blockContentTypeId}/{$blockContentTypeId}.block_content.{$key}.{$operation}.feature");
      }
    }
    $assert->pageTextContains('Spell devel_wizard_block_content_type_behat was successful!');
    // @todo Assert that files are exists.
  }

}
