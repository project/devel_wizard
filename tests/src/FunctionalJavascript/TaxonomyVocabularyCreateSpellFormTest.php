<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Drupal\taxonomy\Entity\Vocabulary;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.taxonomy_vocabulary
 * @group devel_wizard.spell.taxonomy_vocabulary.create
 *
 * @covers \Drupal\devel_wizard\Spell\TaxonomyVocabularyCreateSpell
 * @covers \Drupal\devel_wizard\SpellForm\TaxonomyVocabularyCreateSpellForm
 */
class TaxonomyVocabularyCreateSpellFormTest extends SpellFormTestBase {

  protected string $spellId = 'devel_wizard_taxonomy_vocabulary_create';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'taxonomy',
    'devel_wizard',
  ];

  /**
   * @group browser.chromium
   */
  public function testTaxonomyVocabularyFormSuccess() {
    $taxonomyVocabularyId = 'dummy';
    $taxonomyVocabulary = Vocabulary::load($taxonomyVocabularyId);
    static::assertNull($taxonomyVocabulary, "taxonomy vocabulary $taxonomyVocabularyId not exists");

    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);
    $this->navigateToSpellForm();

    $page = $this->getSession()->getPage();
    sleep(1);
    $page->fillField('Machine-readable name', $taxonomyVocabularyId);
    $page
      ->find(...$this->spellFormFinder(FALSE))
      ->submit();
    sleep(1);
    $this->waitForDocumentReady(30);

    $assert = $this->assertSession();
    $assert->pageTextContains("{$this->spellId} - Taxonomy vocabulary has been created: $taxonomyVocabularyId");
    $assert->pageTextContains("Spell {$this->spellId} was successful!");

    $taxonomyVocabulary = Vocabulary::load($taxonomyVocabularyId);
    static::assertNotNull($taxonomyVocabulary, "taxonomy vocabulary $taxonomyVocabularyId exists");
  }

}
