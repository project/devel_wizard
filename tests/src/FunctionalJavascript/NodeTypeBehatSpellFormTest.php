<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.node_type
 * @group devel_wizard.spell.node_type.behat
 *
 * @covers \Drupal\devel_wizard\Spell\NodeTypeBehatSpell
 * @covers \Drupal\devel_wizard\SpellForm\NodeTypeBehatSpellForm
 */
class NodeTypeBehatSpellFormTest extends SpellFormTestBase {

  protected string $spellId = 'devel_wizard_node_type_behat';

  /**
   * @group browser.chromium
   */
  public function testNodeTypeFormSuccess() {
    $nodeTypeId = 'dummy';
    $this->drupalCreateContentType([
      'name' => ucfirst($nodeTypeId),
      'type' => $nodeTypeId,
    ]);
    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);
    $this->navigateToSpellForm();

    $page = $this->getSession()->getPage();
    $page->selectFieldOption('Content type', $nodeTypeId);
    $page
      ->find(...$this->spellFormFinder(FALSE))
      ->submit();
    $this->waitForElement(30, 'css', 'div[data-drupal-messages]');

    $featuresCreated = [
      'access' => ['create', 'view', 'edit', 'delete'],
      'form' => ['create', 'edit'],
    ];
    $assert = $this->assertSession();
    $assert->pageTextContains("{$this->spellId} - Create behat test for $nodeTypeId");
    foreach ($featuresCreated as $key => $operations) {
      foreach ($operations as $operation) {
        $assert->pageTextContains("{$this->spellId} - file has been created: ../tests/behat/features/{$nodeTypeId}/{$nodeTypeId}.node.{$key}.{$operation}.feature");
      }
    }
    $assert->pageTextContains("Spell {$this->spellId} was successful!");
  }

}
