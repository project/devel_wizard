<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

use Drupal\Tests\devel_wizard\Traits\BlockContentCreationTrait;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.block_content_type
 * @group devel_wizard.spell.block_content_type.migration
 *
 * @covers \Drupal\devel_wizard\Spell\BlockContentTypeMigrationSpell
 * @covers \Drupal\devel_wizard\SpellForm\BlockContentTypeMigrationSpellForm
 */
class BlockContentTypeMigrationSpellFormTest extends SpellFormTestBase {

  use BlockContentCreationTrait;

  protected string $spellId = 'devel_wizard_block_content_type_migration';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block_content',
    'devel_wizard',
  ];

  /**
   * @group browser.chromium
   */
  public function testBlockContentFormSuccess() {
    $blockContentTypeId = 'dummy';
    $this->createBlockContentType(['id' => $blockContentTypeId]);

    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);
    $this->navigateToSpellForm();

    $formFinder = $this->spellFormFinder();
    $page = $this->getSession()->getPage();
    sleep(1);
    $page->selectFieldOption('Custom block type', $blockContentTypeId);
    $page
      ->find($formFinder['selector'], $formFinder['locator'])
      ->submit();
    sleep(1);
    $this->waitForDocumentReady(90);

    /* @todo Install composer packages migrate related modules. */
    $assert = $this->assertSession();
    $assert->pageTextContains('devel_wizard_module - file has been created: modules/custom/app_dc/app_dc.info.yml');
    // @todo Assert file exists.
  }

}
