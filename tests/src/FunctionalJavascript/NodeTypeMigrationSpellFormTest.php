<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\FunctionalJavascript;

/**
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.node_type
 * @group devel_wizard.spell.node_type.migration
 *
 * @covers \Drupal\devel_wizard\Spell\NodeTypeMigrationSpell
 * @covers \Drupal\devel_wizard\SpellForm\NodeTypeMigrationSpellForm
 */
class NodeTypeMigrationSpellFormTest extends SpellFormTestBase {

  protected string $spellId = 'devel_wizard_node_type_migration';

  /**
   * @group browser.chromium
   */
  public function testNodeTypeFormSuccess() {
    $nodeTypeId = 'dummy';
    $this->drupalCreateContentType([
      'name' => ucfirst($nodeTypeId),
      'type' => $nodeTypeId,
    ]);

    $wizard = $this->drupalCreateUserForSpell();
    $this->drupalLogin($wizard);
    $this->navigateToSpellForm();

    $page = $this->getSession()->getPage();
    $page->selectFieldOption('Content type', $nodeTypeId);
    $page
      ->find(...$this->spellFormFinder(FALSE))
      ->submit();
    $this->waitForElement(30, 'css', 'div[data-drupal-messages]');

    /* @todo Install composer packages migrate related modules. */
    $assert = $this->assertSession();
    $assert->pageTextContains('devel_wizard_module - file has been created: modules/custom/app_dc/app_dc.info.yml');
    // @todo Assert file exists.
  }

}
