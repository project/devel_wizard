<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Helper;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

trait SetUpTearDownTestTrait {

  protected string $projectName = 'project_01';

  protected string $gitExecutable = 'git';

  protected string $shell = '/bin/bash';

  /**
   * @return string
   */
  abstract protected static function getDrupalRoot();

  protected function getProjectRootDir(): string {
    return dirname(static::getDrupalRoot());
  }

  public function getSelfRootDir(): string {
    return dirname(__DIR__, 3);
  }

  protected function setUpGitCheckout() {
    $projectRoot = $this->getProjectRootDir();

    $command = [$this->gitExecutable, 'checkout', 'main'];
    $process = new Process($command, $projectRoot);
    $process->run();

    $command = [$this->gitExecutable, 'restore', '--', '.'];
    $process = new Process($command, $projectRoot);
    $process->run();

    $command = [
      $this->gitExecutable,
      'clean',
      '--force',
      '--',
      'tests/',
      'sites/',
      'docroot/modules/',
      'docroot/themes/',
      'docroot/profiles/',
    ];
    $process = new Process($command, $projectRoot);
    $process->run();

    // @todo Delete everything automatically, except "devel_wizard".
    (new Filesystem())->remove([
      "$projectRoot/docroot/modules/contrib/migrate_dc",
      "$projectRoot/docroot/modules/contrib/migrate_plus",
      "$projectRoot/docroot/modules/contrib/migrate_tools",
      "$projectRoot/docroot/modules/contrib/role_delegation",
    ]);

    return $this;
  }

}
