<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Helper;

use Composer\Script\Event;
use Psr\Log\LoggerInterface;
use SebastianBergmann\CodeCoverage\CodeCoverage;
use SebastianBergmann\CodeCoverage\Report\Html\Facade as HtmlCodeCoverageReporter;
use Sweetchuck\Utils\Filesystem as FilesystemUtils;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Process\Process;
use Sweetchuck\Utils\Filter\ArrayFilterFileSystemExists;
use Webmozart\PathUtil\Path;

class ComposerScripts {

  /**
   * Composer event callback.
   */
  public static function postInstallCmd(Event $event): int {
    $self = new static($event);

    $self
      ->preparePhpunitXml()
      ->prepareProject();

    return 0;
  }

  /**
   * Composer event callback.
   */
  public static function postUpdateCmd(Event $event): int {
    $self = new static($event);

    $self
      ->preparePhpunitXml()
      ->prepareProject();

    return 0;
  }

  /**
   * Composer event callback.
   */
  public static function cmdPrepareProject(Event $event) {
    $self = new static($event);
    $self->prepareProject();

    return 0;
  }

  /**
   * Composer event callback.
   */
  public static function cmdPreparePhpunitXml(Event $event) {
    $self = new static($event);
    $self->preparePhpunitXml();

    return 0;
  }

  public static function checkJunit(Event $event) {
    $io = $event->getIO();
    $dir = 'reports/machine/junit';

    $fileNames = $event->getArguments();
    if (!$fileNames) {
      $io->warning('at least one file name should be provided');

      return;
    }

    $numOfProblems = 0;
    $report = [];
    foreach ($fileNames as $fileName) {
      $filePath = "$dir/$fileName.xml";
      $doc = new \DOMDocument();
      $doc->loadXML(file_get_contents($filePath));

      $report[$filePath]['error'] = $doc->getElementsByTagName('error')->count();
      $report[$filePath]['failure'] = $doc->getElementsByTagName('failure')->count();

      $numOfProblems += array_sum($report[$filePath]);
    }

    if (!$numOfProblems) {
      return;
    }

    $io->writeError(var_export($report, TRUE));

    throw new \Exception(sprintf(
      'number of problems: %d',
      $numOfProblems,
    ));
  }

  public static function generateCoverageHtml(Event $event): int {
    $self = new static($event);

    $srcFiles = (new Finder())
      ->in('reports/machine/coverage-php')
      ->files()
      ->name('*.php');

    $dstDir = 'reports/human/coverage/all/html';

    (new HtmlCodeCoverageReporter())->process(
      $self->mergeCodeCoverageFiles($srcFiles),
      $dstDir
    );

    return 0;
  }

  /**
   * Current event.
   */
  protected Event $event;

  /**
   * CLI process callback.
   */
  protected \Closure $processCallbackWrapper;

  protected string $projectRoot = 'tests/fixtures/project_01';

  protected Filesystem $fs;

  protected LoggerInterface $logger;

  /**
   * Current working directory.
   */
  protected string $cwd = '.';

  protected string $gitExecutable = 'git';

  protected string $binDir = './vendor/bin';

  protected string $shell = '/bin/bash';

  protected int $jsonEncodeFlags = \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE;

  protected function __construct(Event $event, ?LoggerInterface $logger = NULL, ?Filesystem $fs = NULL, string $cwd = '.') {
    $this->cwd = $cwd ?: '.';
    $this->event = $event;
    $this->logger = $logger ?: $this->createLogger();
    $this->fs = $fs ?: $this->createFilesystem();
    $this->initProcessCallbackWrapper();
    $shell = getenv('SHELL');
    if ($shell) {
      $this->shell = $shell;
    }
  }

  protected function createLogger(): LoggerInterface {
    $io = $this->event->getIO();
    $verbosity = OutputInterface::VERBOSITY_NORMAL;
    if ($io->isDebug()) {
      $verbosity = OutputInterface::VERBOSITY_DEBUG;
    }
    elseif ($io->isVeryVerbose()) {
      $verbosity = OutputInterface::VERBOSITY_VERY_VERBOSE;
    }
    elseif ($io->isVerbose()) {
      $verbosity = OutputInterface::VERBOSITY_VERBOSE;
    }

    $output = new ConsoleOutput($verbosity, $io->isDecorated());

    return new ConsoleLogger($output);
  }

  protected function createFilesystem(): Filesystem {
    return new Filesystem();
  }

  /**
   * @return $this
   */
  protected function initProcessCallbackWrapper() {
    if (!isset($this->processCallbackWrapper)) {
      $this->processCallbackWrapper = function (string $type, string $buffer) {
        $this->processCallback($type, $buffer);
      };
    }

    return $this;
  }

  /**
   * @return $this
   */
  protected function preparePhpunitXml() {
    $config = $this->event->getComposer()->getConfig();

    $phpunitExecutable = Path::join($config->get('bin-dir'), 'phpunit');
    if (!$this->fs->exists($phpunitExecutable)) {
      $this->logger->info('PHPUnit configuration file creation is skipped because phpunit/phpunit is not installed');

      return $this;
    }

    $dstFileName = Path::join($this->cwd, 'phpunit.xml');
    $srcFileName = Path::join($this->cwd, 'phpunit.xml.dist');
    $logArgs = [
      'srcFileName' => $srcFileName,
      'dstFileName' => $dstFileName,
    ];

    if ($this->fs->exists($dstFileName)) {
      $this->logger->info(
        'PHPUnit configuration file is already exists: {dstFileName}',
        $logArgs,
      );

      return $this;
    }

    $srcFileName = Path::join($this->cwd, 'phpunit.xml.dist');
    if (!$this->fs->exists($srcFileName)) {
      $this->logger->info(
        'PHPUnit configuration source file does not exists: {srcFileName}',
        $logArgs,
      );

      return $this;
    }

    $this->logger->info(
      'PHPUnit configuration {srcFileName} => {dstFileName}',
      $logArgs,
    );
    $basePattern = '<env name="%s" value="%s"/>';
    $oldPattern = "<!-- $basePattern -->";
    $replacementPairs = [];
    foreach ($this->getPhpunitEnvVars() as $envVarName => $envVarValue) {
      $placeholder = sprintf($oldPattern, $envVarName, '');
      $replacementPairs[$placeholder] = sprintf($basePattern, $envVarName, $this->escapeXmlAttribute($envVarValue));
    }

    $content = FilesystemUtils::fileGetContents($srcFileName);
    $this->fs->dumpFile($dstFileName, strtr($content, $replacementPairs));

    return $this;
  }

  /**
   * @return $this
   */
  protected function prepareProject() {
    if (!$this->event->isDevMode()) {
      return $this;
    }

    $this
      ->prepareProjectSelf()
      ->prepareProjectComposerJson()
      ->prepareProjectPhpunitXml()
      ->prepareProjectDirs()
      ->prepareProjectSettingsPhp()
      ->prepareProjectSite()
      ->prepareProjectGit();

    return $this;
  }

  /**
   * @return $this
   */
  protected function prepareProjectSelf() {
    $dstDir = $this->getProjectSelfDestination();

    $this->logger->info('link files into: {dir}', ['dir' => $dstDir]);

    $relative = implode(
      '/',
      array_fill(
        0,
        substr_count($dstDir, '/') + 1,
        '..'
      )
    );

    $filesToSymlink = $this->getProjectSelfFilesToSymlink();
    $this->fs->mkdir($dstDir);
    foreach ($filesToSymlink as $fileToSymlink) {
      $this->fs->symlink("$relative/$fileToSymlink", "$dstDir/$fileToSymlink");
    }

    return $this;
  }

  /**
   * @return $this
   */
  protected function prepareProjectComposerJson() {
    $fileName = Path::join($this->cwd, $this->projectRoot, 'composer.json');
    $fileContent = [
      'name' => 'drupal/marvin-tests-project_01',
      'description' => 'drupal/marvin-tests-project_01',
      "license" => "proprietary",
      'type' => 'drupal-project',
      'minimum-stability' => 'dev',
      'prefer-stable' => TRUE,
      'config' => [
        'optimize-autoloader' => TRUE,
        'preferred-install' => [
          '*' => 'dist',
        ],
        'process-timeout' => 0,
        'sort-packages' => TRUE,
      ],
      'repositories' => [
        'drupal/devel_wizard' => [
          'type' => 'path',
          'url' => '../packages/drupal/devel_wizard',
        ],
        'drupal' => [
          'type' => 'composer',
          'url' => 'https://packages.drupal.org/8',
        ],
        'assets' => [
          'type' => 'composer',
          'url' => 'https://asset-packagist.org',
        ],
      ],
      'require' => [
        'cweagans/composer-patches' => '^1.6',
        'behat/mink-goutte-driver' => '^1.2',
        'behat/mink-selenium2-driver' => '^1.4',
        'drupal/core-composer-scaffold' => '^9.0',
        'drupal/core-recommended' => '^9',
        'drupal/devel_wizard' => '*',
        'drush/drush' => '^10.0',
        'mikey179/vfsstream' => '^1.6',
        'oomphinc/composer-installers-extender' => '^2.0',
        'phpspec/prophecy-phpunit' => '^2.0',
        'phpunit/phpunit' => '^9.0',
        'symfony/phpunit-bridge' => '^5.2',
      ],
      'extra' => [
        'installer-types' => [
          'bower-asset',
          'npm-asset',
        ],
        'installer-paths' => [
          'docroot/core' => [
            'type:drupal-core',
          ],
          'docroot/libraries/{$name}' => [
            'type:drupal-library',
            'type:bower-asset',
            'type:npm-asset',
          ],
          'docroot/modules/contrib/{$name}' => [
            'type:drupal-module',
          ],
          'docroot/profiles/contrib/{$name}' => [
            'type:drupal-profile',
          ],
          'docroot/themes/contrib/{$name}' => [
            'type:drupal-theme',
          ],
          'drush/Commands/contrib/{$name}' => [
            'type:drupal-drush',
          ],
        ],
        'enable-patching' => TRUE,
        'composer-exit-on-patch-failure' => TRUE,
        'patches' => [
          'drupal/core' => [
            'https://www.drupal.org/project/drupal/issues/3049087 - SQLite database maxlength' => 'https://www.drupal.org/files/issues/2019-05-07/3049087-15.patch',
          ],
        ],
        'drupal-scaffold' => [
          'locations' => [
            'web-root' => 'docroot',
          ],
          'file-mapping' => [
            '[web-root]/modules/.gitignore' => [
              'mode' => 'skip',
            ],
            '[web-root]/modules/README.txt' => [
              'mode' => 'skip',
            ],
            '[web-root]/profiles/.gitignore' => [
              'mode' => 'skip',
            ],
            '[web-root]/profiles/README.txt' => [
              'mode' => 'skip',
            ],
            '[web-root]/themes/.gitignore' => [
              'mode' => 'skip',
            ],
            '[web-root]/themes/README.txt' => [
              'mode' => 'skip',
            ],
            '[web-root]/sites/example.settings.local.php' => [
              'mode' => 'skip',
            ],
            '[web-root]/sites/.gitignore' => [
              'mode' => 'skip',
            ],
            '[web-root]/sites/README.txt' => [
              'mode' => 'skip',
            ],
            '[web-root]/.csslintrc' => [
              'mode' => 'skip',
            ],
            '[web-root]/.editorconfig' => [
              'mode' => 'skip',
            ],
            '[web-root]/.eslintignore' => [
              'mode' => 'skip',
            ],
            '[web-root]/.eslintrc.json' => [
              'mode' => 'skip',
            ],
            '[web-root]/.gitattributes' => [
              'mode' => 'skip',
            ],
            '[web-root]/.gitignore' => [
              'mode' => 'skip',
            ],
            '[web-root]/example.gitignore' => [
              'mode' => 'skip',
            ],
            '[web-root]/INSTALL.txt' => [
              'mode' => 'skip',
            ],
            '[web-root]/README.txt' => [
              'mode' => 'skip',
            ],
            '[web-root]/.htaccess' => [
              'mode' => 'skip',
            ],
            '[web-root]/web.config' => [
              'mode' => 'skip',
            ],
            '[project-root]/.editorconfig' => [
              'mode' => 'skip',
            ],
          ],
          'initial' => [
            'sites/default/default.services.yml' => 'sites/default/services.yml',
            'sites/default/default.settings.php' => 'sites/default/settings.php',
          ],
        ],
      ],
    ];

    $this->logger->info(
      'create file: {fileName}',
      [
        'fileName' => $fileName,
      ],
    );

    $this->fs->mkdir(Path::getDirectory($fileName));
    $this->fs->dumpFile($fileName, json_encode($fileContent, $this->jsonEncodeFlags));

    $this->processRun(
      $this->projectRoot,
      [
        'composer',
        '--ansi',
        'update',
      ],
    );

    return $this;
  }

  protected function prepareProjectPhpunitXml() {
    $fileName = Path::join($this->cwd, $this->projectRoot, 'phpunit.xml.dist');
    $fileContent = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<phpunit
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="vendor/phpunit/phpunit/phpunit.xsd"
  bootstrap="docroot/core/tests/bootstrap.php"
  colors="true"
  verbose="true"
  beStrictAboutOutputDuringTests="true"
  beStrictAboutChangesToGlobalState="true">

  <php>
    <ini name="memory_limit" value="-1"/>
    <ini name="error_reporting" value="32767"/>
    <env name="SIMPLETEST_BASE_URL" value="http://127.0.0.1:8888"/>
    <env name="SIMPLETEST_DB" value="sqlite://../sites/default/database/default.default.sqlite"/>
    <env name="SYMFONY_DEPRECATIONS_HELPER" value="max[self]=0"/>
    <env name="MINK_DRIVER_CLASS" value="Drupal\FunctionalJavascriptTests\DrupalSelenium2Driver"/>
    <env name="MINK_DRIVER_ARGS" value="[&quot;firefox&quot;, {}, &quot;http://127.0.0.1:4444/wd/hub&quot;]"/>
    <!--<env name="MINK_DRIVER_ARGS" value='["opera", {}, "http://127.0.0.1:4444/wd/hub"]'/>-->
    <!--<env name="MINK_DRIVER_ARGS" value='["chrome", { "chromeOptions": { "w3c": false } }, "http://127.0.0.1:4444/wd/hub"]'/>-->
  </php>

  <testsuites>
    <testsuite name="unit">
      <directory>./docroot/profiles/custom/*/tests/src/Unit/</directory>
      <directory>./docroot/modules/custom/*/tests/src/Unit/</directory>
      <directory>./docroot/themes/custom/*/tests/src/Unit/</directory>
      <directory>./tests/src/Unit/</directory>
    </testsuite>
    <testsuite name="kernel">
      <directory>./docroot/profiles/custom/*/tests/src/Kernel/</directory>
      <directory>./docroot/modules/custom/*/tests/src/Kernel/</directory>
      <directory>./docroot/themes/custom/*/tests/src/Kernel/</directory>
      <directory>./tests/src/Kernel/</directory>
    </testsuite>
    <testsuite name="functional">
      <directory>./docroot/profiles/custom/*/tests/src/Functional/</directory>
      <directory>./docroot/modules/custom/*/tests/src/Functional/</directory>
      <directory>./docroot/themes/custom/*/tests/src/Functional/</directory>
      <directory>./tests/src/Functional/</directory>
    </testsuite>
  </testsuites>

  <listeners>
    <listener class="\Drupal\Tests\Listeners\DrupalListener"/>
    <listener class="\Symfony\Bridge\PhpUnit\SymfonyTestsListener"/>
  </listeners>

  <coverage processUncoveredFiles="true">
    <include>
      <directory>./docroot/profiles/custom/</directory>
      <directory>./docroot/modules/custom/</directory>
      <directory>./docroot/themes/custom/</directory>
      <directory>./drush/Commands/custom/</directory>
    </include>

    <exclude>
      <directory>./docroot/profiles/custom/*/tests/</directory>
      <directory>./docroot/profiles/custom/*/node_modules/</directory>
      <directory>./docroot/modules/custom/*/tests/</directory>
      <directory>./docroot/modules/custom/*/node_modules/</directory>
      <directory>./docroot/themes/custom/*/tests/</directory>
      <directory>./docroot/themes/custom/*/node_modules/</directory>
    </exclude>

    <report>
      <clover outputFile="reports/machine/coverage/phpunit.xml"/>
      <html outputDirectory="reports/human/coverage/html"/>
      <text outputFile="php://stdout"/>
    </report>
  </coverage>

  <logging>
    <testdoxHtml outputFile="reports/human/junit/phpunit.html"/>
    <junit outputFile="reports/machine/junit/phpunit.xml"/>
  </logging>
</phpunit>

XML;

    $this->logger->info(
      'create file: {fileName}',
      [
        'fileName' => $fileName,
      ],
    );

    $this->fs->mkdir(Path::getDirectory($fileName));
    $this->fs->dumpFile($fileName, $fileContent);

    return $this;
  }

  protected function prepareProjectDirs() {
    $drushSutRoot = $this->projectRoot;

    $dirs = [
      "$drushSutRoot/docroot/libraries",
      "$drushSutRoot/docroot/profiles",
      "$drushSutRoot/docroot/themes",
      "$drushSutRoot/sites/default/config/prod",
      "$drushSutRoot/sites/default/database/",
    ];

    $this->logger->info(
      'create directories: {dirs}',
      [
        'dirs' => implode(', ', $dirs),
      ],
    );

    $this->fs->mkdir($dirs, 0777 - umask());

    return $this;
  }

  protected function prepareProjectSettingsPhp() {
    $src = Path::join($this->projectRoot, 'docroot', 'sites', 'default', 'default.settings.php');
    $dst = Path::join($this->projectRoot, 'docroot', 'sites', 'default', 'settings.php');

    $args = [
      'srcFileName' => $src,
      'dstFileName' => $dst,
    ];

    if (!$this->fs->exists($src)) {
      $this->logger->info("source file does not exists: {srcFileName}", $args);

      return $this;
    }

    if ($this->fs->exists($dst)) {
      $this->logger->info("destination file already exists: {dstFileName}", $args);

      return $this;
    }

    $this->logger->info("copy {srcFileName} => {dstFileName}", $args);

    $replacementPairs = [];
    $replacementPairs['$databases = [];'] = <<<'PHP'
$databases = [
  'default' => [
    'default' => [
      'driver' => 'sqlite',
      'namespace' => '\Drupal\Core\Database\Driver\sqlite',
      'database' => "../$site_path/database/default.default.sqlite",
      'prefix' => '',
    ],
  ],
];
PHP;

    $key = "# \$settings['config_sync_directory'] = '/directory/outside/webroot';";
    $replacementPairs[$key] = "\$settings['config_sync_directory'] = \"../\$site_path/config/prod\";";

    $key = <<< 'PHP'
# $settings['file_chmod_directory'] = 0775;
# $settings['file_chmod_file'] = 0664;
PHP;
    $replacementPairs[$key] = <<< 'PHP'
$settings['file_chmod_directory'] = 0777 - umask();
$settings['file_chmod_file'] = 0666 - umask();
PHP;

    $this->fs->dumpFile($dst, strtr(FilesystemUtils::fileGetContents($src), $replacementPairs));

    return $this;
  }

  protected function prepareProjectSite() {
    $dbFile = $this->getSqliteFileName();

    if ($this->fs->exists($dbFile)) {
      $this->logger->info(
        'file already exists: {fileName}',
        [
          'fileName' => $dbFile,
        ],
      );
    }

    $this->prepareProjectSiteInstall();
    $this->prepareProjectSiteConfiguration();
    $this->prepareProjectSiteConfigExport();
    $this->prepareProjectSiteDatabaseDump();

    return $this;
  }

  protected function prepareProjectSiteInstall() {
    $command = [
      "{$this->binDir}/drush",
      '--ansi',
      '--root',
      $this->projectRoot,
      '--yes',
      'site:install',
      'standard',
    ];

    $process = $this->processRun($this->cwd, $command);
    if ($process->getExitCode()) {
      $this->logger->error($process->getErrorOutput());

      return $this;
    }

    return $this;
  }

  protected function prepareProjectSiteConfiguration() {
    $drush = [
      "{$this->binDir}/drush",
      '--ansi',
      "--root={$this->projectRoot}",
      '--yes',
    ];

    $commands = [
      'enable new wave themes' => array_merge(
        $drush,
        [
          'theme:enable',
          'claro,olivero',
        ],
      ),
      'set frontend theme' => array_merge(
        $drush,
        [
          'config:set',
          'system.theme',
          'default',
          'olivero',
        ],
      ),
      'set admin theme' => array_merge(
        $drush,
        [
          'config:set',
          'system.theme',
          'admin',
          'claro',
        ],
      ),
      'cache:rebuild' => array_merge(
        $drush,
        [
          'cache:rebuild',
        ],
      ),
      'enable the main module' => array_merge(
        $drush,
        [
          'pm:enable',
          $this->getComposerPackageName(),
        ],
      ),
      'pm:uninstall big_pipe' => array_merge(
        $drush,
        [
          'pm:uninstall',
          'big_pipe',
        ],
      ),
      'uninstall themes' => array_merge(
        $drush,
        [
          'theme:uninstall',
          'bartik,seven',
        ],
      ),
      'delete all node types' => array_merge(
        $drush,
        [
          'entity:delete',
          'node_type',
        ],
      ),
      'error_level' => array_merge(
        $drush,
        [
          'config:set',
          'system.logging',
          'error_level',
          'verbose',
        ],
      ),
    ];

    foreach ($commands as $message => $command) {
      $this->logger->info($message);
      $process = $this->processRun($this->cwd, $command);
      if ($process->getExitCode()) {
        $this->logger->error($process->getErrorOutput());
      }
    }

    return $this;
  }

  protected function prepareProjectSiteConfigExport() {
    $command = [
      "{$this->binDir}/drush",
      '--ansi',
      '--root',
      $this->projectRoot,
      '--yes',
      'config:export',
    ];

    $process = $this->processRun($this->cwd, $command);
    if ($process->getExitCode()) {
      $this->event->getIO()->writeError($process->getOutput() . \PHP_EOL . $process->getErrorOutput());
    }

    return $this;
  }

  protected function prepareProjectSiteDatabaseDump() {
    $dbFile = $this->getSqliteFileName();

    $this->fs->copy(
      $dbFile,
      Path::join(Path::getDirectory($dbFile), 'original.default.sqlite'),
    );

    return $this;
  }

  protected function prepareProjectGit() {
    $fileName = "{$this->projectRoot}/.git";
    if ($this->fs->exists($fileName)) {
      $this->logger->info(
        'file already exists: {fileName}',
        [
          'fileName' => $fileName,
        ],
      );

      return $this;
    }

    $this
      ->prepareProjectGitIgnore()
      ->prepareProjectGitInit();

    return $this;
  }

  protected function prepareProjectGitIgnore() {
    $fileName = Path::join(
      $this->projectRoot,
      '.gitignore',
    );
    $content = implode("\n", [
      '/sites/*/database/',
      '',
    ]);

    $this->logger->info('create file: {fileName}', ['fileName' => $fileName]);
    $this->fs->dumpFile($fileName, $content);

    return $this;
  }

  protected function prepareProjectGitInit() {
    $command = [
      $this->gitExecutable,
      'init',
    ];
    $process = $this->processRun($this->projectRoot, $command);
    if ($process->getExitCode()) {
      $this->logger->error($process->getErrorOutput());

      return $this;
    }

    // @todo Old Git version does not support "git init --initial-branch".
    $command = [
      $this->gitExecutable,
      'checkout',
      '-b',
      'main',
    ];
    $process = $this->processRun($this->projectRoot, $command);
    if ($process->getExitCode()) {
      $this->logger->error($process->getErrorOutput());

      return $this;
    }

    $command = [
      $this->shell,
      '-c',
      sprintf(
        '%s add . 1>/dev/null',
        escapeshellcmd($this->gitExecutable),
      ),
    ];
    $process = $this->processRun($this->projectRoot, $command);
    if ($process->getExitCode()) {
      $this->logger->error($process->getErrorOutput());

      return $this;
    }

    $command = [
      $this->shell,
      '-c',
      sprintf(
        '%s commit --message=%s 1>/dev/null',
        escapeshellcmd($this->gitExecutable),
        escapeshellarg('Initial commit'),
      ),
    ];
    $process = $this->processRun($this->projectRoot, $command);
    if ($process->getExitCode()) {
      $this->logger->error($process->getOutput() . \PHP_EOL . $process->getErrorOutput());
    }

    return $this;
  }

  protected function getProjectSelfDestination(): string {
    $name = $this->getComposerPackageName();

    return "tests/fixtures/packages/drupal/$name";
  }

  protected function getComposerPackageName(): string {
    $parts = explode('/', $this->event->getComposer()->getPackage()->getName(), 2);
    if (empty($parts[1])) {
      throw new \Exception('Invalid package name', 1);
    }

    return $parts[1];
  }

  /**
   * @return string[]
   */
  protected function getProjectSelfFilesToSymlink(): array {
    $extra = $this->event->getComposer()->getPackage()->getExtra();
    $filesToSymLink = $extra['marvin']['drushUnish']['filesToSymlink'] ?? [];
    $filesToSymLink += $this->getProjectSelfFilesToSymlinkDefaults();

    $filesToSymLink = array_keys($filesToSymLink, TRUE, TRUE);

    $filter = new ArrayFilterFileSystemExists();
    $filter->setBaseDir($this->cwd);

    return array_filter($filesToSymLink, $filter);
  }

  /**
   * @return bool[]
   */
  protected function getProjectSelfFilesToSymlinkDefaults(): array {
    $name = $this->getComposerPackageName();

    $defaults = [
      'config' => TRUE,
      'Commands' => TRUE,
      'Generators' => TRUE,
      'src' => TRUE,
      'templates' => TRUE,
      'composer.json' => TRUE,
      'drush.services.yml' => TRUE,
      'drush9.services.yml' => TRUE,
      'drush10.services.yml' => TRUE,
    ];

    $files = (new Finder())
      ->in('.')
      ->depth(0)
      ->files()
      ->name("$name.*.yml")
      ->name("$name.module")
      ->name("$name.install")
      ->name("$name.profile")
      ->name("$name.theme");
    foreach ($files as $file) {
      $defaults[$file->getRelativePathname()] = TRUE;
    }

    return $defaults;
  }

  protected function processRun(string $workingDirectory, array $command): Process {
    $this->logger->info(
      'run {cmd} in {cwd}',
      [
        'cmd' => implode(' ', $command),
        'cwd' => $workingDirectory,
      ],
    );

    $process = new Process($command, NULL, NULL, NULL, 0);
    $process->setWorkingDirectory($workingDirectory);
    $process->run($this->processCallbackWrapper);

    return $process;
  }

  protected function processCallback(string $type, string $buffer): void {
    $type === Process::OUT ?
      $this->event->getIO()->write($buffer, FALSE)
      : $this->event->getIO()->writeError($buffer, FALSE);
  }

  protected function escapeXmlAttribute(string $value): string {
    return htmlentities($value, ENT_QUOTES);
  }

  protected function getPhpunitEnvVars(): array {
    return [
      'BROWSERTEST_OUTPUT_DIRECTORY' => realpath($this->cwd) . "/{$this->projectRoot}/docroot/sites/simpletest/browser_output",
    ];
  }

  protected function mergeCodeCoverageFiles(iterable $files): CodeCoverage {
    $codeCoverage = new CodeCoverage();

    $coverage = NULL;
    $require = function (string $fileName) {
      return require $fileName;
    };

    /** @var \Symfony\Component\Finder\SplFileInfo $file */
    foreach ($files as $file) {
      $coverage = $require($file->getRealPath());
      if ($coverage instanceof CodeCoverage) {
        $codeCoverage->merge($coverage);
      }
    }

    return $codeCoverage;
  }

  protected function getSqliteFileName(): string {
    return Path::join(
      $this->projectRoot,
      'sites',
      'default',
      'database',
      'default.default.sqlite',
    );
  }

}
