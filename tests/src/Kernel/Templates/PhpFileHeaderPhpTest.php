<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Kernel\Templates;

use Drupal\Tests\devel_wizard\Kernel\Templates\TestBase as TemplatesTestBase;

class PhpFileHeaderPhpTest extends TemplatesTestBase {

  protected static string $templateName = 'php/devel_wizard.php.file.header.php';

}
