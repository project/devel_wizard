<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Kernel\Templates;

use Drupal\Tests\devel_wizard\Kernel\TestBase as MainTestBase;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;
use Webmozart\PathUtil\Path;

abstract class TestBase extends MainTestBase {

  protected static string $templateNamespace = 'devel_wizard';

  protected static string $templateName = '';

  protected function collectCases(string $templateName): array {
    $cases = [];

    $fileName = "{$this->fixturesDir}/templates/$templateName.yml";
    if (file_exists($fileName)) {
      $cases += Yaml::parseFile($fileName);
    }

    $casesDir = "{$this->fixturesDir}/templates/$templateName";
    if (is_dir($casesDir)) {
      $dirs = (new Finder())
        ->in($casesDir)
        ->directories()
        ->depth(0);
      foreach ($dirs as $dir) {
        $extension = Path::getExtension($templateName);
        $caseDir = $dir->getPathname();
        $cases[$dir->getFilename()] = [
          'expected' => file_get_contents("$caseDir/expected.$extension"),
          'context' => Yaml::parseFile("$caseDir/context.yml"),
        ];
      }
    }

    return $cases;
  }

  public function casesTemplate(): array {
    return $this->collectCases(static::$templateName);
  }

  /**
   * @dataProvider casesTemplate
   */
  public function testTemplate(string $expected, array $context) {
    $twig = \Drupal::getContainer()->get('twig');
    $actual = $twig->render(
      '@' . static::$templateNamespace . '/' . static::$templateName . '.twig',
      $context,
    );

    $this->assertSame($expected, $actual);
  }

}
