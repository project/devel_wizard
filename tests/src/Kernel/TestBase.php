<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Kernel;

use Drupal\KernelTests\KernelTestBase;

class TestBase extends KernelTestBase {

  public static $theme = 'classy';

  public static $modules = ['system', 'user', 'devel_wizard'];

  protected ?string $selfDir;

  protected ?string $fixturesDir;

  public function __construct(?string $name = NULL, array $data = [], $dataName = '') {
    $this->selfDir = dirname(__DIR__, 3);
    $this->fixturesDir = "{$this->selfDir}/tests/fixtures";

    parent::__construct($name, $data, $dataName);
  }

}
