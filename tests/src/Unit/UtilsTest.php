<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Unit;

use Drupal\devel_wizard\Utils;
use PHPUnit\Framework\TestCase;

/**
 * @group devel_wizard
 *
 * @covers \Drupal\devel_wizard\Utils
 */
class UtilsTest extends TestCase {

  public function casesFormatByExpiryDate(): array {
    return [
      'green' => [
        'a',
        'a',
        new \DateTime('+8 months'),
      ],
      'yellow - far future' => [
        '<fg=yellow>a</>',
        'a',
        new \DateTime('+4 months'),
      ],
      'red - past' => [
        '<fg=red>a</>',
        'a',
        new \DateTime('yesterday'),
      ],
      'red - near future' => [
        '<fg=red>a</>',
        'a',
        new \DateTime('tomorrow'),
      ],
    ];
  }

  /**
   * @dataProvider casesFormatByExpiryDate
   */
  public function testFormatByExpiryDate(string $expected, string $text, \DateTimeInterface $expiry_date) {
    static::assertSame($expected, Utils::formatByExpiryDate($text, $expiry_date));
  }

}
