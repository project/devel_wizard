<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.block_content_type
 * @group devel_wizard.spell.block_content_type.admin_view
 *
 * @covers \Drupal\devel_wizard\Commands\BlockContentTypeAdminViewSpellCommands
 */
class BlockContentTypeAdminViewSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:block-content-type:admin-view';

  public function testRunSuccess(): void {
    $blockContentTypeId = 'basic';
    $this->createBlockContentType(['type' => $blockContentTypeId]);

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $blockContentTypeId,
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      0,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    $expectedMessages = [
      " Message: devel_wizard_block_content_type_admin_view - View has been created: \n{$blockContentTypeId}_admin ",
    ];
    foreach ($expectedMessages as $expectedMessage) {
      static::assertStringContainsString($expectedMessage, $actualStdError);
    }

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

  public function testRunFailInvalidMachineName(): void {
    $this->createBlockContentType(['type' => 'basic']);

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      '0a b',
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      1,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      '[error]  Custom block type machine-name <em class="placeholder">0a b</em> is invalid, it has to be one of: basic',
      $actualStdError,
      'StdError',
    );

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

}
