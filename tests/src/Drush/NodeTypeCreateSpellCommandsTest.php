<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.node_type
 * @group devel_wizard.spell.node_type.create
 *
 * @covers \Drupal\devel_wizard\Commands\NodeTypeCreateSpellCommands
 */
class NodeTypeCreateSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:node-type:create';

  public function testRun(): void {
    $expected = [];
    $expected += [
      'stdError' => '',
      'stdOutput' => '',
      'exitCode' => 0,
    ];

    $envVars = [];

    $args = [
      'foo',
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      $expected['exitCode'],
      NULL,
      $envVars + $this->getCommonCommandLineEnvVars()
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      'Message: devel_wizard_node_type_create - Content type has been created: foo',
      $actualStdError,
      'StdError',
    );
    static::assertSame($expected['stdOutput'], $actualStdOutput, 'StdOutput');
  }

}
