<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.node_type
 * @group devel_wizard.spell.node_type.role
 *
 * @covers \Drupal\devel_wizard\Commands\NodeTypeRoleSpellCommands
 */
class NodeTypeRoleSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:node-type:role';

  public function testRunSuccess(): void {
    $nodeTypeId = 'article';
    $this->createNodeType(['type' => $nodeTypeId]);

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $nodeTypeId,
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      'devel-wizard:spell:node-type:role',
      $args,
      $options,
      NULL,
      NULL,
      0,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      'Message: devel_wizard_node_type_role - Role has been created: article administrator',
      $actualStdError,
      'StdError',
    );
    static::assertStringContainsString(
      "'require' 'drupal/role_delegation:^1.1'",
      $actualStdError,
      'StdError',
    );
    // @todo Message: devel_wizard_node_type_role - module /role_delegation/ has been installed'.
    static::assertStringContainsString(
      'Message: devel_wizard_node_type_role - Role updated: Role delegator',
      $actualStdError,
      'StdError',
    );
    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

  public function testRunFailInvalidMachineName(): void {
    $nodeTypeId = '0a b';

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $nodeTypeId,
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      1,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      "[error]  Content type machine-name <em class=\"placeholder\">0a b</em> is invalid, it has to be one of:  \n",
      $actualStdError,
      'StdError',
    );

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

}
