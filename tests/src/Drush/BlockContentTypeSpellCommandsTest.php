<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.block_content_type
 * @group devel_wizard.spell.block_content_type.all_in_one
 *
 * @covers \Drupal\devel_wizard\Commands\BlockContentTypeSpellCommands
 */
class BlockContentTypeSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:block-content-type';

  public function testRunSuccess(): void {
    $blockContentTypeId = 'wizard';

    $drupalRoot = static::getDrupalRoot();

    $expectedFiles = [
      [
        '{{ path }}' => "../tests/behat/features/$blockContentTypeId/$blockContentTypeId.block_content.access.create.feature",
        '{{ message }}' => " Message: devel_wizard_block_content_type_behat - file has been created: \n{{ path }} ",
      ],
      [
        '{{ path }}' => "../tests/behat/features/$blockContentTypeId/$blockContentTypeId.block_content.form.create.feature",
        '{{ message }}' => " Message: devel_wizard_block_content_type_behat - file has been created: \n{{ path }} ",
      ],
    ];
    foreach ($expectedFiles as $expectedFile) {
      static::assertFileDoesNotExist("$drupalRoot/{$expectedFile['{{ path }}']}");
    }

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $blockContentTypeId,
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      0,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    foreach ($expectedFiles as $expectedFile) {
      if (isset($expectedFile['{{ message }}'])) {
        $message = strtr($expectedFile['{{ message }}'], $expectedFile);
        static::assertStringContainsString($message, $actualStdError);
      }

      static::assertFileExists("$drupalRoot/{$expectedFile['{{ path }}']}");
    }

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

  public function testRunFailInvalidMachineName(): void {
    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      '0a b',
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      1,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      'Machine name must only contain lowercase letters, numbers, and underscores',
      $actualStdError,
      'StdError',
    );

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

}
