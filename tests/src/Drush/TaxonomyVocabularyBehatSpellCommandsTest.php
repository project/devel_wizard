<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.taxonomy_vocabulary
 * @group devel_wizard.spell.taxonomy_vocabulary.behat
 *
 * @covers \Drupal\devel_wizard\Commands\TaxonomyVocabularyBehatSpellCommands
 */
class TaxonomyVocabularyBehatSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:taxonomy-vocabulary:behat';

  public function testRunSuccess(): void {
    $vocabulary = $this->createVocabulary();
    $vid = $vocabulary->id();

    $drupalRoot = static::getDrupalRoot();

    $expectedFiles = [
      [
        'path' => "../tests/behat/features/{$vid}/{$vid}.access.create.feature",
      ],
      [
        'path' => "../tests/behat/features/{$vid}/{$vid}.access.delete.feature",
      ],
      [
        'path' => "../tests/behat/features/{$vid}/{$vid}.access.edit.feature",
      ],
      [
        'path' => "../tests/behat/features/{$vid}/{$vid}.access.view.feature",
      ],
      [
        'path' => "../tests/behat/features/{$vid}/{$vid}.form.create.feature",
      ],
      [
        'path' => "../tests/behat/features/{$vid}/{$vid}.form.edit.feature",
      ],
    ];
    foreach ($expectedFiles as $expectedFile) {
      static::assertFileDoesNotExist("$drupalRoot/{$expectedFile['path']}");
    }

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $vocabulary->id(),
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      0,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    foreach ($expectedFiles as $expectedFile) {
      static::assertStringContainsString(
        " Message: devel_wizard_taxonomy_vocabulary_behat - file has been created: \n{$expectedFile['path']} ",
        $actualStdError,
      );
      static::assertFileExists("$drupalRoot/{$expectedFile['path']}");
    }

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

  public function testRunFailInvalidMachineName(): void {
    $this->createVocabulary();

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      '0a b',
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      1,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    // @todo Check spell for invalid machine name validation.
    static::assertStringContainsString(
      "[error]  Unable to load the taxonomy_vocabulary: 0a b",
      $actualStdError,
      'StdError',
    );

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

}
