<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.taxonomy_vocabulary
 * @group devel_wizard.spell.taxonomy_vocabulary.migration
 *
 * @covers \Drupal\devel_wizard\Commands\TaxonomyVocabularyMigrationSpellCommands
 */
class TaxonomyVocabularyMigrationSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:taxonomy-vocabulary:migration';

  public function testRunSuccess(): void {
    $entityTypeId = 'taxonomy_term';
    $bundle = 'tags';
    $this->createVocabulary();

    $drupalRoot = static::getDrupalRoot();

    $moduleName = 'app_dc';
    $migrationGroup = 'app_default';
    $migrationId = "{$migrationGroup}__{$entityTypeId}__{$bundle}";

    $expectedFiles = [
      [
        '{{ path }}' => "modules/custom/{$moduleName}/{$moduleName}.info.yml",
        '{{ message }}' => " Message: devel_wizard_module - file has been created: \n{{ path }} ",
      ],
      [
        '{{ path }}' => "modules/custom/{$moduleName}/config/install/migrate_plus.migration_group.{$migrationGroup}.yml",
        '{{ message }}' => " Message: devel_wizard_taxonomy_vocabulary_migration - file has been created: \n{{ path }} ",
      ],
      [
        '{{ path }}' => "modules/custom/{$moduleName}/config/install/migrate_plus.migration.{$migrationId}.yml",
        '{{ message }}' => " Message: devel_wizard_taxonomy_vocabulary_migration - file has been created: \n{{ path }} ",
      ],
      [
        '{{ path }}' => "../sites/default/content/$migrationGroup/$entityTypeId/$bundle.yml",
        '{{ message }}' => " Message: devel_wizard_taxonomy_vocabulary_migration - file has been created: \n{{ path }} ",
      ],
    ];

    foreach ($expectedFiles as $expectedFile) {
      static::assertFileDoesNotExist("$drupalRoot/{$expectedFile['{{ path }}']}");
    }
    static::assertDirectoryDoesNotExist("$drupalRoot/modules/contrib/migrate_dc");

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $bundle,
    ];

    $options = [
      'migration-module' => $moduleName,
      'migration-group' => $migrationGroup,
    ];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      0,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    foreach ($expectedFiles as $expectedFile) {
      static::assertStringContainsString(
        strtr($expectedFile['{{ message }}'], $expectedFile),
        $actualStdError,
      );
      static::assertFileExists("$drupalRoot/{$expectedFile['{{ path }}']}");
    }

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

  public function testRunFailInvalidMachineName(): void {
    $this->createVocabulary();

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      '0a b',
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      1,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    // @todo Check spell for machine name issue where invalid machine names are
    // allowed for some reason.
    static::assertStringContainsString(
      "[error]  Unable to load the taxonomy_vocabulary: 0a b",
      $actualStdError,
      'StdError',
    );

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

}
