<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.taxonomy_vocabulary
 * @group devel_wizard.spell.taxonomy_vocabulary.role
 *
 * @covers \Drupal\devel_wizard\Commands\TaxonomyVocabularyRoleSpellCommands
 */
class TaxonomyVocabularyRoleSpellCommandsTest extends DrushTestCase {

  use TaxonomyTestTrait;

  protected static string $command = 'devel-wizard:spell:taxonomy-vocabulary:role';

  public function testRunSuccess(): void {
    $vocabulary = $this->createVocabulary();

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $vocabulary->id(),
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      0,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      "Message: devel_wizard_taxonomy_vocabulary_role - Role has been created: {$vocabulary->id()} \nadministrator",
      $actualStdError,
      'StdError',
    );
    static::assertStringContainsString(
      "'require' 'drupal/role_delegation:^1.1'",
      $actualStdError,
      'StdError',
    );
    // @todo Message: devel_wizard_taxonomy_term_role - module /role_delegation/ has been installed'.
    static::assertStringContainsString(
      'Message: devel_wizard_taxonomy_vocabulary_role - Role updated: Role delegator',
      $actualStdError,
      'StdError',
    );
    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

  public function testRunFailInvalidMachineName(): void {
    $taxonomyTermId = '0a b';

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $taxonomyTermId,
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      1,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      "[error]  Unable to load the taxonomy_vocabulary: 0a b",
      $actualStdError,
      'StdError',
    );

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

}
