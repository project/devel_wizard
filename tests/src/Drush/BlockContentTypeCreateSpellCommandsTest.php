<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.block_content_type
 * @group devel_wizard.spell.block_content_type.create
 *
 * @covers \Drupal\devel_wizard\Commands\BlockContentTypeCreateSpellCommands
 */
class BlockContentTypeCreateSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:block-content-type:create';

  public function testRun(): void {
    $expected = [];
    $expected += [
      'stdError' => '',
      'stdOutput' => '',
      'exitCode' => 0,
    ];

    $envVars = [];

    $args = [
      'foo',
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      $expected['exitCode'],
      NULL,
      $envVars + $this->getCommonCommandLineEnvVars()
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      "Message: devel_wizard_block_content_type_create - Custom block type has been created: \nfoo",
      $actualStdError,
      'StdError',
    );
    static::assertSame($expected['stdOutput'], $actualStdOutput, 'StdOutput');
  }

}
