<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.node_type
 * @group devel_wizard.spell.node_type.admin_view
 *
 * @covers \Drupal\devel_wizard\Commands\NodeTypeAdminViewSpellCommands
 */
class NodeTypeAdminViewSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:node-type:admin-view';

  public function testRunSuccess(): void {
    $nodeTypeId = 'article';
    $this->createNodeType(['type' => $nodeTypeId]);

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $nodeTypeId,
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      0,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    $expectedMessages = [
      " Message: devel_wizard_node_type_admin_view - View has been created: {$nodeTypeId}_admin ",
    ];
    foreach ($expectedMessages as $expectedMessage) {
      static::assertStringContainsString($expectedMessage, $actualStdError);
    }

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

  public function testRunFailInvalidMachineName(): void {
    $this->createNodeType(['type' => 'article']);

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      '0a b',
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      1,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      '[error]  Content type machine-name <em class="placeholder">0a b</em> is invalid, it has to be one of: article',
      $actualStdError,
      'StdError',
    );

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

}
