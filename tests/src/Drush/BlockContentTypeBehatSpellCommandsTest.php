<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.block_content_type
 * @group devel_wizard.spell.block_content_type.behat
 *
 * @covers \Drupal\devel_wizard\Commands\BlockContentTypeBehatSpellCommands
 */
class BlockContentTypeBehatSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:block-content-type:behat';

  public function testRunSuccess(): void {
    $blockContentTypeId = 'basic';
    $this->createBlockContentType(['type' => $blockContentTypeId]);

    $drupalRoot = static::getDrupalRoot();

    $expectedFiles = [
      [
        'path' => "../tests/behat/features/$blockContentTypeId/$blockContentTypeId.block_content.access.create.feature",
      ],
      [
        'path' => "../tests/behat/features/$blockContentTypeId/$blockContentTypeId.block_content.form.create.feature",
      ],
    ];
    foreach ($expectedFiles as $expectedFile) {
      static::assertFileDoesNotExist("$drupalRoot/{$expectedFile['path']}");
    }

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $blockContentTypeId,
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      'devel-wizard:spell:block-content-type:behat',
      $args,
      $options,
      NULL,
      NULL,
      0,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    foreach ($expectedFiles as $expectedFile) {
      static::assertStringContainsString(
        " Message: devel_wizard_block_content_type_behat - file has been created: \n{$expectedFile['path']} ",
        $actualStdError,
      );
      static::assertFileExists("$drupalRoot/{$expectedFile['path']}");
    }

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

  public function testRunFailInvalidMachineName(): void {
    $this->createBlockContentType(['type' => 'basic']);

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      '0a b',
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      1,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      '[error]  Custom block type machine-name <em class="placeholder">0a b</em> is invalid, it has to be one of: basic',
      $actualStdError,
      'StdError',
    );

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

}
