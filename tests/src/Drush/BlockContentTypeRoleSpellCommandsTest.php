<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.block_content_type
 * @group devel_wizard.spell.block_content_type.role
 *
 * @covers \Drupal\devel_wizard\Commands\BlockContentTypeRoleSpellCommands
 */
class BlockContentTypeRoleSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:block-content-type:role';

  public function testRunSuccess(): void {
    $blockContentTypeId = 'basic';
    $this->createBlockContentType(['type' => $blockContentTypeId]);

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $blockContentTypeId,
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      'devel-wizard:spell:block-content-type:role',
      $args,
      $options,
      NULL,
      NULL,
      0,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      "Message: devel_wizard_block_content_type_role - Role has been created: basic \nadministrator",
      $actualStdError,
      'StdError',
    );
    static::assertStringContainsString(
      "'require' 'drupal/role_delegation:^1.1'",
      $actualStdError,
      'StdError',
    );
    // @todo Message: devel_wizard_node_type_role - module /role_delegation/ has been installed'.
    static::assertStringContainsString(
      'Message: devel_wizard_block_content_type_role - Role updated: Role delegator',
      $actualStdError,
      'StdError',
    );
    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

  public function testRunFailInvalidMachineName(): void {
    $blockContentTypeId = '0a b';

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $blockContentTypeId,
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      1,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      '[error]  Custom block type machine-name <em class="placeholder">0a b</em> is invalid, it has to be one of: basic',
      $actualStdError,
      'StdError',
    );

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

}
