<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.module
 *
 * @covers \Drupal\devel_wizard\Commands\ModuleSpellCommands
 */
class ModuleSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:module';

  public function testRunSuccess(): void {
    $drupalRoot = static::getDrupalRoot();

    $moduleName = 'foo';

    $expectedFiles = [
      [
        '{{ path }}' => "modules/custom/{$moduleName}/{$moduleName}.info.yml",
        '{{ message }}' => " Message: devel_wizard_module - file has been created: {{ path }} ",
      ],
    ];

    foreach ($expectedFiles as $expectedFile) {
      static::assertFileDoesNotExist("$drupalRoot/{$expectedFile['{{ path }}']}");
    }

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $moduleName,
    ];

    $options = [
      'parent-dir' => 'modules/custom',
    ];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      'devel-wizard:spell:module',
      $args,
      $options,
      NULL,
      NULL,
      0,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    foreach ($expectedFiles as $expectedFile) {
      static::assertStringContainsString(
        strtr($expectedFile['{{ message }}'], $expectedFile),
        $actualStdError,
      );
      static::assertFileExists("$drupalRoot/{$expectedFile['{{ path }}']}");
    }

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

  public function testRunFailInvalidMachineName(): void {
    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      '0a b',
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      1,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      '[error]  Machine name must only contain lowercase letters, numbers, and underscores.',
      $actualStdError,
      'StdError',
    );

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

}
