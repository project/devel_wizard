<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.node_type
 * @group devel_wizard.spell.node_type.all_in_one
 *
 * @covers \Drupal\devel_wizard\Commands\NodeTypeSpellCommands
 */
class NodeTypeSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:node-type';

  public function testRunSuccess(): void {
    $nodeTypeId = 'article';

    $drupalRoot = static::getDrupalRoot();

    $expectedFiles = [
      [
        '{{ path }}' => "../tests/behat/features/$nodeTypeId/$nodeTypeId.node.access.create.feature",
        '{{ message }}' => " Message: devel_wizard_node_type_behat - file has been created: \n{{ path }} ",
      ],
      [
        '{{ path }}' => "../tests/behat/features/$nodeTypeId/$nodeTypeId.node.access.delete.feature",
        '{{ message }}' => " Message: devel_wizard_node_type_behat - file has been created: \n{{ path }} ",
      ],
      [
        '{{ path }}' => "../tests/behat/features/$nodeTypeId/$nodeTypeId.node.access.edit.feature",
        '{{ message }}' => " Message: devel_wizard_node_type_behat - file has been created: \n{{ path }} ",
      ],
      [
        '{{ path }}' => "../tests/behat/features/$nodeTypeId/$nodeTypeId.node.access.view.feature",
        '{{ message }}' => " Message: devel_wizard_node_type_behat - file has been created: \n{{ path }} ",
      ],
      [
        '{{ path }}' => "../tests/behat/features/$nodeTypeId/$nodeTypeId.node.form.create.feature",
        '{{ message }}' => " Message: devel_wizard_node_type_behat - file has been created: \n{{ path }} ",
      ],
      [
        '{{ path }}' => "../tests/behat/features/$nodeTypeId/$nodeTypeId.node.form.edit.feature",
        '{{ message }}' => " Message: devel_wizard_node_type_behat - file has been created: \n{{ path }} ",
      ],
    ];
    foreach ($expectedFiles as $expectedFile) {
      static::assertFileDoesNotExist("$drupalRoot/{$expectedFile['{{ path }}']}");
    }

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $nodeTypeId,
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      0,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    foreach ($expectedFiles as $expectedFile) {
      if (isset($expectedFile['{{ message }}'])) {
        $message = strtr($expectedFile['{{ message }}'], $expectedFile);
        static::assertStringContainsString($message, $actualStdError);
      }

      static::assertFileExists("$drupalRoot/{$expectedFile['{{ path }}']}");
    }

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

  public function testRunFailInvalidMachineName(): void {
    $nodeTypeId = '0a b';

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $nodeTypeId,
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      1,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      'Machine name must only contain lowercase letters, numbers, and underscores',
      $actualStdError,
      'StdError',
    );

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

}
