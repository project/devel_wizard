<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.node_type
 * @group devel_wizard.spell.node_type.behat
 *
 * @covers \Drupal\devel_wizard\Commands\NodeTypeBehatSpellCommands
 */
class NodeTypeBehatSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:node-type:behat';

  public function testRunSuccess(): void {
    $nodeTypeId = 'article';
    $this->createNodeType(['type' => $nodeTypeId]);

    $drupalRoot = static::getDrupalRoot();

    $expectedFiles = [
      [
        'path' => "../tests/behat/features/$nodeTypeId/$nodeTypeId.node.access.create.feature",
      ],
      [
        'path' => "../tests/behat/features/$nodeTypeId/$nodeTypeId.node.access.delete.feature",
      ],
      [
        'path' => "../tests/behat/features/$nodeTypeId/$nodeTypeId.node.access.edit.feature",
      ],
      [
        'path' => "../tests/behat/features/$nodeTypeId/$nodeTypeId.node.access.view.feature",
      ],
      [
        'path' => "../tests/behat/features/$nodeTypeId/$nodeTypeId.node.form.create.feature",
      ],
      [
        'path' => "../tests/behat/features/$nodeTypeId/$nodeTypeId.node.form.edit.feature",
      ],
    ];
    foreach ($expectedFiles as $expectedFile) {
      static::assertFileDoesNotExist("$drupalRoot/{$expectedFile['path']}");
    }

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      $nodeTypeId,
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      0,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    foreach ($expectedFiles as $expectedFile) {
      static::assertStringContainsString(
        " Message: devel_wizard_node_type_behat - file has been created: \n{$expectedFile['path']} ",
        $actualStdError,
      );
      static::assertFileExists("$drupalRoot/{$expectedFile['path']}");
    }

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

  public function testRunFailInvalidMachineName(): void {
    $this->createNodeType(['type' => 'article']);

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $args = [
      '0a b',
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      'devel-wizard:spell:node-type:behat',
      $args,
      $options,
      NULL,
      NULL,
      1,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      '[error]  Content type machine-name <em class="placeholder">0a b</em> is invalid, it has to be one of: article',
      $actualStdError,
      'StdError',
    );

    static::assertSame('', $actualStdOutput, 'StdOutput');
  }

}
