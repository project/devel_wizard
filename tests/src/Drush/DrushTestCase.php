<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

use Drupal\block_content\BlockContentTypeInterface;
use Drupal\node\NodeTypeInterface;
use Drupal\Tests\devel_wizard\Helper\SetUpTearDownTestTrait;
use Drush\TestTraits\DrushTestTrait;
use Webmozart\PathUtil\Path;
use weitzman\DrupalTestTraits\ExistingSiteBase;

abstract class DrushTestCase extends ExistingSiteBase {

  use DrushTestTrait;
  use SetUpTearDownTestTrait;

  protected static string $command;

  protected function setUp(): void {
    $this->setUpConfigImport();
    $this->setUpGitCheckout();

    parent::setUp();
  }

  protected function setUpConfigImport() {
    $this->drush(
      'config:import',
      [],
      ['yes' => NULL] + $this->getCommonCommandLineOptions(),
    );

    return $this;
  }

  protected static function getDrupalRoot() {
    return Path::join(
      dirname(__DIR__, 3),
      'tests',
      'fixtures',
      'project_01',
      'docroot',
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function convertKeyValueToFlag($key, $value): string {
    if ($value === NULL) {
      return "--$key";
    }

    $options = [];

    if (!is_iterable($value)) {
      $value = [$value];
    }

    foreach ($value as $item) {
      $options[] = sprintf('--%s=%s', $key, static::escapeshellarg($item));
    }

    return implode(' ', $options);
  }

  protected function getCommonCommandLineOptions(): array {
    return [
      'uri' => getenv('SIMPLETEST_BASE_URL') ?: 'http://localhost:8888',
      'config' => [
        Path::join(static::getDrupalRoot(), '..', 'drush'),
      ],
    ];
  }

  protected function getCommonCommandLineEnvVars(): array {
    return [
      'HOME' => '/dev/null',
    ];
  }

  protected function createNodeType(array $values): NodeTypeInterface {
    $type = $values['type'];
    $values = array_replace_recursive(
      [
        'name' => ucfirst($type),
      ],
      $values,
    );

    /** @var \Drupal\node\NodeTypeInterface $nodeType */
    $nodeType = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->create($values);
    $nodeType->save();

    return $nodeType;
  }

  protected function createBlockContentType(array $values): BlockContentTypeInterface {
    // @todo DRY with the above createNodeType().
    $type = $values['type'];
    $values = array_replace_recursive(
      [
        'id' => ucfirst($type),
      ],
      $values,
    );

    /** @var \Drupal\block_content\BlockContentTypeInterface $blockContentType */
    $blockContentType = \Drupal::entityTypeManager()
      ->getStorage('block_content_type')
      ->create($values);
    $blockContentType->save();

    return $blockContentType;
  }

}
