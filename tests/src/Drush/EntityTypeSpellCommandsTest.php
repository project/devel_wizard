<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

use Stringy\StaticStringy;
use Symfony\Component\Finder\Finder;
use Webmozart\PathUtil\Path;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.entity_type
 *
 * @covers \Drupal\devel_wizard\Commands\EntityTypeSpellCommands
 */
class EntityTypeSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:entity-type';

  public function testRunSuccess(): void {
    $drupalRoot = static::getDrupalRoot();

    $moduleName = 'my_mod';
    $moduleNameDash = StaticStringy::dasherize($moduleName);
    $configEntityTypeId = 'my_mod_foo_type';
    $contentEntityTypeId = 'my_mod_foo';
    $contentEntityTypeIdDash = StaticStringy::dasherize($contentEntityTypeId);

    $configClass = StaticStringy::upperCamelize($configEntityTypeId);
    $contentClass = StaticStringy::upperCamelize($contentEntityTypeId);

    $expectedFiles = [
      "modules/custom/{$moduleName}/config/schema/{$moduleName}.schema.yml" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}AccessControlHandler.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}AddForm.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}Comparer.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}DeleteForm.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}EditForm.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}ListBuilder.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}RouteProvider.php" => [],
      "modules/custom/{$moduleName}/src/ConfigEntity/{$configClass}Storage.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}AccessControlHandler.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}AddForm.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}Controller.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}DeleteForm.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}EditForm.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}ListBuilder.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}RevisionDeleteForm.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}RevisionRevertForm.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}RevisionRevertTranslationForm.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}RouteProvider.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}Storage.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}StorageInterface.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}StorageSchema.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}TranslationHandler.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}ViewBuilder.php" => [],
      "modules/custom/{$moduleName}/src/ContentEntity/{$contentClass}ViewController.php" => [],
      "modules/custom/{$moduleName}/src/Entity/{$contentClass}.php" => [],
      "modules/custom/{$moduleName}/src/Entity/{$configClass}.php" => [],
      "modules/custom/{$moduleName}/src/{$contentClass}Interface.php" => [],
      "modules/custom/{$moduleName}/src/{$contentClass}PermissionProvider.php" => [],
      "modules/custom/{$moduleName}/src/{$contentClass}PermissionProviderInterface.php" => [],
      "modules/custom/{$moduleName}/src/{$configClass}Interface.php" => [],
      "modules/custom/{$moduleName}/templates/{$moduleNameDash}.{$contentEntityTypeIdDash}.html.twig" => [],
      "modules/custom/{$moduleName}/{$moduleName}.info.yml" => [],
      "modules/custom/{$moduleName}/{$moduleName}.links.action.yml" => [],
      "modules/custom/{$moduleName}/{$moduleName}.links.contextual.yml" => [],
      "modules/custom/{$moduleName}/{$moduleName}.links.menu.yml" => [],
      "modules/custom/{$moduleName}/{$moduleName}.links.task.yml" => [],
      "modules/custom/{$moduleName}/{$moduleName}.module" => [],
      "modules/custom/{$moduleName}/{$moduleName}.permissions.yml" => [],
      "modules/custom/{$moduleName}/{$moduleName}.services.yml" => [],
      "modules/custom/{$moduleName}/tests/src/Functional/ConfigEntityCrudTest.php" => [],
      "modules/custom/{$moduleName}/tests/src/Functional/TestBase.php" => [],
    ];

    static::assertDirectoryDoesNotExist("$drupalRoot/modules/custom/$moduleName");

    $args = [
      $contentEntityTypeId,
      $configEntityTypeId,
      $moduleName,
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $envVars = [];
    $envVars += $this->getCommonCommandLineEnvVars();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      0,
      NULL,
      $envVars,
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    foreach ($expectedFiles as $path => $expectedFile) {
      $expectedFile += [
        '{{ path }}' => $path,
      ];

      if (!empty($expectedFile['{{ message }}'])) {
        static::assertStringContainsString(
          strtr($expectedFile['{{ message }}'], $expectedFile),
          $actualStdError,
        );
      }

      if (!empty($expectedFile['{{ path }}'])) {
        static::assertFileExists("$drupalRoot/{$expectedFile['{{ path }}']}");
      }
    }

    static::assertSame('', $actualStdOutput, 'StdOutput');

    $actualFiles = $this->collectFiles("$drupalRoot/modules/custom");
    ksort($expectedFiles);
    ksort($actualFiles);
    static::assertSame(array_keys($expectedFiles), array_keys($actualFiles));
  }

  /**
   * @return \Symfony\Component\Finder\SplFileInfo[]
   */
  protected function collectFiles(string $dir): array {
    $result = [];

    $files = (new Finder())
      ->in($dir)
      ->files();
    $drupalRoot = static::getDrupalRoot();
    foreach ($files as $file) {
      $relative = Path::makeRelative($file->getPathname(), $drupalRoot);
      $result[$relative] = $file;
    }

    return $result;
  }

}
