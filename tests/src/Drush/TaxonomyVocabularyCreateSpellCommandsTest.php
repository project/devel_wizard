<?php

declare(strict_types = 1);

namespace Drupal\Tests\devel_wizard\Drush;

/**
 * @group drush.command
 * @group devel_wizard
 * @group devel_wizard.spell
 * @group devel_wizard.spell.taxonomy_vocabulary
 * @group devel_wizard.spell.taxonomy_vocabulary.create
 *
 * @covers \Drupal\devel_wizard\Commands\TaxonomyVocabularyCreateSpellCommands
 */
class TaxonomyVocabularyCreateSpellCommandsTest extends DrushTestCase {

  protected static string $command = 'devel-wizard:spell:taxonomy-vocabulary:create';

  public function testRun(): void {
    $expected = [];
    $expected += [
      'stdError' => '',
      'stdOutput' => '',
      'exitCode' => 0,
    ];

    $envVars = [];

    $args = [
      'foo',
    ];

    $options = [];
    $options += $this->getCommonCommandLineOptions();

    $this->drush(
      static::$command,
      $args,
      $options,
      NULL,
      NULL,
      $expected['exitCode'],
      NULL,
      $envVars + $this->getCommonCommandLineEnvVars()
    );

    $actualStdError = $this->getErrorOutput();
    $actualStdOutput = $this->getOutput();

    static::assertStringContainsString(
      "Message: devel_wizard_taxonomy_vocabulary_create - Taxonomy vocabulary has been \ncreated: foo",
      $actualStdError,
      'StdError',
    );
    static::assertSame($expected['stdOutput'], $actualStdOutput, 'StdOutput');
  }

}
