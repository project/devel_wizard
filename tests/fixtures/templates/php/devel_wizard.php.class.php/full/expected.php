<?php

declare(strict_types = 1);

namespace My\Lib01;

use Me\Lib01\MyParent;
use Me\Lib01\MyInter01;
use Me\Lib01\MyInter02;
use Me\Lib01\MyInter03;

abstract class MyIdentifier extends MyParent implements MyInter01, MyInter02, MyInter03 {

  use Ab\Cd;
  use Ef\Gh {
    m1 as a1;
    m2 as a2;
    m3 as public a3;
  }


  const MY_CONST_A = 'ca';

  const MY_CONST_B = 'cb';

  protected static string $myProp01 = 'p01';

  protected string $myProp02 = 'p02';

}
